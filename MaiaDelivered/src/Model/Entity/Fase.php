<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fase Entity
 *
 * @property int $IdFase
 * @property string $CheckIdFase
 * @property int $Struttura_id
 * @property string $CheckIdStruttura
 * @property string $NomeFase
 * @property string $RiferimentiNormativi
 * @property string $TipoEventoAvvio
 * @property string $TipoOutput
 * @property string $AltreUnitaOrganizzativeCoinvolte
 * @property string $Periodicita
 * @property string $Gennaio
 * @property string $Febbraio
 * @property string $Marzo
 * @property string $Aprile
 * @property string $Maggio
 * @property string $Giugno
 * @property string $Luglio
 * @property string $Agosto
 * @property string $Settembre
 * @property string $Ottobre
 * @property string $Novembre
 * @property string $Dicembre
 * @property int $NumMedioGiorniComplet
 * @property int $NumIterazioniAnnue
 * @property int $TotaleGiornateLavoroAnnue
 * @property int $Processo_id
 * @property string $CheckIdProcesso
 * @property int $IdRilevamento
 *
 * @property \App\Model\Entity\Processo $processo
 */
class Fase extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'CheckIdFase' => true,
        'Struttura_id' => true,
        'CheckIdStruttura' => true,
        'NomeFase' => true,
        'RiferimentiNormativi' => true,
        'TipoEventoAvvio' => true,
        'TipoOutput' => true,
        'AltreUnitaOrganizzativeCoinvolte' => true,
        'Periodicita' => true,
        'Gennaio' => true,
        'Febbraio' => true,
        'Marzo' => true,
        'Aprile' => true,
        'Maggio' => true,
        'Giugno' => true,
        'Luglio' => true,
        'Agosto' => true,
        'Settembre' => true,
        'Ottobre' => true,
        'Novembre' => true,
        'Dicembre' => true,
        'NumMedioGiorniComplet' => true,
        'NumIterazioniAnnue' => true,
        'TotaleGiornateLavoroAnnue' => true,
        'Processo_id' => true,
        'CheckIdProcesso' => true,
        'IdRilevamento' => true,
        'processo' => true
    ];
}
