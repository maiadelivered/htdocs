<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;


/**
 * Processo Entity
 *
 * @property int $IdProcesso
 * @property int $IdTipoProcesso
 * @property string $CheckIdTipoProcesso
 * @property string $NomeProcesso
 * @property string $DescrizioneProcesso
 * @property string $NomeFunzione
 * @property int $IdStruttura
 * @property string $CheckIdStruttura
 * @property int $IdRilevamento
 * @property string|resource $Note
 *
 * @property \App\Model\Entity\Fase[] $fase
 */
class Processo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'IdTipoProcesso' => true,
        'CheckIdTipoProcesso' => true,
        'NomeProcesso' => true,
        'DescrizioneProcesso' => true,
        'NomeFunzione' => true,
        'IdStruttura' => true,
        'CheckIdStruttura' => true,
        'IdRilevamento' => true,
        'Note' => true,
        'fase' => true
    ];
}
