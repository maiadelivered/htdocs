<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vlistprocesso Entity
 *
 * @property int $IdProcesso
 * @property int $TipoProcesso_Id
 * @property string $NomeProcesso
 * @property string $DescrizioneProcesso
 * @property string $NomeFunzione
 * @property int $Struttura_Id
 * @property int $IdRilevamento
 * @property string|resource $Note
 * @property string $CheckIdTipoProcesso
 * @property string $CheckIdStruttura
 */
class Vlistprocesso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'IdProcesso' => true,
        'TipoProcesso_Id' => true,
        'NomeProcesso' => true,
        'DescrizioneProcesso' => true,
        'NomeFunzione' => true,
        'Struttura_Id' => true,
        'IdRilevamento' => true,
        'Note' => true,
        'CheckIdTipoProcesso' => true,
        'CheckIdStruttura' => true
    ];
}
