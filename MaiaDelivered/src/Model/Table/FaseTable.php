<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fase Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Strutturas
 * @property |\Cake\ORM\Association\BelongsTo $Processos
 * @property |\Cake\ORM\Association\BelongsToMany $Dipendente
 *
 * @method \App\Model\Entity\Fase get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Fase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fase findOrCreate($search, callable $callback = null, $options = [])
 */
class FaseTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fase');
        $this->setDisplayField('IdFase');
        $this->setPrimaryKey('IdFase');

        $this->belongsTo('Strutturas', [
            'foreignKey' => 'Struttura_id'
        ]);
        $this->belongsTo('Processos', [
            'foreignKey' => 'Processo_id'
        ]);
        $this->belongsToMany('Dipendente', [
            'foreignKey' => 'fase_id',
            'targetForeignKey' => 'dipendente_id',
            'joinTable' => 'dipendente_fase'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdFase')
            ->allowEmpty('IdFase', 'create');

        $validator
            ->scalar('CheckIdFase')
            ->allowEmpty('CheckIdFase');

        $validator
            ->scalar('CheckIdStruttura')
            ->allowEmpty('CheckIdStruttura');

        $validator
            ->scalar('NomeFase')
            ->allowEmpty('NomeFase');

        $validator
            ->scalar('RiferimentiNormativi')
            ->allowEmpty('RiferimentiNormativi');

        $validator
            ->scalar('TipoEventoAvvio')
            ->allowEmpty('TipoEventoAvvio');

        $validator
            ->scalar('TipoOutput')
            ->allowEmpty('TipoOutput');

        $validator
            ->scalar('AltreUnitaOrganizzativeCoinvolte')
            ->allowEmpty('AltreUnitaOrganizzativeCoinvolte');

        $validator
            ->scalar('Periodicita')
            ->allowEmpty('Periodicita');

        $validator
            ->scalar('Gennaio')
            ->allowEmpty('Gennaio');

        $validator
            ->scalar('Febbraio')
            ->allowEmpty('Febbraio');

        $validator
            ->scalar('Marzo')
            ->allowEmpty('Marzo');

        $validator
            ->scalar('Aprile')
            ->allowEmpty('Aprile');

        $validator
            ->scalar('Maggio')
            ->allowEmpty('Maggio');

        $validator
            ->scalar('Giugno')
            ->allowEmpty('Giugno');

        $validator
            ->scalar('Luglio')
            ->allowEmpty('Luglio');

        $validator
            ->scalar('Agosto')
            ->allowEmpty('Agosto');

        $validator
            ->scalar('Settembre')
            ->allowEmpty('Settembre');

        $validator
            ->scalar('Ottobre')
            ->allowEmpty('Ottobre');

        $validator
            ->scalar('Novembre')
            ->allowEmpty('Novembre');

        $validator
            ->scalar('Dicembre')
            ->allowEmpty('Dicembre');

        $validator
            ->integer('NumMedioGiorniComplet')
            ->allowEmpty('NumMedioGiorniComplet');

        $validator
            ->integer('NumIterazioniAnnue')
            ->allowEmpty('NumIterazioniAnnue');

        $validator
            ->integer('TotaleGiornateLavoroAnnue')
            ->allowEmpty('TotaleGiornateLavoroAnnue');

        $validator
            ->scalar('CheckIdProcesso')
            ->allowEmpty('CheckIdProcesso');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['Struttura_id'], 'Strutturas'));
        $rules->add($rules->existsIn(['Processo_id'], 'Processos'));

        return $rules;
    }
}
