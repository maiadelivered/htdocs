<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Processo Model
 *
 * @property \App\Model\Table\FaseTable|\Cake\ORM\Association\HasMany $Fase
 *
 * @method \App\Model\Entity\Processo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Processo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Processo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Processo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Processo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Processo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Processo findOrCreate($search, callable $callback = null, $options = [])
 */
class ProcessoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('processo');
        $this->setDisplayField('IdProcesso');
        $this->setPrimaryKey('IdProcesso');

        $this->hasMany('Fase', [
            'foreignKey' => 'processo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */

    public function getcustomprocess()
    {
        /*
         * $processoFiltered = $this->Processo->find('all', array('conditions' => array('Nomeprocesso LIKE' => '%'.$passedArgs.'%')));
        return $processoFiltered;

          funzionante
         */
        $connection = ConnectionManager::get('default');
        $sql = "select * from processo where NomeProcesso = 'Trasparenza e pubblicazioni'";
        $results = $connection->execute($sql)->fetchAll('assoc');
        return  $results;
        /**/
    }

/*
    public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $recursive = -1;

        // Mandatory to have
        $this->useTable = false;
        $sql = "select * from processo where NomeProcesso = 'Trasparenza e pubblicazioni' ";

        $sql .= " Limit ";

        // Adding LIMIT Clause
        $sql .= (($page - 1) * $limit) . ', ' . $limit;
        echo $sql;
        $results = $this->query($sql);

        return $results;
    }

    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {

        $sql = "SELECT COUNT(processo.idprocesso) FROM processo";

        // $sql .= "Your custom query here just do not include limit portion";

        $this->recursive = $recursive;

        $results = $this->query($sql);

        return count($results);
    }
*/
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdProcesso')
            ->allowEmpty('IdProcesso', 'create');

        $validator
            ->integer('IdTipoProcesso')
            ->allowEmpty('IdTipoProcesso');

        $validator
            ->scalar('CheckIdTipoProcesso')
            ->allowEmpty('CheckIdTipoProcesso');

        $validator
            ->scalar('NomeProcesso')
            ->allowEmpty('NomeProcesso');

        $validator
            ->scalar('DescrizioneProcesso')
            ->allowEmpty('DescrizioneProcesso');

        $validator
            ->scalar('NomeFunzione')
            ->allowEmpty('NomeFunzione');

        $validator
            ->integer('IdStruttura')
            ->allowEmpty('IdStruttura');

        $validator
            ->scalar('CheckIdStruttura')
            ->allowEmpty('CheckIdStruttura');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        $validator
            ->allowEmpty('Note');

        return $validator;
    }
}
