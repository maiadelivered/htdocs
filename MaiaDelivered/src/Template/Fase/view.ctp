<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase $fase
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fase'), ['action' => 'edit', $fase->IdFase]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fase'), ['action' => 'delete', $fase->IdFase], ['confirm' => __('Are you sure you want to delete # {0}?', $fase->IdFase)]) ?> </li>
        <li><?= $this->Html->link(__('List Fase'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fase'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Processo'), ['controller' => 'Processo', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Processo'), ['controller' => 'Processo', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fase view large-9 medium-8 columns content">
    <h3><?= h($fase->IdFase) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('CheckIdFase') ?></th>
            <td><?= h($fase->CheckIdFase) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdStruttura') ?></th>
            <td><?= h($fase->CheckIdStruttura) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NomeFase') ?></th>
            <td><?= h($fase->NomeFase) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TipoEventoAvvio') ?></th>
            <td><?= h($fase->TipoEventoAvvio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TipoOutput') ?></th>
            <td><?= h($fase->TipoOutput) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('AltreUnitaOrganizzativeCoinvolte') ?></th>
            <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Periodicita') ?></th>
            <td><?= h($fase->Periodicita) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gennaio') ?></th>
            <td><?= h($fase->Gennaio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Febbraio') ?></th>
            <td><?= h($fase->Febbraio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Marzo') ?></th>
            <td><?= h($fase->Marzo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Aprile') ?></th>
            <td><?= h($fase->Aprile) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Maggio') ?></th>
            <td><?= h($fase->Maggio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Giugno') ?></th>
            <td><?= h($fase->Giugno) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Luglio') ?></th>
            <td><?= h($fase->Luglio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agosto') ?></th>
            <td><?= h($fase->Agosto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Settembre') ?></th>
            <td><?= h($fase->Settembre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ottobre') ?></th>
            <td><?= h($fase->Ottobre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Novembre') ?></th>
            <td><?= h($fase->Novembre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dicembre') ?></th>
            <td><?= h($fase->Dicembre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdProcesso') ?></th>
            <td><?= h($fase->CheckIdProcesso) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdFase') ?></th>
            <td><?= $this->Number->format($fase->IdFase) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Struttura Id') ?></th>
            <td><?= $this->Number->format($fase->Struttura_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NumMedioGiorniComplet') ?></th>
            <td><?= $this->Number->format($fase->NumMedioGiorniComplet) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NumIterazioniAnnue') ?></th>
            <td><?= $this->Number->format($fase->NumIterazioniAnnue) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TotaleGiornateLavoroAnnue') ?></th>
            <td><?= $this->Number->format($fase->TotaleGiornateLavoroAnnue) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Processo Id') ?></th>
            <td><?= $this->Number->format($fase->Processo_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdRilevamento') ?></th>
            <td><?= $this->Number->format($fase->IdRilevamento) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('RiferimentiNormativi') ?></h4>
        <?= $this->Text->autoParagraph(h($fase->RiferimentiNormativi)); ?>
    </div>
</div>
