<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo $processo
 */
?>

<br/>
<br/><br/><br/>
<div class="processo view large-9 medium-8 columns content">
    <h3><?= h($processo->NomeProcesso) ?></h3>
    <table class="table-responsive" ="vertical-table">
        <tr>
            <th scope="row" width="40"><?= __('Tipo Processo') ?></th>
            <td  width="300"><?= h($processo->CheckIdTipoProcesso) ?> </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Struttura') ?></th>
            <td><?= h($processo->CheckIdStruttura) ?></td>
        </tr>
    </table>
    <div >
        <h5><?= __('Descrizione Processo') ?></h5>
        <?= $this->Text->autoParagraph(h($processo->DescrizioneProcesso)); ?>
    </div>
    <div class="processo view large-9 medium-8 columns content">
        <h5><?= __('NomeFunzione') ?></h5>
        <?= $this->Text->autoParagraph(h($processo->NomeFunzione)); ?>
    </div>
</div>
<div >
    <nav class="navbar navbar-default" >
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><?= __('Fasi del processo');?></a>
            </div>
        </div>
    </nav>
    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col">IdFase</th>
            <th scope="col">Struttura</th>
            <th scope="col">Nome Fase</th>
            <th scope="col">Tipo Evento Avvio</th>
            <th scope="col">Tipologia di Output</th>
            <th scope="col">Altre Unità Organizzative Coinvolte</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($fase as $fase): ?>
            <tr>
                <td><?= h($fase->CheckIdFase) ?></td>
                <td><?= h($fase->CheckIdStruttura) ?></td>
                <td><?= h($fase->NomeFase) ?></td>
                <td><?= h($fase->TipoOutput) ?></td>
                <td><?= h($fase->TipoEventoAvvio) ?></td>
                <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>