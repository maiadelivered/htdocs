<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo[]|\Cake\Collection\CollectionInterface $processo
 */
use Cake\I18n\Time;
?>


</div>
     <nav class="navbar navbar-default" >
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><?= __('Processo');?></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <a href="#" id= "ImgSearch"  >
                                <img  src="img/search.jpg" height="40" width="40" ></h3>
                             </a>
                            <script>
                                $( "#ImgSearch" ).click(function() {
                                    if($("#Divricerca").is(':hidden')){
                                        $( "#Divricerca").show( "slow" );
                                }else {
                                        $( "#Divricerca").hide("slow");
                                    }
                                });
                            </script>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li  class="nav navbar-nav"><?= $this->Html->link(__('Nuovo'), ['action' => 'add']) ?></li>
                            <li  class="nav navbar-nav"><?= $this->Html->link(__('List Fase'), ['controller' => 'Fase', 'action' => 'index']) ?></li>
                            <li  class="nav navbar-nav"><?= $this->Html->link(__('New Fase'), ['controller' => 'Fase', 'action' => 'add']) ?></li>
                        </ul>
                    </div>
                </div>
     </nav>

    <div class="form-group" id="Divricerca" hidden="true" >
            <?= $this->Form->create('Post') ?>
            <fieldset>
                <legend><?= __('Ricerca Processo') ?></legend>
                <?php
                echo $this->Form->control('descrizione',
                    array('type'=>'text','class'=>'form-control', 'size'=>30,
                        'label'=> 'Inserisci il nome del processo da ricercare:', 'placeholder'=>'inserisci processo'));
                ?>
            </fieldset>
            <?= $this->Form->button(__('Ricerca'),array(' class'=> 'btn btn-default', 'type=' => 'submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('IdProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IdTipoProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdTipoProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IdStruttura') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdStruttura') ?></th>

                <th scope="col" class="actions" width="120" ><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($processo as $processo): ?>
            <tr>
                <td><?= $this->Number->format($processo->IdProcesso) ?></td>
                <td><?= $this->Number->format($processo->IdTipoProcesso) ?></td>
                <td><?= h($processo->CheckIdTipoProcesso) ?></td>
                <td><?= h($processo->NomeProcesso) ?></td>
                <td><?= $this->Number->format($processo->IdStruttura) ?></td>
                <td><?= h($processo->CheckIdStruttura) ?></td>

                <td class="actions">

                    <div class="btn-group">
                        <a href="#" class="btn btn-default">Azioni</a>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><?= $this->Html->link(__('View'), ['action' => 'view', $processo->IdProcesso]) ?></li>
                            <li> <?= $this->Html->link(__('Edit'), ['action' => 'edit', $processo->IdProcesso]) ?></li>
                            <li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $processo->IdProcesso], ['confirm' => __('Are you sure you want to delete # {0}?', $processo->IdProcesso)]) ?>
                           </li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
