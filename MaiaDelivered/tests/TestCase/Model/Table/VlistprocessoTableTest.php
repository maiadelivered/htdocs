<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VlistprocessoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VlistprocessoTable Test Case
 */
class VlistprocessoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VlistprocessoTable
     */
    public $Vlistprocesso;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vlistprocesso'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Vlistprocesso') ? [] : ['className' => VlistprocessoTable::class];
        $this->Vlistprocesso = TableRegistry::get('Vlistprocesso', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Vlistprocesso);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
