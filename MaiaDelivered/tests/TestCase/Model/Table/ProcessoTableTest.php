<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProcessoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProcessoTable Test Case
 */
class ProcessoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProcessoTable
     */
    public $Processo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.processo',
        'app.fase',
        'app.processos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Processo') ? [] : ['className' => ProcessoTable::class];
        $this->Processo = TableRegistry::get('Processo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Processo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
