<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FaseTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FaseTable Test Case
 */
class FaseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FaseTable
     */
    public $Fase;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fase',
        'app.processo'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Fase') ? [] : ['className' => FaseTable::class];
        $this->Fase = TableRegistry::get('Fase', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fase);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
