<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase $fase
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Fase'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Struttura'), ['controller' => 'Struttura', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Struttura'), ['controller' => 'Struttura', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Processo'), ['controller' => 'Processo', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Processo'), ['controller' => 'Processo', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dipendente'), ['controller' => 'Dipendente', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dipendente'), ['controller' => 'Dipendente', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fase form large-9 medium-8 columns content">
    <?= $this->Form->create($fase) ?>
    <fieldset>
        <legend><?= __('Add Fase') ?></legend>
        <?php
            echo $this->Form->control('CheckIdFase');
            echo $this->Form->control('Struttura_id', ['options' => $struttura, 'empty' => true]);
            echo $this->Form->control('CheckIdStruttura');
            echo $this->Form->control('NomeFase');
            echo $this->Form->control('RiferimentiNormativi');
            echo $this->Form->control('TipoEventoAvvio');
            echo $this->Form->control('TipoOutput');
            echo $this->Form->control('AltreUnitaOrganizzativeCoinvolte');
            echo $this->Form->control('Periodicita');
            echo $this->Form->control('Gennaio');
            echo $this->Form->control('Febbraio');
            echo $this->Form->control('Marzo');
            echo $this->Form->control('Aprile');
            echo $this->Form->control('Maggio');
            echo $this->Form->control('Giugno');
            echo $this->Form->control('Luglio');
            echo $this->Form->control('Agosto');
            echo $this->Form->control('Settembre');
            echo $this->Form->control('Ottobre');
            echo $this->Form->control('Novembre');
            echo $this->Form->control('Dicembre');
            echo $this->Form->control('NumMedioGiorniComplet');
            echo $this->Form->control('NumIterazioniAnnue');
            echo $this->Form->control('TotaleGiornateLavoroAnnue');
            echo $this->Form->control('Processo_id', ['options' => $processo, 'empty' => true]);
            echo $this->Form->control('CheckIdProcesso');
            echo $this->Form->control('IdRilevamento');
            echo $this->Form->control('dipendente._ids', ['options' => $dipendente]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
