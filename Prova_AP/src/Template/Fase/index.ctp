<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase[]|\Cake\Collection\CollectionInterface $fase
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Fase'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Struttura'), ['controller' => 'Struttura', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Struttura'), ['controller' => 'Struttura', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Processo'), ['controller' => 'Processo', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Processo'), ['controller' => 'Processo', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dipendente'), ['controller' => 'Dipendente', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dipendente'), ['controller' => 'Dipendente', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fase index large-9 medium-8 columns content">
    <h3><?= __('Fase') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('IdFase') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdFase') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Struttura_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdStruttura') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeFase') ?></th>
                <th scope="col"><?= $this->Paginator->sort('TipoEventoAvvio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('TipoOutput') ?></th>
                <th scope="col"><?= $this->Paginator->sort('AltreUnitaOrganizzativeCoinvolte') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Periodicita') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Gennaio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Febbraio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Marzo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Aprile') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Maggio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Giugno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Luglio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Agosto') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Settembre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Ottobre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Novembre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Dicembre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NumMedioGiorniComplet') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NumIterazioniAnnue') ?></th>
                <th scope="col"><?= $this->Paginator->sort('TotaleGiornateLavoroAnnue') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Processo_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IdRilevamento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($fase as $fase): ?>
            <tr>
                <td><?= $this->Number->format($fase->IdFase) ?></td>
                <td><?= h($fase->CheckIdFase) ?></td>
                <td><?= $fase->has('struttura') ? $this->Html->link($fase->struttura->IdStruttura, ['controller' => 'Struttura', 'action' => 'view', $fase->struttura->IdStruttura]) : '' ?></td>
                <td><?= h($fase->CheckIdStruttura) ?></td>
                <td><?= h($fase->NomeFase) ?></td>
                <td><?= h($fase->TipoEventoAvvio) ?></td>
                <td><?= h($fase->TipoOutput) ?></td>
                <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
                <td><?= h($fase->Periodicita) ?></td>
                <td><?= h($fase->Gennaio) ?></td>
                <td><?= h($fase->Febbraio) ?></td>
                <td><?= h($fase->Marzo) ?></td>
                <td><?= h($fase->Aprile) ?></td>
                <td><?= h($fase->Maggio) ?></td>
                <td><?= h($fase->Giugno) ?></td>
                <td><?= h($fase->Luglio) ?></td>
                <td><?= h($fase->Agosto) ?></td>
                <td><?= h($fase->Settembre) ?></td>
                <td><?= h($fase->Ottobre) ?></td>
                <td><?= h($fase->Novembre) ?></td>
                <td><?= h($fase->Dicembre) ?></td>
                <td><?= $this->Number->format($fase->NumMedioGiorniComplet) ?></td>
                <td><?= $this->Number->format($fase->NumIterazioniAnnue) ?></td>
                <td><?= $this->Number->format($fase->TotaleGiornateLavoroAnnue) ?></td>
                <td><?= $fase->has('processo') ? $this->Html->link($fase->processo->IdProcesso, ['controller' => 'Processo', 'action' => 'view', $fase->processo->IdProcesso]) : '' ?></td>
                <td><?= h($fase->CheckIdProcesso) ?></td>
                <td><?= $this->Number->format($fase->IdRilevamento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $fase->IdFase]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fase->IdFase]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fase->IdFase], ['confirm' => __('Are you sure you want to delete # {0}?', $fase->IdFase)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
