<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo $processo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Processo'), ['action' => 'edit', $processo->IdProcesso]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Processo'), ['action' => 'delete', $processo->IdProcesso], ['confirm' => __('Are you sure you want to delete # {0}?', $processo->IdProcesso)]) ?> </li>
        <li><?= $this->Html->link(__('List Processo'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Processo'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="processo view large-9 medium-8 columns content">
    <h3><?= h($processo->IdProcesso) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('CheckIdTipoProcesso') ?></th>
            <td><?= h($processo->CheckIdTipoProcesso) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NomeProcesso') ?></th>
            <td><?= h($processo->NomeProcesso) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdStruttura') ?></th>
            <td><?= h($processo->CheckIdStruttura) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdProcesso') ?></th>
            <td><?= $this->Number->format($processo->IdProcesso) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TipoProcesso Id') ?></th>
            <td><?= $this->Number->format($processo->TipoProcesso_Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Struttura Id') ?></th>
            <td><?= $this->Number->format($processo->Struttura_Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdRilevamento') ?></th>
            <td><?= $this->Number->format($processo->IdRilevamento) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('DescrizioneProcesso') ?></h4>
        <?= $this->Text->autoParagraph(h($processo->DescrizioneProcesso)); ?>
    </div>
    <div class="row">
        <h4><?= __('NomeFunzione') ?></h4>
        <?= $this->Text->autoParagraph(h($processo->NomeFunzione)); ?>
    </div>
</div>


<div class="fase index large-9 medium-8 columns content">
    <h3><?= __('Fase') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('CheckIdFase') ?></th>
            <th scope="col"><?= $this->Paginator->sort('CheckIdStruttura') ?></th>
            <th scope="col"><?= $this->Paginator->sort('NomeFase') ?></th>
            <th scope="col"><?= $this->Paginator->sort('TipoEventoAvvio') ?></th>
            <th scope="col"><?= $this->Paginator->sort('TipoOutput') ?></th>
            <th scope="col"><?= $this->Paginator->sort('AltreUnitaOrganizzativeCoinvolte') ?></th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($fase as $fase): ?>
            <tr>
                <td><?= h($fase->CheckIdFase) ?></td>
                <td><?= h($fase->CheckIdStruttura) ?></td>
                <td><?= h($fase->NomeFase) ?></td>
                <td><?= h($fase->TipoOutput) ?></td>
                <td><?= h($fase->TipoEventoAvvio) ?></td>
                <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
            </tr>
        <?php endforeach; ?>


        <div class="processo view large-9 medium-8 columns content">
            <h3>Dettagli del carico di lavoro dichiarato sul processo<?= h($processo->NomeProcesso) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('NumDipTot') ?></th>
                    <td>


                        <?= h($vdipeprorownpAllproFiltered->NumDipTot) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('NomeProcesso') ?></th>
                    <td><?= h($processo->NomeProcesso) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('CheckIdStruttura') ?></th>
                    <td><?= h($processo->CheckIdStruttura) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('IdProcesso') ?></th>
                    <td><?= $this->Number->format($processo->IdProcesso) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('TipoProcesso Id') ?></th>
                    <td><?= $this->Number->format($processo->TipoProcesso_Id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Struttura Id') ?></th>
                    <td><?= $this->Number->format($processo->Struttura_Id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('IdRilevamento') ?></th>
                    <td><?= $this->Number->format($processo->IdRilevamento) ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= __('DescrizioneProcesso') ?></h4>
                <?= $this->Text->autoParagraph(h($processo->DescrizioneProcesso)); ?>
            </div>
            <div class="row">
                <h4><?= __('NomeFunzione') ?></h4>
                <?= $this->Text->autoParagraph(h($processo->NomeFunzione)); ?>
            </div>
        </div>

        </tbody>
    </table>
</div>
