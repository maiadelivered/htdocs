<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo $processo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Processo'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="processo form large-9 medium-8 columns content">
    <?= $this->Form->create($processo) ?>
    <fieldset>
        <legend><?= __('Add Processo') ?></legend>
        <?php
            echo $this->Form->control('TipoProcesso_Id');
            echo $this->Form->control('CheckIdTipoProcesso');
            echo $this->Form->control('NomeProcesso');
            echo $this->Form->control('DescrizioneProcesso');
            echo $this->Form->control('NomeFunzione');
            echo $this->Form->control('Struttura_Id');
            echo $this->Form->control('CheckIdStruttura');
            echo $this->Form->control('IdRilevamento');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
