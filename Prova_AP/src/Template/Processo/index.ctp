<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo[]|\Cake\Collection\CollectionInterface $processo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Processo'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="processo index large-9 medium-8 columns content">
    <h3><?= __('Processo') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('IdProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('TipoProcesso_Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdTipoProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Struttura_Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdStruttura') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IdRilevamento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($processo as $processo): ?>
            <tr>
                <td><?= $this->Number->format($processo->IdProcesso) ?></td>
                <td><?= $this->Number->format($processo->TipoProcesso_Id) ?></td>
                <td><?= h($processo->CheckIdTipoProcesso) ?></td>
                <td><?= h($processo->NomeProcesso) ?></td>
                <td><?= $this->Number->format($processo->Struttura_Id) ?></td>
                <td><?= h($processo->CheckIdStruttura) ?></td>
                <td><?= $this->Number->format($processo->IdRilevamento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $processo->IdProcesso]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $processo->IdProcesso]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $processo->IdProcesso], ['confirm' => __('Are you sure you want to delete # {0}?', $processo->IdProcesso)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
