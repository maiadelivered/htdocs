<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento2 $rilevamento2
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $rilevamento2->IdRilevamento],
                ['confirm' => __('Are you sure you want to delete # {0}?', $rilevamento2->IdRilevamento)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Rilevamento2'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="rilevamento2 form large-9 medium-8 columns content">
    <?= $this->Form->create($rilevamento2) ?>
    <fieldset>
        <legend><?= __('Edit Rilevamento2') ?></legend>
        <?php
            echo $this->Form->control('Rilevamento');
            echo $this->Form->control('Inizio', ['empty' => true]);
            echo $this->Form->control('Fine', ['empty' => true]);
            echo $this->Form->control('Descrizione');
            echo $this->Form->control('Note');
            echo $this->Form->control('IdStato');
            echo $this->Form->control('CheckIdStato');
            echo $this->Form->control('Creatore');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
