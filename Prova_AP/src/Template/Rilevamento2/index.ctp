<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento2[]|\Cake\Collection\CollectionInterface $rilevamento2
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Rilevamento2'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="rilevamento2 index large-9 medium-8 columns content">
    <h3><?= __('Rilevamento2') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('IdRilevamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Rilevamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Inizio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Fine') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IdStato') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdStato') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Creatore') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rilevamento2 as $rilevamento2): ?>
            <tr>
                <td><?= $this->Number->format($rilevamento2->IdRilevamento) ?></td>
                <td><?= h($rilevamento2->Rilevamento) ?></td>
                <td><?= h($rilevamento2->Inizio) ?></td>
                <td><?= h($rilevamento2->Fine) ?></td>
                <td><?= $this->Number->format($rilevamento2->IdStato) ?></td>
                <td><?= h($rilevamento2->CheckIdStato) ?></td>
                <td><?= h($rilevamento2->Creatore) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $rilevamento2->IdRilevamento]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rilevamento2->IdRilevamento]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rilevamento2->IdRilevamento], ['confirm' => __('Are you sure you want to delete # {0}?', $rilevamento2->IdRilevamento)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
