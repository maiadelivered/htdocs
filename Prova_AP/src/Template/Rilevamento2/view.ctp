<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento2 $rilevamento2
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Rilevamento2'), ['action' => 'edit', $rilevamento2->IdRilevamento]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rilevamento2'), ['action' => 'delete', $rilevamento2->IdRilevamento], ['confirm' => __('Are you sure you want to delete # {0}?', $rilevamento2->IdRilevamento)]) ?> </li>
        <li><?= $this->Html->link(__('List Rilevamento2'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rilevamento2'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rilevamento2 view large-9 medium-8 columns content">
    <h3><?= h($rilevamento2->IdRilevamento) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Rilevamento') ?></th>
            <td><?= h($rilevamento2->Rilevamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdStato') ?></th>
            <td><?= h($rilevamento2->CheckIdStato) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creatore') ?></th>
            <td><?= h($rilevamento2->Creatore) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdRilevamento') ?></th>
            <td><?= $this->Number->format($rilevamento2->IdRilevamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdStato') ?></th>
            <td><?= $this->Number->format($rilevamento2->IdStato) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Inizio') ?></th>
            <td><?= h($rilevamento2->Inizio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fine') ?></th>
            <td><?= h($rilevamento2->Fine) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descrizione') ?></h4>
        <?= $this->Text->autoParagraph(h($rilevamento2->Descrizione)); ?>
    </div>
    <div class="row">
        <h4><?= __('Note') ?></h4>
        <?= $this->Text->autoParagraph(h($rilevamento2->Note)); ?>
    </div>
</div>
