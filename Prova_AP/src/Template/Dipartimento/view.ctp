<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dipartimento $dipartimento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dipartimento'), ['action' => 'edit', $dipartimento->IdDipartimento]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dipartimento'), ['action' => 'delete', $dipartimento->IdDipartimento], ['confirm' => __('Are you sure you want to delete # {0}?', $dipartimento->IdDipartimento)]) ?> </li>
        <li><?= $this->Html->link(__('List Dipartimento'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dipartimento'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dipartimento view large-9 medium-8 columns content">
    <h3><?= h($dipartimento->IdDipartimento) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('NomeDipartimento') ?></th>
            <td><?= h($dipartimento->NomeDipartimento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NomeDirettore') ?></th>
            <td><?= h($dipartimento->NomeDirettore) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdDipartimento') ?></th>
            <td><?= $this->Number->format($dipartimento->IdDipartimento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdDirettore') ?></th>
            <td><?= $this->Number->format($dipartimento->IdDirettore) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rilevamento Id') ?></th>
            <td><?= $this->Number->format($dipartimento->Rilevamento_id) ?></td>
        </tr>
    </table>
</div>
