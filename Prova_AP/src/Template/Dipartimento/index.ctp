<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dipartimento[]|\Cake\Collection\CollectionInterface $dipartimento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dipartimento'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dipartimento index large-9 medium-8 columns content">
    <h3><?= __('Dipartimento') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('IdDipartimento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeDipartimento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeDirettore') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IdDirettore') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Rilevamento_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dipartimento as $dipartimento): ?>
            <tr>
                <td><?= $this->Number->format($dipartimento->IdDipartimento) ?></td>
                <td><?= h($dipartimento->NomeDipartimento) ?></td>
                <td><?= h($dipartimento->NomeDirettore) ?></td>
                <td><?= $this->Number->format($dipartimento->IdDirettore) ?></td>
                <td><?= $this->Number->format($dipartimento->Rilevamento_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dipartimento->IdDipartimento]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dipartimento->IdDipartimento]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dipartimento->IdDipartimento], ['confirm' => __('Are you sure you want to delete # {0}?', $dipartimento->IdDipartimento)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
