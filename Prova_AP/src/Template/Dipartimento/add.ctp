<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dipartimento $dipartimento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Dipartimento'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="dipartimento form large-9 medium-8 columns content">
    <?= $this->Form->create($dipartimento) ?>
    <fieldset>
        <legend><?= __('Add Dipartimento') ?></legend>
        <?php
            echo $this->Form->control('NomeDipartimento');
            echo $this->Form->control('NomeDirettore');
            echo $this->Form->control('IdDirettore');
            echo $this->Form->control('Rilevamento_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
