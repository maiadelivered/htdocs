<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sezione $sezione
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sezione'), ['action' => 'edit', $sezione->IdSezione]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sezione'), ['action' => 'delete', $sezione->IdSezione], ['confirm' => __('Are you sure you want to delete # {0}?', $sezione->IdSezione)]) ?> </li>
        <li><?= $this->Html->link(__('List Sezione'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sezione'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dipartimento'), ['controller' => 'Dipartimento', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dipartimento'), ['controller' => 'Dipartimento', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sezione view large-9 medium-8 columns content">
    <h3><?= h($sezione->IdSezione) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('NomeSezione') ?></th>
            <td><?= h($sezione->NomeSezione) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NomeDirigente') ?></th>
            <td><?= h($sezione->NomeDirigente) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('EmailDirigente') ?></th>
            <td><?= h($sezione->EmailDirigente) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Riferimenti') ?></th>
            <td><?= h($sezione->Riferimenti) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dipartimento') ?></th>
            <td><?= $sezione->has('dipartimento') ? $this->Html->link($sezione->dipartimento->IdDipartimento, ['controller' => 'Dipartimento', 'action' => 'view', $sezione->dipartimento->IdDipartimento]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdDipartimento') ?></th>
            <td><?= h($sezione->CheckIdDipartimento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdSezione') ?></th>
            <td><?= $this->Number->format($sezione->IdSezione) ?></td>
        </tr>
    </table>
</div>
