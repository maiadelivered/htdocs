<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sezione $sezione
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sezione'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dipartimento'), ['controller' => 'Dipartimento', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dipartimento'), ['controller' => 'Dipartimento', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sezione form large-9 medium-8 columns content">
    <?= $this->Form->create($sezione) ?>
    <fieldset>
        <legend><?= __('Add Sezione') ?></legend>
        <?php
            echo $this->Form->control('NomeSezione');
            echo $this->Form->control('NomeDirigente');
            echo $this->Form->control('EmailDirigente');
            echo $this->Form->control('Riferimenti');
            echo $this->Form->control('Dipartimento_id', ['options' => $dipartimento]);
            echo $this->Form->control('CheckIdDipartimento');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
