<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sezione[]|\Cake\Collection\CollectionInterface $sezione
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sezione'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dipartimento'), ['controller' => 'Dipartimento', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dipartimento'), ['controller' => 'Dipartimento', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sezione index large-9 medium-8 columns content">
    <h3><?= __('Sezione') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('IdSezione') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeSezione') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeDirigente') ?></th>
                <th scope="col"><?= $this->Paginator->sort('EmailDirigente') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Riferimenti') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Dipartimento_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdDipartimento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sezione as $sezione): ?>
            <tr>
                <td><?= $this->Number->format($sezione->IdSezione) ?></td>
                <td><?= h($sezione->NomeSezione) ?></td>
                <td><?= h($sezione->NomeDirigente) ?></td>
                <td><?= h($sezione->EmailDirigente) ?></td>
                <td><?= h($sezione->Riferimenti) ?></td>
                <td><?= $sezione->has('dipartimento') ? $this->Html->link($sezione->dipartimento->IdDipartimento, ['controller' => 'Dipartimento', 'action' => 'view', $sezione->dipartimento->IdDipartimento]) : '' ?></td>
                <td><?= h($sezione->CheckIdDipartimento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sezione->IdSezione]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sezione->IdSezione]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sezione->IdSezione], ['confirm' => __('Are you sure you want to delete # {0}?', $sezione->IdSezione)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
