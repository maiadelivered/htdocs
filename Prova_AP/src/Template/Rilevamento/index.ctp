<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento[]|\Cake\Collection\CollectionInterface $rilevamento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Rilevamento'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="rilevamento index large-9 medium-8 columns content">
    <h3><?= __('Rilevamento') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('IdRilevamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Rilevamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Inizio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Fine') ?></th>
                <th scope="col"><?= $this->Paginator->sort('IdStato') ?></th>
                <th scope="col"><?= $this->Paginator->sort('CheckIdStato') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Creatore') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rilevamento as $rilevamento): ?>
            <tr>
                <td><?= $this->Number->format($rilevamento->IdRilevamento) ?></td>
                <td><?= h($rilevamento->Rilevamento) ?></td>
                <td><?= h($rilevamento->Inizio) ?></td>
                <td><?= h($rilevamento->Fine) ?></td>
                <td><?= $this->Number->format($rilevamento->IdStato) ?></td>
                <td><?= h($rilevamento->CheckIdStato) ?></td>
                <td><?= h($rilevamento->Creatore) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $rilevamento->IdRilevamento]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rilevamento->IdRilevamento]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rilevamento->IdRilevamento], ['confirm' => __('Are you sure you want to delete # {0}?', $rilevamento->IdRilevamento)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
