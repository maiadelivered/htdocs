<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento $rilevamento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Rilevamento'), ['action' => 'edit', $rilevamento->IdRilevamento]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rilevamento'), ['action' => 'delete', $rilevamento->IdRilevamento], ['confirm' => __('Are you sure you want to delete # {0}?', $rilevamento->IdRilevamento)]) ?> </li>
        <li><?= $this->Html->link(__('List Rilevamento'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rilevamento'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rilevamento view large-9 medium-8 columns content">
    <h3><?= h($rilevamento->IdRilevamento) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Rilevamento') ?></th>
            <td><?= h($rilevamento->Rilevamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Inizio') ?></th>
            <td><?= h($rilevamento->Inizio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fine') ?></th>
            <td><?= h($rilevamento->Fine) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdStato') ?></th>
            <td><?= h($rilevamento->CheckIdStato) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creatore') ?></th>
            <td><?= h($rilevamento->Creatore) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdRilevamento') ?></th>
            <td><?= $this->Number->format($rilevamento->IdRilevamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdStato') ?></th>
            <td><?= $this->Number->format($rilevamento->IdStato) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descrizione') ?></h4>
        <?= $this->Text->autoParagraph(h($rilevamento->Descrizione)); ?>
    </div>
    <div class="row">
        <h4><?= __('Note') ?></h4>
        <?= $this->Text->autoParagraph(h($rilevamento->Note)); ?>
    </div>
</div>
