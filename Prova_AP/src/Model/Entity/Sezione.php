<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sezione Entity
 *
 * @property int $IdSezione
 * @property string $NomeSezione
 * @property string $NomeDirigente
 * @property string $EmailDirigente
 * @property string $Riferimenti
 * @property int $Dipartimento_id
 * @property string $CheckIdDipartimento
 *
 * @property \App\Model\Entity\Dipartimento $dipartimento
 */
class Sezione extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'NomeSezione' => true,
        'NomeDirigente' => true,
        'EmailDirigente' => true,
        'Riferimenti' => true,
        'Dipartimento_id' => true,
        'CheckIdDipartimento' => true,
        'dipartimento' => true
    ];
}
