<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dipartimento Entity
 *
 * @property int $IdDipartimento
 * @property string $NomeDipartimento
 * @property string $NomeDirettore
 * @property int $IdDirettore
 * @property int $Rilevamento_id
 *
 * @property \App\Model\Entity\Rilevamento $rilevamento
 */
class Dipartimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'NomeDipartimento' => true,
        'NomeDirettore' => true,
        'IdDirettore' => true,
        'Rilevamento_id' => true,
        'rilevamento' => true
    ];
}
