<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VdipeprorownpAllpro Entity
 *
 * @property string $NomeDipartimento
 * @property string $NomeSezione
 * @property string $NomeStruttura
 * @property string $TipoProcesso
 * @property string $NomeProcesso
 * @property int $NumDipTot
 * @property float $NumCatD
 * @property float $NumPO
 * @property float $NumAP
 * @property float $NumAltrePOAP
 * @property float $NumCatDirigente
 * @property float $NumCatC
 * @property float $NumCatB
 * @property float $NumCatSegConsReg
 * @property float $NumCatA
 * @property float $NumCatDirDip
 * @property float $NumCatCapoRedattore
 * @property float $NumCatSegrGiuntaReg
 * @property float $NumCatResCoordPolInte
 * @property float $NumCatDirConTD
 * @property float $NumCatCapoSerSenior
 * @property float $NumCatRed30Mesi
 * @property float $NumCatCapoGab
 * @property float $NumCatSegrGenPres
 * @property float $NumCatAvvCoord
 * @property float $PercCatD
 * @property float $PercPO
 * @property float $PercAP
 * @property float $PercAltrePOAP
 * @property float $PercCatDirigente
 * @property float $PercCatC
 * @property float $PercCatB
 * @property float $PercCatSegConsReg
 * @property float $PercCatA
 * @property float $PercCatDirDip
 * @property float $PercCatCapoRedattore
 * @property float $PercCatSegrGiuntaReg
 * @property float $PercCatResCoordPolInte
 * @property float $PercCatDirConTD
 * @property float $PercCatCapoSerSenior
 * @property float $PercCatRed30Mesi
 * @property float $PercCatCapoGab
 * @property float $PercCatSegrGenPres
 * @property float $PercCatAvvCoord
 * @property int $IdDipartimento
 * @property int $Sezione_Id
 * @property int $Struttura_Id
 * @property int $IdProcesso
 * @property int $IdTipoProcesso
 * @property int $IdRilevamento
 */
class VdipeprorownpAllpro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'NomeDipartimento' => true,
        'NomeSezione' => true,
        'NomeStruttura' => true,
        'TipoProcesso' => true,
        'NomeProcesso' => true,
        'NumDipTot' => true,
        'NumCatD' => true,
        'NumPO' => true,
        'NumAP' => true,
        'NumAltrePOAP' => true,
        'NumCatDirigente' => true,
        'NumCatC' => true,
        'NumCatB' => true,
        'NumCatSegConsReg' => true,
        'NumCatA' => true,
        'NumCatDirDip' => true,
        'NumCatCapoRedattore' => true,
        'NumCatSegrGiuntaReg' => true,
        'NumCatResCoordPolInte' => true,
        'NumCatDirConTD' => true,
        'NumCatCapoSerSenior' => true,
        'NumCatRed30Mesi' => true,
        'NumCatCapoGab' => true,
        'NumCatSegrGenPres' => true,
        'NumCatAvvCoord' => true,
        'PercCatD' => true,
        'PercPO' => true,
        'PercAP' => true,
        'PercAltrePOAP' => true,
        'PercCatDirigente' => true,
        'PercCatC' => true,
        'PercCatB' => true,
        'PercCatSegConsReg' => true,
        'PercCatA' => true,
        'PercCatDirDip' => true,
        'PercCatCapoRedattore' => true,
        'PercCatSegrGiuntaReg' => true,
        'PercCatResCoordPolInte' => true,
        'PercCatDirConTD' => true,
        'PercCatCapoSerSenior' => true,
        'PercCatRed30Mesi' => true,
        'PercCatCapoGab' => true,
        'PercCatSegrGenPres' => true,
        'PercCatAvvCoord' => true,
        'IdDipartimento' => true,
        'Sezione_Id' => true,
        'Struttura_Id' => true,
        'IdProcesso' => true,
        'IdTipoProcesso' => true,
        'IdRilevamento' => true
    ];
}
