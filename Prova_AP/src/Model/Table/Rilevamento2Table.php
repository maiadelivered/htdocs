<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rilevamento2 Model
 *
 * @method \App\Model\Entity\Rilevamento2 get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rilevamento2 newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rilevamento2[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rilevamento2|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rilevamento2 patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rilevamento2[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rilevamento2 findOrCreate($search, callable $callback = null, $options = [])
 */
class Rilevamento2Table extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rilevamento2');
        $this->setDisplayField('IdRilevamento');
        $this->setPrimaryKey('IdRilevamento');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento', 'create');

        $validator
            ->scalar('Rilevamento')
            ->allowEmpty('Rilevamento');

        $validator
            ->date('Inizio')
            ->allowEmpty('Inizio');

        $validator
            ->date('Fine')
            ->allowEmpty('Fine');

        $validator
            ->scalar('Descrizione')
            ->allowEmpty('Descrizione');

        $validator
            ->scalar('Note')
            ->allowEmpty('Note');

        $validator
            ->integer('IdStato')
            ->allowEmpty('IdStato');

        $validator
            ->scalar('CheckIdStato')
            ->allowEmpty('CheckIdStato');

        $validator
            ->scalar('Creatore')
            ->allowEmpty('Creatore');

        return $validator;
    }
}
