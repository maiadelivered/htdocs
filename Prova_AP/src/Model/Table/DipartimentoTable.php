<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dipartimento Model
 *
 * @property \App\Model\Table\RilevamentosTable|\Cake\ORM\Association\BelongsTo $Rilevamentos
 *
 * @method \App\Model\Entity\Dipartimento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Dipartimento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Dipartimento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dipartimento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dipartimento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Dipartimento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dipartimento findOrCreate($search, callable $callback = null, $options = [])
 */
class DipartimentoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dipartimento');
        $this->setDisplayField('IdDipartimento');
        $this->setPrimaryKey('IdDipartimento');

        $this->belongsTo('Rilevamentos', [
            'foreignKey' => 'Rilevamento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdDipartimento')
            ->allowEmpty('IdDipartimento', 'create');

        $validator
            ->scalar('NomeDipartimento')
            ->allowEmpty('NomeDipartimento');

        $validator
            ->scalar('NomeDirettore')
            ->allowEmpty('NomeDirettore');

        $validator
            ->integer('IdDirettore')
            ->allowEmpty('IdDirettore');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['Rilevamento_id'], 'Rilevamentos'));

        return $rules;
    }
}
