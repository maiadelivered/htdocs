<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vlistfase Model
 *
 * @property \App\Model\Table\StrutturaTable|\Cake\ORM\Association\BelongsTo $Struttura
 * @property \App\Model\Table\ProcessoTable|\Cake\ORM\Association\BelongsTo $Processo
 *
 * @method \App\Model\Entity\Vlistfase get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vlistfase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vlistfase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vlistfase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vlistfase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistfase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistfase findOrCreate($search, callable $callback = null, $options = [])
 */
class VlistfaseTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vlistfase');

        $this->belongsTo('Struttura', [
            'foreignKey' => 'Struttura_id'
        ]);
        $this->belongsTo('Processo', [
            'foreignKey' => 'Processo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('CheckIdStruttura')
            ->allowEmpty('CheckIdStruttura');

        $validator
            ->integer('IdFase')
            ->requirePresence('IdFase', 'create')
            ->notEmpty('IdFase');

        $validator
            ->scalar('NomeFase')
            ->allowEmpty('NomeFase');

        $validator
            ->scalar('RiferimentiNormativi')
            ->allowEmpty('RiferimentiNormativi');

        $validator
            ->scalar('TipoEventoAvvio')
            ->allowEmpty('TipoEventoAvvio');

        $validator
            ->scalar('TipoOutput')
            ->allowEmpty('TipoOutput');

        $validator
            ->scalar('AltreUnitaOrganizzativeCoinvolte')
            ->allowEmpty('AltreUnitaOrganizzativeCoinvolte');

        $validator
            ->scalar('Periodicita')
            ->allowEmpty('Periodicita');

        $validator
            ->scalar('Gennaio')
            ->allowEmpty('Gennaio');

        $validator
            ->scalar('Febbraio')
            ->allowEmpty('Febbraio');

        $validator
            ->scalar('Marzo')
            ->allowEmpty('Marzo');

        $validator
            ->scalar('Aprile')
            ->allowEmpty('Aprile');

        $validator
            ->scalar('Maggio')
            ->allowEmpty('Maggio');

        $validator
            ->scalar('Giugno')
            ->allowEmpty('Giugno');

        $validator
            ->scalar('Luglio')
            ->allowEmpty('Luglio');

        $validator
            ->scalar('Agosto')
            ->allowEmpty('Agosto');

        $validator
            ->scalar('Settembre')
            ->allowEmpty('Settembre');

        $validator
            ->scalar('Ottobre')
            ->allowEmpty('Ottobre');

        $validator
            ->scalar('Novembre')
            ->allowEmpty('Novembre');

        $validator
            ->scalar('Dicembre')
            ->allowEmpty('Dicembre');

        $validator
            ->integer('NumMedioGiorniComplet')
            ->allowEmpty('NumMedioGiorniComplet');

        $validator
            ->integer('NumIterazioniAnnue')
            ->allowEmpty('NumIterazioniAnnue');

        $validator
            ->integer('TotaleGiornateLavoroAnnue')
            ->allowEmpty('TotaleGiornateLavoroAnnue');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        $validator
            ->scalar('CheckIdProcesso')
            ->allowEmpty('CheckIdProcesso');

        $validator
            ->scalar('NomeSezione')
            ->allowEmpty('NomeSezione');

        $validator
            ->scalar('NomeDipartimento')
            ->allowEmpty('NomeDipartimento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['Struttura_id'], 'Struttura'));
        $rules->add($rules->existsIn(['Processo_id'], 'Processo'));

        return $rules;
    }
}
