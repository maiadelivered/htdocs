<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Processo Model
 *
 * @method \App\Model\Entity\Processo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Processo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Processo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Processo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Processo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Processo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Processo findOrCreate($search, callable $callback = null, $options = [])
 */
class ProcessoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('processo');
        $this->setDisplayField('IdProcesso');
        $this->setPrimaryKey('IdProcesso');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdProcesso')
            ->allowEmpty('IdProcesso', 'create');

        $validator
            ->integer('TipoProcesso_Id')
            ->allowEmpty('TipoProcesso_Id');

        $validator
            ->scalar('CheckIdTipoProcesso')
            ->allowEmpty('CheckIdTipoProcesso');

        $validator
            ->scalar('NomeProcesso')
            ->allowEmpty('NomeProcesso');

        $validator
            ->scalar('DescrizioneProcesso')
            ->allowEmpty('DescrizioneProcesso');

        $validator
            ->scalar('NomeFunzione')
            ->allowEmpty('NomeFunzione');

        $validator
            ->integer('Struttura_Id')
            ->allowEmpty('Struttura_Id');

        $validator
            ->scalar('CheckIdStruttura')
            ->allowEmpty('CheckIdStruttura');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        $validator
            ->allowEmpty('Note');

        return $validator;
    }
}
