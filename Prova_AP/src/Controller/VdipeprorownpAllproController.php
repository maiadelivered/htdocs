<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * VdipeprorownpAllpro Controller
 *
 * @property \App\Model\Table\VdipeprorownpAllproTable $VdipeprorownpAllpro
 *
 * @method \App\Model\Entity\VdipeprorownpAllpro[] paginate($object = null, array $settings = [])
 */
class VdipeprorownpAllproController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $vdipeprorownpAllpro = $this->paginate($this->VdipeprorownpAllpro);

        $this->set(compact('vdipeprorownpAllpro'));
        $this->set('_serialize', ['vdipeprorownpAllpro']);
    }

    /**
     * View method
     *
     * @param string|null $id Vdipeprorownp Allpro id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vdipeprorownpAllpro = $this->VdipeprorownpAllpro->get($id, [
            'contain' => []
        ]);

        $this->set('vdipeprorownpAllpro', $vdipeprorownpAllpro);
        $this->set('_serialize', ['vdipeprorownpAllpro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vdipeprorownpAllpro = $this->VdipeprorownpAllpro->newEntity();
        if ($this->request->is('post')) {
            $vdipeprorownpAllpro = $this->VdipeprorownpAllpro->patchEntity($vdipeprorownpAllpro, $this->request->getData());
            if ($this->VdipeprorownpAllpro->save($vdipeprorownpAllpro)) {
                $this->Flash->success(__('The vdipeprorownp allpro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vdipeprorownp allpro could not be saved. Please, try again.'));
        }
        $this->set(compact('vdipeprorownpAllpro'));
        $this->set('_serialize', ['vdipeprorownpAllpro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vdipeprorownp Allpro id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vdipeprorownpAllpro = $this->VdipeprorownpAllpro->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vdipeprorownpAllpro = $this->VdipeprorownpAllpro->patchEntity($vdipeprorownpAllpro, $this->request->getData());
            if ($this->VdipeprorownpAllpro->save($vdipeprorownpAllpro)) {
                $this->Flash->success(__('The vdipeprorownp allpro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vdipeprorownp allpro could not be saved. Please, try again.'));
        }
        $this->set(compact('vdipeprorownpAllpro'));
        $this->set('_serialize', ['vdipeprorownpAllpro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vdipeprorownp Allpro id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vdipeprorownpAllpro = $this->VdipeprorownpAllpro->get($id);
        if ($this->VdipeprorownpAllpro->delete($vdipeprorownpAllpro)) {
            $this->Flash->success(__('The vdipeprorownp allpro has been deleted.'));
        } else {
            $this->Flash->error(__('The vdipeprorownp allpro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
