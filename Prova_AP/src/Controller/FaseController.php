<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fase Controller
 *
 * @property \App\Model\Table\FaseTable $Fase
 *
 * @method \App\Model\Entity\Fase[] paginate($object = null, array $settings = [])
 */
class FaseController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Struttura', 'Processo']
        ];
        $fase = $this->paginate($this->Fase);

        $this->set(compact('fase'));
        $this->set('_serialize', ['fase']);
    }

    /**
     * View method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fase = $this->Fase->get($id, [
            'contain' => ['Struttura', 'Processo', 'Dipendente']
        ]);

        $this->set('fase', $fase);
        $this->set('_serialize', ['fase']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fase = $this->Fase->newEntity();
        if ($this->request->is('post')) {
            $fase = $this->Fase->patchEntity($fase, $this->request->getData());
            if ($this->Fase->save($fase)) {
                $this->Flash->success(__('The fase has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fase could not be saved. Please, try again.'));
        }
        $struttura = $this->Fase->Struttura->find('list', ['limit' => 200]);
        $processo = $this->Fase->Processo->find('list', ['limit' => 200]);
        $dipendente = $this->Fase->Dipendente->find('list', ['limit' => 200]);
        $this->set(compact('fase', 'struttura', 'processo', 'dipendente'));
        $this->set('_serialize', ['fase']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fase = $this->Fase->get($id, [
            'contain' => ['Dipendente']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fase = $this->Fase->patchEntity($fase, $this->request->getData());
            if ($this->Fase->save($fase)) {
                $this->Flash->success(__('The fase has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fase could not be saved. Please, try again.'));
        }
        $struttura = $this->Fase->Struttura->find('list', ['limit' => 200]);
        $processo = $this->Fase->Processo->find('list', ['limit' => 200]);
        $dipendente = $this->Fase->Dipendente->find('list', ['limit' => 200]);
        $this->set(compact('fase', 'struttura', 'processo', 'dipendente'));
        $this->set('_serialize', ['fase']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fase = $this->Fase->get($id);
        if ($this->Fase->delete($fase)) {
            $this->Flash->success(__('The fase has been deleted.'));
        } else {
            $this->Flash->error(__('The fase could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
