<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Fase;


/**
 * Processo Controller
 *
 * @property \App\Model\Table\ProcessoTable $Processo
 *
 * @method \App\Model\Entity\Processo[] paginate($object = null, array $settings = [])
 */
class ProcessoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $processo = $this->paginate($this->Processo);

        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);
    }

    /**
     * View method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $processo = $this->Processo->get($id, [
            'contain' => []
        ]);


        $this->set('processo', $processo);
        $this->set('_serialize', ['processo']);
      //  $processoFiltered = $this->Processo->find('all', array('conditions' => array('Nomeprocesso LIKE' => $this->Processo->get($id))));

        $this->loadModel('Fase');
        $FaseFiltered= $this->Fase->find('all', array('conditions' => array('processo_id = ' => ($id))));

        $this->set('fase', $FaseFiltered);
        $this->set('_serialize', ['fase']);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $processo = $this->Processo->newEntity();
        if ($this->request->is('post')) {
            $processo = $this->Processo->patchEntity($processo, $this->request->getData());
            if ($this->Processo->save($processo)) {
                $this->Flash->success(__('The processo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The processo could not be saved. Please, try again.'));
        }
        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $processo = $this->Processo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $processo = $this->Processo->patchEntity($processo, $this->request->getData());
            if ($this->Processo->save($processo)) {
                $this->Flash->success(__('The processo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The processo could not be saved. Please, try again.'));
        }
        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $processo = $this->Processo->get($id);
        if ($this->Processo->delete($processo)) {
            $this->Flash->success(__('The processo has been deleted.'));
        } else {
            $this->Flash->error(__('The processo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
