<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dipartimento Controller
 *
 * @property \App\Model\Table\DipartimentoTable $Dipartimento
 *
 * @method \App\Model\Entity\Dipartimento[] paginate($object = null, array $settings = [])
 */
class DipartimentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Rilevamentos']
        ];
        $dipartimento = $this->paginate($this->Dipartimento);

        $this->set(compact('dipartimento'));
        $this->set('_serialize', ['dipartimento']);
    }

    /**
     * View method
     *
     * @param string|null $id Dipartimento id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dipartimento = $this->Dipartimento->get($id, [
            'contain' => ['Rilevamentos']
        ]);

        $this->set('dipartimento', $dipartimento);
        $this->set('_serialize', ['dipartimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dipartimento = $this->Dipartimento->newEntity();
        if ($this->request->is('post')) {
            $dipartimento = $this->Dipartimento->patchEntity($dipartimento, $this->request->getData());
            if ($this->Dipartimento->save($dipartimento)) {
                $this->Flash->success(__('The dipartimento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dipartimento could not be saved. Please, try again.'));
        }
        $rilevamentos = $this->Dipartimento->Rilevamentos->find('list', ['limit' => 200]);
        $this->set(compact('dipartimento', 'rilevamentos'));
        $this->set('_serialize', ['dipartimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dipartimento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dipartimento = $this->Dipartimento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dipartimento = $this->Dipartimento->patchEntity($dipartimento, $this->request->getData());
            if ($this->Dipartimento->save($dipartimento)) {
                $this->Flash->success(__('The dipartimento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dipartimento could not be saved. Please, try again.'));
        }
        $rilevamentos = $this->Dipartimento->Rilevamentos->find('list', ['limit' => 200]);
        $this->set(compact('dipartimento', 'rilevamentos'));
        $this->set('_serialize', ['dipartimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dipartimento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dipartimento = $this->Dipartimento->get($id);
        if ($this->Dipartimento->delete($dipartimento)) {
            $this->Flash->success(__('The dipartimento has been deleted.'));
        } else {
            $this->Flash->error(__('The dipartimento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
