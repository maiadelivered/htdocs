<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rilevamento2 Controller
 *
 * @property \App\Model\Table\Rilevamento2Table $Rilevamento2
 *
 * @method \App\Model\Entity\Rilevamento2[] paginate($object = null, array $settings = [])
 */
class Rilevamento2Controller extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $rilevamento2 = $this->paginate($this->Rilevamento2);

        $this->set(compact('rilevamento2'));
        $this->set('_serialize', ['rilevamento2']);
    }

    /**
     * View method
     *
     * @param string|null $id Rilevamento2 id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rilevamento2 = $this->Rilevamento2->get($id, [
            'contain' => []
        ]);

        $this->set('rilevamento2', $rilevamento2);
        $this->set('_serialize', ['rilevamento2']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rilevamento2 = $this->Rilevamento2->newEntity();
        if ($this->request->is('post')) {
            $rilevamento2 = $this->Rilevamento2->patchEntity($rilevamento2, $this->request->getData());
            if ($this->Rilevamento2->save($rilevamento2)) {
                $this->Flash->success(__('The rilevamento2 has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rilevamento2 could not be saved. Please, try again.'));
        }
        $this->set(compact('rilevamento2'));
        $this->set('_serialize', ['rilevamento2']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rilevamento2 id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rilevamento2 = $this->Rilevamento2->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rilevamento2 = $this->Rilevamento2->patchEntity($rilevamento2, $this->request->getData());
            if ($this->Rilevamento2->save($rilevamento2)) {
                $this->Flash->success(__('The rilevamento2 has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rilevamento2 could not be saved. Please, try again.'));
        }
        $this->set(compact('rilevamento2'));
        $this->set('_serialize', ['rilevamento2']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rilevamento2 id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rilevamento2 = $this->Rilevamento2->get($id);
        if ($this->Rilevamento2->delete($rilevamento2)) {
            $this->Flash->success(__('The rilevamento2 has been deleted.'));
        } else {
            $this->Flash->error(__('The rilevamento2 could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
