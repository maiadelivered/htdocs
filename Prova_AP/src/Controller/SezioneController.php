<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Sezione Controller
 *
 * @property \App\Model\Table\SezioneTable $Sezione
 *
 * @method \App\Model\Entity\Sezione[] paginate($object = null, array $settings = [])
 */
class SezioneController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dipartimento']
        ];
        $sezione = $this->paginate($this->Sezione);

        $this->set(compact('sezione'));
        $this->set('_serialize', ['sezione']);
    }

    /**
     * View method
     *
     * @param string|null $id Sezione id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sezione = $this->Sezione->get($id, [
            'contain' => ['Dipartimento']
        ]);

        $this->set('sezione', $sezione);
        $this->set('_serialize', ['sezione']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sezione = $this->Sezione->newEntity();
        if ($this->request->is('post')) {
            $sezione = $this->Sezione->patchEntity($sezione, $this->request->getData());
            if ($this->Sezione->save($sezione)) {
                $this->Flash->success(__('The sezione has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sezione could not be saved. Please, try again.'));
        }
        $dipartimento = $this->Sezione->Dipartimento->find('list', ['limit' => 200]);
        $this->set(compact('sezione', 'dipartimento'));
        $this->set('_serialize', ['sezione']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sezione id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sezione = $this->Sezione->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sezione = $this->Sezione->patchEntity($sezione, $this->request->getData());
            if ($this->Sezione->save($sezione)) {
                $this->Flash->success(__('The sezione has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sezione could not be saved. Please, try again.'));
        }
        $dipartimento = $this->Sezione->Dipartimento->find('list', ['limit' => 200]);
        $this->set(compact('sezione', 'dipartimento'));
        $this->set('_serialize', ['sezione']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sezione id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sezione = $this->Sezione->get($id);
        if ($this->Sezione->delete($sezione)) {
            $this->Flash->success(__('The sezione has been deleted.'));
        } else {
            $this->Flash->error(__('The sezione could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
