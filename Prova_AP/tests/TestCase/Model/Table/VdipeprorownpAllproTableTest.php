<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VdipeprorownpAllproTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VdipeprorownpAllproTable Test Case
 */
class VdipeprorownpAllproTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VdipeprorownpAllproTable
     */
    public $VdipeprorownpAllpro;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vdipeprorownp_allpro'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('VdipeprorownpAllpro') ? [] : ['className' => VdipeprorownpAllproTable::class];
        $this->VdipeprorownpAllpro = TableRegistry::get('VdipeprorownpAllpro', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VdipeprorownpAllpro);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
