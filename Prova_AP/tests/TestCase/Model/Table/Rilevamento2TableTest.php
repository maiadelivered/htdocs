<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Rilevamento2Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Rilevamento2Table Test Case
 */
class Rilevamento2TableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\Rilevamento2Table
     */
    public $Rilevamento2;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rilevamento2'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Rilevamento2') ? [] : ['className' => Rilevamento2Table::class];
        $this->Rilevamento2 = TableRegistry::get('Rilevamento2', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rilevamento2);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
