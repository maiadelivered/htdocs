<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DipartimentoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DipartimentoTable Test Case
 */
class DipartimentoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DipartimentoTable
     */
    public $Dipartimento;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dipartimento',
        'app.rilevamentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Dipartimento') ? [] : ['className' => DipartimentoTable::class];
        $this->Dipartimento = TableRegistry::get('Dipartimento', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dipartimento);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
