<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rilevamento Entity
 *
 * @property int $IdRilevamento
 * @property string $Rilevamento
 * @property \Cake\I18n\FrozenTime $StartDate
 * @property \Cake\I18n\FrozenTime $EndDate
 * @property string $Descrizione
 * @property string $Note
 * @property int $IdStato
 * @property string $CheckIdStato
 * @property string $Creatore
 */
class Rilevamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Rilevamento' => true,
        'StartDate' => true,
        'EndDate' => true,
        'Descrizione' => true,
        'Note' => true,
        'IdStato' => true,
        'CheckIdStato' => true,
        'Creatore' => true
    ];
}
