<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Processo Entity
 *
 * @property int $IdProcesso
 * @property int $TipoProcesso_Id
 * @property string $CheckIdTipoProcesso
 * @property string $NomeProcesso
 * @property string $DescrizioneProcesso
 * @property string $NomeFunzione
 * @property int $Struttura_Id
 * @property string $CheckIdStruttura
 * @property int $IdRilevamento
 * @property string|resource $Note
 */
class Processo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'TipoProcesso_Id' => true,
        'CheckIdTipoProcesso' => true,
        'NomeProcesso' => true,
        'DescrizioneProcesso' => true,
        'NomeFunzione' => true,
        'Struttura_Id' => true,
        'CheckIdStruttura' => true,
        'IdRilevamento' => true,
        'Note' => true
    ];
}
