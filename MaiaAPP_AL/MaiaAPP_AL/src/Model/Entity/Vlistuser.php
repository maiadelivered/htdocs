<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vlistuser Entity
 *
 * @property int $id_user
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $address
 * @property int $idDipartimento
 * @property string $CheckIdDipartimento
 * @property int $idSezione
 * @property string $checkIdSezione
 * @property int $idrilevamento
 * @property \Cake\I18n\FrozenTime $birthdate
 * @property string $community
 * @property string $gender
 * @property string $language
 * @property string $location
 * @property string $short_bio
 * @property string $surname
 * @property string $username
 */
class Vlistuser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id_user' => true,
        'name' => true,
        'email' => true,
        'password' => true,
        'address' => true,
        'idDipartimento' => true,
        'CheckIdDipartimento' => true,
        'idSezione' => true,
        'checkIdSezione' => true,
        'idrilevamento' => true,
        'birthdate' => true,
        'community' => true,
        'gender' => true,
        'language' => true,
        'location' => true,
        'short_bio' => true,
        'surname' => true,
        'username' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
