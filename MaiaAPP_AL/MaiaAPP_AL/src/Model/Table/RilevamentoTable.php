<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rilevamento Model
 *
 * @method \App\Model\Entity\Rilevamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rilevamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rilevamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rilevamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rilevamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rilevamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rilevamento findOrCreate($search, callable $callback = null, $options = [])
 */
class RilevamentoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rilevamento');
        $this->setDisplayField('IdRilevamento');
        $this->setPrimaryKey('IdRilevamento');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento', 'create');

        $validator
            ->scalar('Rilevamento')
            ->requirePresence('Rilevamento', 'create')
            ->notEmpty('Rilevamento');

        $validator
            ->dateTime('StartDate')
            ->allowEmpty('StartDate');

        $validator
            ->dateTime('EndDate')
            ->allowEmpty('EndDate');

        $validator
            ->scalar('Descrizione')
            ->allowEmpty('Descrizione');

        $validator
            ->scalar('Note')
            ->allowEmpty('Note');

        $validator
            ->integer('IdStato')
            ->allowEmpty('IdStato');

        $validator
            ->scalar('CheckIdStato')
            ->allowEmpty('CheckIdStato');

        $validator
            ->scalar('Creatore')
            ->allowEmpty('Creatore');

        return $validator;
    }
}
