<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vlistprocesso Model
 *
 * @method \App\Model\Entity\Vlistprocesso get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vlistprocesso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vlistprocesso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vlistprocesso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vlistprocesso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistprocesso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistprocesso findOrCreate($search, callable $callback = null, $options = [])
 */
class VlistprocessoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vlistprocesso');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdProcesso')
            ->requirePresence('IdProcesso', 'create')
            ->notEmpty('IdProcesso');

        $validator
            ->integer('TipoProcesso_Id')
            ->allowEmpty('TipoProcesso_Id');

        $validator
            ->scalar('NomeProcesso')
            ->allowEmpty('NomeProcesso');

        $validator
            ->scalar('DescrizioneProcesso')
            ->allowEmpty('DescrizioneProcesso');

        $validator
            ->scalar('NomeFunzione')
            ->allowEmpty('NomeFunzione');

        $validator
            ->integer('Struttura_Id')
            ->allowEmpty('Struttura_Id');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        $validator
            ->allowEmpty('Note');

        $validator
            ->scalar('CheckIdTipoProcesso')
            ->allowEmpty('CheckIdTipoProcesso');

        $validator
            ->scalar('CheckIdStruttura')
            ->allowEmpty('CheckIdStruttura');

        $validator
            ->integer('IdSezione')
            ->requirePresence('IdSezione', 'create')
            ->notEmpty('IdSezione');

        $validator
            ->scalar('CheckIdSezione')
            ->allowEmpty('CheckIdSezione');

        $validator
            ->integer('IdDipartimento')
            ->requirePresence('IdDipartimento', 'create')
            ->notEmpty('IdDipartimento');

        $validator
            ->scalar('CheckIdDipartimento')
            ->allowEmpty('CheckIdDipartimento');

        return $validator;
    }
}
