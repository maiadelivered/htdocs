<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Struttura Model
 *
 * @property \App\Model\Table\DipendenteTable|\Cake\ORM\Association\BelongsToMany $Dipendente
 *
 * @method \App\Model\Entity\Struttura get($primaryKey, $options = [])
 * @method \App\Model\Entity\Struttura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Struttura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Struttura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Struttura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Struttura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Struttura findOrCreate($search, callable $callback = null, $options = [])
 */
class StrutturaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('struttura');
        $this->setDisplayField('IdStruttura');
        $this->setPrimaryKey('IdStruttura');

        $this->belongsToMany('Dipendente', [
            'foreignKey' => 'struttura_id',
            'targetForeignKey' => 'dipendente_id',
            'joinTable' => 'dipendente_struttura'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdStruttura')
            ->allowEmpty('IdStruttura', 'create');

        $validator
            ->integer('Dipartimento_Id')
            ->requirePresence('Dipartimento_Id', 'create')
            ->notEmpty('Dipartimento_Id');

        $validator
            ->integer('Sezione_Id')
            ->requirePresence('Sezione_Id', 'create')
            ->notEmpty('Sezione_Id');

        $validator
            ->scalar('CheckIdSezione')
            ->allowEmpty('CheckIdSezione');

        $validator
            ->scalar('CheckIdDipartimento')
            ->allowEmpty('CheckIdDipartimento');

        $validator
            ->scalar('NomeStruttura')
            ->allowEmpty('NomeStruttura');

        $validator
            ->scalar('NomeDirigente')
            ->allowEmpty('NomeDirigente');

        $validator
            ->scalar('Email')
            ->allowEmpty('Email');

        $validator
            ->integer('IdDirigente')
            ->allowEmpty('IdDirigente');

        return $validator;
    }
}
