<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Vlistusers Controller
 *
 * @property \App\Model\Table\VlistusersTable $Vlistusers
 *
 * @method \App\Model\Entity\Vlistuser[] paginate($object = null, array $settings = [])
 */
class VlistusersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $vlistusers = $this->paginate($this->Vlistusers);

        $this->set(compact('vlistusers'));
        $this->set('_serialize', ['vlistusers']);
    }

    /**
     * View method
     *
     * @param string|null $id Vlistuser id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vlistuser = $this->Vlistusers->get($id, [
            'contain' => []
        ]);

        $this->set('vlistuser', $vlistuser);
        $this->set('_serialize', ['vlistuser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vlistuser = $this->Vlistusers->newEntity();
        if ($this->request->is('post')) {
            $vlistuser = $this->Vlistusers->patchEntity($vlistuser, $this->request->getData());
            if ($this->Vlistusers->save($vlistuser)) {
                $this->Flash->success(__('The vlistuser has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vlistuser could not be saved. Please, try again.'));
        }
        $this->set(compact('vlistuser'));
        $this->set('_serialize', ['vlistuser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vlistuser id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vlistuser = $this->Vlistusers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vlistuser = $this->Vlistusers->patchEntity($vlistuser, $this->request->getData());
            if ($this->Vlistusers->save($vlistuser)) {
                $this->Flash->success(__('The vlistuser has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vlistuser could not be saved. Please, try again.'));
        }
        $this->set(compact('vlistuser'));
        $this->set('_serialize', ['vlistuser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vlistuser id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vlistuser = $this->Vlistusers->get($id);
        if ($this->Vlistusers->delete($vlistuser)) {
            $this->Flash->success(__('The vlistuser has been deleted.'));
        } else {
            $this->Flash->error(__('The vlistuser could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


}
