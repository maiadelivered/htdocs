<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Session;
use Cake\ORM\Behavior;
use Cake\Collection\Collection;
use Cake\Auth;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function login()
    {
        if ($this->request->is('post')) {

            $user = $this->Auth->identify();


            if($user)
            {

                $this->Auth->setUser($user);
                //ACQUISISCO I PARAMETRI SEZIONE E DIPARTIMENTO DA UTENTE
                $idSezione = $this->Auth->user('idSezione');
                $idDipartimento = $this->Auth->user('idDipartimento');
                $idUtente = $this->Auth->user('id_user');

                $this->loadModel('vlistusers');
                $query = $this->vlistusers->find('all',array('conditions' => array('idSezione' => $idSezione,
                    'idDipartimento' => $idDipartimento, 'id_user' => $idUtente), 'fields' => array('CheckIdSezione', 'CheckIdDipartimento')));

                $nomeSezione = (string)$query->first()->CheckIdSezione;
                $nomeDipartimento = (string)$query->first()->CheckIdDipartimento;
                //scrivo NELLA SESSIONE
                $this->request->session()->write('User.idSezione',$idSezione);
                $this->request->session()->write('User.idDipartimento',$idDipartimento);
                $this->request->session()->write('Sezione.nomeSezione',$nomeSezione);
                $this->request->session()->write('Dipartimento.nomeDipartimento',$nomeDipartimento);
                $this->set(compact('user'));
                $this->set('_serialize', ['user']);

                //return $this->redirect(['controller'=>'processo']);
                return $this->redirect(['controller'=>'rilevamento']);
               // return $this->redirect('/');

            }
            else{


                //bad login

                $this->Flash->error('username o password non validi');
                $this->redirect(['controller' => 'Pages', 'action' => 'display']);
            }

        }
    }
    public function logout() {
        $this->Flash->success("Arrivederci");
        $this->Auth->logout();
       $this->request->getSession()->destroy();
    // $this->redirect($this->Auth->logout());
        $this->redirect(['controller' => 'Pages', 'action' => 'display']);

    }
    public function register()
    {
        $user = $this->Users->newEntity();
        if($this->request->is('post')){
            $user=$this->Users->patchEntity($user, $this->request->getData());
            if($this->Users->save($user)) {
                $this->Flash->success('Sei registrato e puoi fare il login');
                return $this->redirect(['action'=>'login']);
            }else
            {
                $this->Flash->error("non sei registrato");
            }
        }
        $this->set(compact('uses'));
        $this->set('_serialize',['user']);

    }
    public function beforeFilter(Event $event)
    {
       $this->Auth->allow('login');
      //  $this->Auth->allow('users');


    }
}
