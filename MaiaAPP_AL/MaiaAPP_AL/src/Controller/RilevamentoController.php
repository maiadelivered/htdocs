<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rilevamento Controller
 *
 * @property \App\Model\Table\RilevamentoTable $Rilevamento
 *
 * @method \App\Model\Entity\Rilevamento[] paginate($object = null, array $settings = [])
 */
class RilevamentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $rilevamento = $this->paginate($this->Rilevamento);

        $this->set(compact('rilevamento'));
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * View method
     *
     * @param string|null $id Rilevamento id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function SetRilevamento($id = null)
    {

        $this->loadModel('rilevamento');
        $passedArgs = $this->request->getData('Id');
        $passedArgs = $id;

        $rilevamentoFiltered = $this->rilevamento->find('all', array('conditions' => array('IdRilevamento  =' => $passedArgs)));

        $this->request->session()->write('IdRilevamento', $rilevamentoFiltered->first()->IdRilevamento);
        $this->request->session()->write('StatoRilevamento', $rilevamentoFiltered->first()->IdStato);

        $this->redirect('/processo');

    }

    public function view($id = null)
    {
        $rilevamento = $this->Rilevamento->get($id, [
            'contain' => []
        ]);

        $this->set('rilevamento', $rilevamento);
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rilevamento = $this->Rilevamento->newEntity();
        if ($this->request->is('post')) {
            $rilevamento = $this->Rilevamento->patchEntity($rilevamento, $this->request->getData());
            if ($this->Rilevamento->save($rilevamento)) {
                $this->Flash->success(__('The rilevamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rilevamento could not be saved. Please, try again.'));
        }
        $this->set(compact('rilevamento'));
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rilevamento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rilevamento = $this->Rilevamento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rilevamento = $this->Rilevamento->patchEntity($rilevamento, $this->request->getData());
            if ($this->Rilevamento->save($rilevamento)) {
                $this->Flash->success(__('The rilevamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rilevamento could not be saved. Please, try again.'));
        }
        $this->set(compact('rilevamento'));
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rilevamento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rilevamento = $this->Rilevamento->get($id);
        if ($this->Rilevamento->delete($rilevamento)) {
            $this->Flash->success(__('The rilevamento has been deleted.'));
        } else {
            $this->Flash->error(__('The rilevamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
