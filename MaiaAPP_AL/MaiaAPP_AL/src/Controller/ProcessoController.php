<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Rilevamento;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Session;

/**
 * Processo Controller
 *
 * @property \App\Model\Table\ProcessoTable $Processo
 *
 * @method \App\Model\Entity\Processo[] paginate($object = null, array $settings = [])
 */
class ProcessoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');

        $IdRilevamento = $this->request->session()->read('IdRilevamento');
        $IdStato = $this->request->session()->read('IdStato');
        //capisco se c'è già stata la scelta del rilevamento
        if($IdRilevamento == null)
        {
            return $this->redirect(['controller'=>'rilevamento']);
        }


        $this->loadModel('vlistprocesso');

        $conditions =  array('conditions'=>array('iddipartimento' =>  $idDipartimento,'idsezione' => $idSezione, 'IdRilevamento'=>$IdRilevamento ));
        $processoFiltered =  $this->vlistprocesso->find('all', $conditions);

        $processo = $this->paginate($processoFiltered);
        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);


    }

    /**
     * View method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $getID=isset($this->request->query['id']) ? $this->request->query['id'] : null;

        $processo = $this->Processo->get($getID, [
            'contain' => []
        ]);

        $this->set('processo', $processo);
        $this->set('_serialize', ['processo']);

        $this->loadModel('Fase');
        $FaseFiltered= $this->Fase->find('all', array('conditions' => array('processo_id = ' => ($getID))));


        $this->set('fase', $FaseFiltered);
        $this->set('_serialize', ['fase']);

        $this->loadModel('VdipeprorownpAllpro');
        $vdipeprorownpAllproFiltered= $this->VdipeprorownpAllpro->find('all', array('conditions' => array('Idprocesso = ' => ($getID))));
// debug($vdipeprorownpAllproFiltered->count());

        $this->set(compact('vdipeprorownpAllproFiltered'));
        $this->set('_serialize', ['vdipeprorownpAllproFiltered']);
    }

    public function SearchProcesso()
    {


        $this->loadModel('vlistprocesso');
        $passedArgs = $this->request->getData('descrizione');
        $processoFiltered = $this->vlistprocesso->find('all', array('conditions' => array('Nomeprocesso LIKE' => '%'.$passedArgs.'%')));

        $processo = $this->paginate($processoFiltered);
        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);

        $this->render('index');

    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        If ($StatoRilevamento>0)
        {
            $this->redirect('/rilevamentochiuso');
        }


        $this->loadModel('struttura');

        $CStrutturaFiltered = $this->struttura->find('list',[
                'keyField' => 'IdStruttura',
                'valueField' => 'NomeStruttura']
        )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione ]);

        $this->set(compact('CStrutturaFiltered'));

        // Tipo processo per la dropbox
        $this->loadModel('Tipoprocesso');
        $CTipoProcesso = $this->Tipoprocesso->find('list',[
            'keyField' => 'IdTipoProcesso',
            'valueField' => 'TipoProcesso'
        ]);
        $this->set(compact('CTipoProcesso'));


        $processo = $this->Processo->newEntity();
        if ($this->request->is('post')) {
            $processo = $this->Processo->patchEntity($processo, $this->request->getData());
            if ($this->Processo->save($processo)) {
                $this->Flash->success(__('The processo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The processo could not be saved. Please, try again.'));
        }
        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        if ($StatoRilevamento>0)
        {
            $this->redirect('/rilevamentochiuso');
        }


        $this->loadModel('struttura');
        $CStrutturaFiltered = $this->struttura->find('list',array('fields' =>array(
            'keyField' => 'IdStruttura',
            'valueField' => 'NomeStruttura'
        ), 'conditions'=> array('Dipartimento_Id' =>  $idDipartimento,'Sezione_Id' => $idSezione )));
        $CStrutturaFiltered = $this->struttura->find('list',[
                'keyField' => 'IdStruttura',
                'valueField' => 'NomeStruttura']
            )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione ]);

        $this->set(compact('CStrutturaFiltered'));


        // Tipo processo per la dropbox
        $this->loadModel('Tipoprocesso');
        $CTipoProcesso = $this->Tipoprocesso->find('list',[
            'keyField' => 'IdTipoProcesso',
            'valueField' => 'TipoProcesso'
        ]);
        $this->set(compact('CTipoProcesso'));


        $processo = $this->Processo->get($id, [
            'contain' => []
        ]);



        if ($this->request->is(['patch', 'post', 'put'])) {
            $processo = $this->Processo->patchEntity($processo, $this->request->getData());
            if ($this->Processo->save($processo)) {
                $this->Flash->success(__('The processo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The processo could not be saved. Please, try again.'));
        }
        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $processo = $this->Processo->get($id);

        // NOTA ho dovuto impostare  'quoteIdentifiers' => true, in app.php in config
        $conn = ConnectionManager::get('default');
        $SqlStored =  "call DeleteProcesso (" . $id . ")";
     //   debug($SqlStored);
        $results = $conn->execute($SqlStored);
        $this->Flash->success(__('Il processo è stato cancellato correttamente.'));
        /*if ($this->Processo->delete($processo))
        {
            $this->Flash->success(__('The processo has been deleted.'));
        } else {
            $this->Flash->error(__('The processo could not be deleted. Please, try again.'));
        }
*/
        return $this->redirect(['action' => 'index']);
    }
}
