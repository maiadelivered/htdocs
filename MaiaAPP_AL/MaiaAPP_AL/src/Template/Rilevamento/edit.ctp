<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento $rilevamento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $rilevamento->IdRilevamento],
                ['confirm' => __('Are you sure you want to delete # {0}?', $rilevamento->IdRilevamento)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Rilevamento'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="rilevamento form large-9 medium-8 columns content">
    <?= $this->Form->create($rilevamento) ?>
    <fieldset>
        <legend><?= __('Edit Rilevamento') ?></legend>
        <?php
            echo $this->Form->control('Rilevamento');
            echo $this->Form->control('StartDate', ['empty' => true]);
            echo $this->Form->control('EndDate', ['empty' => true]);
            echo $this->Form->control('Descrizione');
            echo $this->Form->control('Note');
            echo $this->Form->control('IdStato');
            echo $this->Form->control('CheckIdStato');
            echo $this->Form->control('Creatore');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
