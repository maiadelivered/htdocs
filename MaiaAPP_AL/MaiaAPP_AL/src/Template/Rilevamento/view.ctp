<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento $rilevamento
 */
?>
</div>
<div class="col-lg-3">
    <ul class="well">

        <li><?= $this->Html->link(__('Edit Rilevamento'), ['action' => 'edit', $rilevamento->IdRilevamento]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rilevamento'), ['action' => 'delete', $rilevamento->IdRilevamento], ['confirm' => __('Are you sure you want to delete # {0}?', $rilevamento->IdRilevamento)]) ?> </li>
        <li><?= $this->Html->link(__('List Rilevamento'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rilevamento'), ['action' => 'add']) ?> </li>
    </ul>
</div>

<div class="col-lg-8">

    <p class="bs-component">
    <div class="processo view large-9 medium-8 columns content">

        <table class="vertical-table">

        <h3><?= h($rilevamento->IdRilevamento) ?></h3>
    <table class="table table-striped table-hover ">
        <tr>
            <th scope="row"><?= __('Rilevamento') ?></th>
            <td><?= h($rilevamento->Rilevamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descrizione') ?></th>
            <td><?= h($rilevamento->Descrizione) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Note') ?></th>
            <td><?= h($rilevamento->Note) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdStato') ?></th>
            <td><?= h($rilevamento->CheckIdStato) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creatore') ?></th>
            <td><?= h($rilevamento->Creatore) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdRilevamento') ?></th>
            <td><?= $this->Number->format($rilevamento->IdRilevamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdStato') ?></th>
            <td><?= $this->Number->format($rilevamento->IdStato) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('StartDate') ?></th>
            <td><?= h($rilevamento->StartDate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('EndDate') ?></th>
            <td><?= h($rilevamento->EndDate) ?></td>
        </tr>
    </table>
</div>
</div>
