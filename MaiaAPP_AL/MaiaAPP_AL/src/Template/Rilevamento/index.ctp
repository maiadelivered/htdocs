<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento[]|\Cake\Collection\CollectionInterface $rilevamento
 */
use Cake\I18n\Date;
use Cake\Routing\Router;
?>
</div>
<nav class="navbar navbar-default" >
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Rilevamento</a>
        </div>
    </div>
</nav>
<table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
    <thead>
        <tr>
                <th scope="col" class="info"><?= $this->Paginator->sort('IdRilevamento')?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Nome Rilevamento') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Inizio') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Fine') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Descrizione') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Stato') ?></th>
                <th scope="col" class="info"><?= __('Selezione') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php

            foreach ($rilevamento as $rilevamento): ?>
            <tr>
                <td><?= $this->Number->format($rilevamento->IdRilevamento) ?></td>
                <td><?= h($rilevamento->Rilevamento) ?></td>
                <td><?= $this->Time->format($rilevamento->StartDate,'dd-MMMM-YYYY') ?></td>
                <td><?= $this->Time->format($rilevamento->EndDate,'dd-MMMM-YYYY')
                 ?></td>
                <td><?= h($rilevamento->Descrizione) ?></td>
                <td><?= h($rilevamento->CheckIdStato) ?></td>
                <td class="btn btn-primary btn-lg">

                    <?php  echo $this->Html->link(__('Seleziona'), ['action' => 'SetRilevamento', $rilevamento->IdRilevamento],array('class'=>'class="btn btn-primary btn-lg"')) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
