<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo $processo
 */
?>

    </div>
    <div class="col-lg-3">
        <ul class="well"><?= $this->Html->link(__('Elenco Processi'), ['action' => 'index']) ?> <br/>
           <?= $this->Html->link(__('Modifica Processo'), ['action' => 'edit', $processo->IdProcesso]) ?> <br/>
           <?= $this->Form->postLink(__('Cancella Processo'), ['action' => 'delete', $processo->IdProcesso], ['confirm' => __('Are you sure you want to delete # {0}?', $processo->IdProcesso)]) ?> </br>
           <?= $this->Html->link(__('Nuovo Processo'), ['action' => 'add']) ?> <br/>
        </ul>
    </div>


   <div class="col-lg-8">

    <p class="bs-component">
<div class="processo view large-9 medium-8 columns content">

    <table class="vertical-table">
        <tr>
            <th scope="row">
            <td><h1> PROCESSO:
                <?= h($processo->NomeProcesso) ?></h1> </td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descrizione Processo') ?></h4>
        <?php if(((string)($processo->DescrizioneProcesso)) == "0"): ?>

            <h5>Non è stata fornita descrizione per il processo selezionato </h5>

        <?php else:  ?>
            <?= $this->Text->autoParagraph(h($processo->DescrizioneProcesso)); ?>
        <?php endif; ?>

    </div>
    <div class="row">
        <h4><?= __('Nome Funzione') ?></h4>
        <?= $this->Text->autoParagraph(h($processo->NomeFunzione)); ?>
    </div>
           <div class="row">
               <h4><?= __('Tipo Processo') ?></h4>
               <?= $this->Text->autoParagraph(h($processo->CheckIdTipoProcesso)); ?>
           </div>
</div>


<div >
    <nav class="navbar navbar-default" >
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Fasi del processo</a>
            </div>
        </div>
    </nav>
    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col" class="info">Identificativo fase</th>
            <th scope="col" class="info">Struttura di afferenza</th>
            <th scope="col" class="info">Nome Fase</th>
            <th scope="col" class="info">Tipo Evento Avvio</th>
            <th scope="col" class="info">Tipologia di Output</th>
            <th scope="col" class="info">Altre Unità Organizzative Coinvolte</th>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($fase as $fase): ?>
            <tr>
                <td><?= h($fase->CheckIdFase) ?></td>
                <td><?= h($fase->CheckIdStruttura) ?></td>
                <td><?= h($fase->NomeFase) ?></td>
                <td><?= h($fase->TipoOutput) ?></td>
                <td><?= h($fase->TipoEventoAvvio) ?></td>
                <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


<?php
    if ($vdipeprorownpAllproFiltered->count() > 0) {
     ?>
        <div class="processo view large-9 medium-8 columns content">
            <nav class="navbar navbar-default" >
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Carichi di lavoro dichiarati sul processo</a>
                    </div>
                </div>
            </nav>

            <h2> Numero dipendenti totali impegnati sul processo: <?php echo($this->Number->format($vdipeprorownpAllproFiltered->first()->NumDipTot)) ?></h2>
            <table class="table table-striped table-hover ">
                <th scope="col" class="info">Tipologia contrattuale</th>
                <th scope="col" class="info">Numero</th>
                <th scope="col" class="info">Percentuale occupazione</th>
                <tr>
                    <th scope="row"><?= __('Numero Dirigenti') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirigente) ?></td>
                        <td><?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirigente) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Numero di dipendenti categoria D') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatD) ?></td>
                       <td> <?= h($vdipeprorownpAllproFiltered->first()->PercCatD) ?> </td>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Numero di PO') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumPO) ?></td>
                    <td>  <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercPO) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Numero di AP') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumAP) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercAP) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Numero altre Posizioni Organizzative') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumAltrePOAP) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercAltrePOAP) ?></td>
                </tr>

                <tr>
                    <th scope="row"><?= __('Numero Categorie C') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatC) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatC) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Numero Categorie B') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatB) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatB) ?></td>
                </tr>

                    <th scope="row"><?= __('Numero Categorie A') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatA) ?></td>
                <td>
                    <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatA) ?></td>
                </tr>
                 <?php if($vdipeprorownpAllproFiltered->first()->NumCatDirDip > 0): ?>
                <tr>
                    <th scope="row"><?= __('NumCatDirDip') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirDip) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirDip) ?></td>

                </tr>
                 <?php endif; ?>
                <?php if($vdipeprorownpAllproFiltered->first()->NumCatCapoRedattore > 0):?>
                <tr>
                    <th scope="row"><?= __('Numero Capo Redattore') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoRedattore) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoRedattore) ?></td>
                </tr>
                <?php endif; ?>
                <?php if ($vdipeprorownpAllproFiltered->first()->NumCatSegrGiuntaReg >0) :?>
                <tr>
                    <th scope="row"><?= __('Numero Segretaro di Giunta Regionale') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatSegrGiuntaReg) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatSegrGiuntaReg) ?></td>
                </tr>
                <?php endif; ?>
                <?php if ($vdipeprorownpAllproFiltered->first()->NumCatResCoordPolInte>0) :?>
                <tr>
                    <th scope="row"><?= __('Numero Responsabile Politiche interne') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatResCoordPolInte) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatResCoordPolInte) ?></td>
                </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirConTD)>0):?>
                <tr>
                    <th scope="row"><?= __('Numero Dirigenti Tempo Determinato') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirConTD) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirConTD) ?></td>
                </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoSerSenior)>0) :?>
                <tr>
                    <th scope="row"><?= __('NumCatCapoSerSenior') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoSerSenior) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoSerSenior) ?></td>
                </tr>
                <?php endif; ?>
                <?php if($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatRed30Mesi)>0) :?>
                <tr>
                    <th scope="row"><?= __('NumCatRed30Mesi') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatRed30Mesi) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatRed30Mesi) ?></td>
                </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoGab)>0): ?>
                <tr>
                    <th scope="row"><?= __('Numero Capo Gabinetto') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoGab) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoGab) ?></td>
                </tr>
                 <?php endif; ?>
               <?php if($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatSegrGenPres)>0) : ?>
                <tr>
                    <th scope="row"><?= __('Numero Segeretari Generali Presidenza') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatSegrGenPres) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatSegrGenPres) ?></td>
                </tr>
                <?php endif; ?>
                <?php if($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatAvvCoord)>0) :?>
                <tr>
                    <th scope="row"><?= __('NumCatAvvCoord') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatAvvCoord) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatAvvCoord) ?></td>
                </tr>
                <?php endif; ?>

                <!--
                <tr>
                    <th scope="row"><?= __('PercPO') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercPO) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercAP') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercAP) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercAltrePOAP') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercAltrePOAP) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatDirigente') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirigente) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatC') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatC) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatB') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatB) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatSegConsReg') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatSegConsReg) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatA') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatA) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatDirDip') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirDip) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatCapoRedattore') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoRedattore) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatSegrGiuntaReg') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatSegrGiuntaReg) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatResCoordPolInte') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatResCoordPolInte) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatDirConTD') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirConTD) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatCapoSerSenior') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoSerSenior) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatRed30Mesi') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatRed30Mesi) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatCapoGab') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoGab) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatSegrGenPres') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatSegrGenPres) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('PercCatAvvCoord') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatAvvCoord) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('IdDipartimento') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->IdDipartimento) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sezione_Id') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->Sezione_Id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Struttura_Id') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->Struttura_Id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('IdProcesso') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->IdProcesso) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('IdTipoProcesso') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->IdTipoProcesso) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('IdRilevamento') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->IdRilevamento) ?></td>
                </tr>

-->
            </table>

        </div>
        <?php
    }else {
        ?>
        <div class="processo view large-9 medium-8 columns content">
            <h3>Non sono presenti carichi di lavoro sul processo <?= h($processo->NomeProcesso) ?></h3>
        </div>
        </div>
        <?php
    }
    ?>

   </div>
