<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo[]|\Cake\Collection\CollectionInterface $processo
 */

use Cake\I18n\Time;
use Cake\Routing\Router;
?>

</div>
<nav class="navbar navbar-default" >
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><?= __('Processo');?></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <a href="#" id= "ImgSearch"  >
                    <img  src="img/search.jpg" height="40" width="40" ></h3>
                </a>
                <script>
                    $( "#ImgSearch" ).click(function() {
                        if($("#Divricerca").is(':hidden')){
                            $( "#Divricerca").show( "slow" );
                        }else {
                            $( "#Divricerca").hide("slow");
                        }
                    });
                </script>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav"><?= $this->Html->link(__('Nuovo'), ['action' => 'add']) ?></li>
                <li  class="nav navbar-nav"><?= $this->Html->link(__('List Fase'), ['controller' => 'Fase', 'action' => 'index']) ?></li>
                <li  class="nav navbar-nav"><?= $this->Html->link(__('New Fase'), ['controller' => 'Fase', 'action' => 'add']) ?></li>
            </ul>
        </div>
    </div>
</nav>

<div class="form-group" id="Divricerca" hidden="true" >
    <?= $this->Form->create('Post') ?>
    <fieldset>
        <legend><?= __('Ricerca Processo') ?></legend>
        <?php
        echo $this->Form->control('descrizione',
            array('type'=>'text','class'=>'form-control', 'size'=>30,
                'label'=> 'Inserisci il nome del processo da ricercare:', 'placeholder'=>'Nome processo'));
        ?>
    </fieldset>

    <?= $this->Form->button(
        'Ricerca',
        array('class'=>'btn btn-primary btn-lg',
            'formaction' => Router::url(
                array('controller' => 'processo','action' => 'SearchProcesso')
            )
        )
    );
    ?>

    <?= $this->Form->end() ?>
</div>
</div>
</div>


<div class="table table-striped table-hover " cellpadding="1" cellspacing="1">


    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('NomeProcesso') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Struttura di appartenenza') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Tipo Processo') ?></th>

                <th scope="col" class="actions" width="120" ><?= __('') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($processo as $processo): ?>
            <tr>
                <td><?= h($processo->NomeProcesso) ?></td>

                <td><?= h($processo->CheckIdStruttura) ?></td>
                <td><?= h($processo->CheckIdTipoProcesso) ?></td>
                <td class="actions">

                    <div class="btn-group">
                        <a href="#" class="btn btn-default">Azioni</a>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><?= $this->Html->link(__('View'),
                                    array('action' => 'view', '?'=> array('id'=>$processo->IdProcesso))); ?></li>
                            <li> <?= $this->Html->link(__('Edit'), ['action' => 'edit', $processo->IdProcesso]) ?></li>
                            <li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $processo->IdProcesso],
                                    ['confirm' => __(
                                        'Si è sicuri di voler eliminare il processo {0}? 
 L\'eliminazione del processo comporterà  l\'eliminazione delle relative fasi e i carichi di lavoro. ', $processo->NomeProcesso)]) ?>
                            </li>
                            <li class="divider"></li>
                            <li><?= $this->Html->link(__('View Fasi'),
                                    array('controller' => 'Fase', 'action' => 'index', '?'=> array('idprocesso'=>$processo->IdProcesso)));
                                ?></li>

                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>

                </td>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pager">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

