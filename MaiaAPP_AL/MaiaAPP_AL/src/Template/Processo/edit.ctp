<?php
/**
 * @var \App\View\AppView $this
 */
?>
</div>
<div class="col-lg-3">
       <ul class="well">
        <?= $this->Form->postLink(
                __('Cancella il processo'),
                ['action' => 'delete', $processo->IdProcesso],
                ['confirm' => __('Are you sure you want to delete # {0}?', $processo->IdProcesso)]
            )
            ?><br/>
        <?= $this->Html->link(__('Elenco processi'), ['action' => 'index']) ?><br/>
        <?= $this->Html->link(__('Elenco Fasi'), ['controller' => 'Fase', 'action' => 'index']) ?><br/>

    </ul>
</div>


<script>
    $(document).ready(function(){
        $('#idtipoprocesso').change(function(e){

            var textValue;
            textValue = $( "#idtipoprocesso option:selected" ).text();
            $('input[name=CheckIdTipoProcesso]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#idtipoprocesso').trigger('change');

        $('#idstruttura').change(function(e){
            var textValue;
            textValue = $( "#idstruttura option:selected" ).text();
            $('input[name=CheckIdStruttura]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#IdStruttura').trigger('change');


    });
</script>

<div class="col-lg-8">

    <p class="bs-component">

    <?= $this->Form->create($processo) ?>
    <fieldset>
        <h1>Modifica il processo</h1>
        <div class="well">
        <?php
        echo $this->Form->control('NomeProcesso', array( 'label'=>'Nome processo','class' =>'form-control'));
        echo $this->Form->control('DescrizioneProcesso', array('label'=>'Descrizione Processo','class' =>'form-control'));
        echo $this->Form->control('NomeFunzione', array( 'label'=>'Funzione','class' =>'form-control'));

       // echo $this->Form->control('TipoProcesso_Id',array('hidden'=>true, 'label'=>''));


        $IdTipoprocessotoSelect = $processo->TipoProcesso_Id;
        echo
        $this->Form->input('TipoProcesso_Id', array(
            'type'=>'select',
            'label'=>'Tipo Processo',
            'options'=>$CTipoProcesso,
            'value'=> $IdTipoprocessotoSelect,  // indica il valore da selezionare
            'class' =>'form-control'
        ));

        $IdStrutturaSelect = $processo->Struttura_Id;
        echo
            $this->Form->input('Struttura_Id', array(
                'type'=>'select',
                'label'=>'Nome Struttura',
                'options'=>$CStrutturaFiltered,
                'value'=> $IdStrutturaSelect,
                'class' =>'form-control'//$this->Form->control('IdStruttura')// 20 // indica il valore con cui è selezionata la drop
        ));
        $IdRilevamento = $this->request->session()->read('IdRilevamento');
            echo $this->Form->control('IdRilevamento', array('value'=>$IdRilevamento,'class' =>'form-control'));
        echo $this->Form->control('CheckIdStruttura',array('hidden'=>true, 'label'=>''));
        echo $this->Form->control('CheckIdTipoProcesso',array('hidden'=>true, 'label'=>''));

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary btn-lg"')) ?>
    <?= $this->Form->end() ?>

</div>
