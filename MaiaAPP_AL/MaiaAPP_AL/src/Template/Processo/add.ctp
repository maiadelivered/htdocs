<?php
/**
 * @var \App\View\AppView $this
 */
?>

</div>
<div class="col-lg-3">
    <p class="well">

        <?= $this->Html->link(__('Elenco Processi'), ['action' => 'index']) ?>
    </p>
</div>

<div>
    <script>
        $(document).ready(function(){

            $('#tipoprocesso-id').change(function(e){
                var textValue;
                textValue = $( "#tipoprocesso-id option:selected" ).text();
                $('input[name=CheckIdTipoProcesso]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#tipoprocesso-id').trigger('change');


            $('#struttura-id').change(function(e){

                var textValue;
                textValue = $( "#struttura-id option:selected" ).text();
                $('input[name=CheckIdStruttura]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#struttura-id').trigger('change');
        });
    </script>
</div>

    <div class="col-lg-8">

        <p class="bs-component">
    <fieldset>
        <h1> Inserisci un nuovo processo</h1>
        <div class="well">
            <?= $this->Form->create(($processo),array('class'=>'form-horizontal'));?>

                   <div>
                    <?php

                    echo
                    $this->Form->input('TipoProcesso_Id', array(
                        'type'=>'select',
                        'label'=>'Tipo Processo',
                        'options'=>$CTipoProcesso,
                         'class' =>'form-control'
                         // 'value'=> $IdTipoprocessotoSelect  // indica il valore da selezionare
                    ));?>
                    <br/>
                     <?php  echo $this->Form->control('Nome Processo',array('class'=>'form-control'));?>
                     <br/>
                     <?php  echo $this->Form->control('Descrizione Processo',array('class'=>'form-control'));?>
                     <br/>
                      <?php  echo  $this->Form->input('Struttura_Id', array(
                            'type'=>'select',
                            'label'=>'Nome Struttura',
                            'options'=>$CStrutturaFiltered,
                          'class' =>'form-control'
                            //   'value'=> $IdStrutturaSelect //$this->Form->control('IdStruttura')// 20 // indica il valore con cui è selezionata la drop
                        ));;?>
                      <br/>
                      <?php echo $this->Form->control('CheckIdStruttura',array('hidden'=>true, 'label'=>''));
                    echo $this->Form->control('CheckIdTipoProcesso',array('hidden'=>true, 'label'=>''));
                    echo $this->Form->control('CheckIdTipoProcesso',array('hidden'=>true, 'label'=>''));
                   $IdRilevamento = $this->request->session()->read('IdRilevamento');
                    echo $this->Form->control('IdRilevamento', array('value'=>$IdRilevamento,'hidden'=>true, 'label'=>''));?>
                   </div>
        </fieldset>
       <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary btn-lg"')); ?>

            <?= $this->Form->end() ?>

    </p>
    </div>



