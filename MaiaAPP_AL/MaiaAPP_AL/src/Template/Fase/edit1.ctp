<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase $fase
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $fase->IdFase],
                ['confirm' => __('Are you sure you want to delete # {0}?', $fase->IdFase)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Fase'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Struttura'), ['controller' => 'Struttura', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Struttura'), ['controller' => 'Struttura', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Processo'), ['controller' => 'Processo', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Processo'), ['controller' => 'Processo', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dipendente'), ['controller' => 'Dipendente', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dipendente'), ['controller' => 'Dipendente', 'action' => 'add']) ?></li>
    </ul>
</nav>
<script>
    $(document).ready(function(){


        // And now fire change event when the DOM is ready

        $('#struttura-id').change(function(e){

            var textValue;
            textValue = $( "#struttura-id option:selected" ).text();
            $('input[name=CheckIdStruttura]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#IdStruttura').trigger('change');

        $('#periodicitaselect').change(function(e){

            var textValue;
            textValue = $( "#periodicitaselect option:selected" ).text();
            $('input[name=Periodicita]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#periodicitaselect').trigger('change');

        $('#GennaioCB').change(function(e){

            var textValue;
           //  if ($( "#Gennaiocb option:checked")) {
            if (GennaioCB.checked==1) {

            $('input[name=Gennaio]').val('x');
            }
                else
            {
                $('input[name=Gennaio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#GennaioCB').trigger('change');

        $('#FebbraioCB').change(function(e){

            var textValue;
            //  if ($( "#Febbraiocb option:checked")) {
            if (FebbraioCB.checked==1) {

                $('input[name=Febbraio]').val('x');
            }
            else
            {
                $('input[name=Febbraio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#FebbraioCB').trigger('change');

        $('#MarzoCB').change(function(e){

            var textValue;
            //  if ($( "#Marzocb option:checked")) {
            if (MarzoCB.checked==1) {

                $('input[name=Marzo]').val('x');
            }
            else
            {
                $('input[name=Marzo]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#MarzoCB').trigger('change');

        $('#AprileCB').change(function(e){

            var textValue;
            //  if ($( "#Aprilecb option:checked")) {
            if (AprileCB.checked==1) {

                $('input[name=Aprile]').val('x');
            }
            else
            {
                $('input[name=Aprile]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#AprileCB').trigger('change');

        $('#MaggioCB').change(function(e){

            var textValue;
            //  if ($( "#Maggiocb option:checked")) {
            if (MaggioCB.checked==1) {

                $('input[name=Maggio]').val('x');
            }
            else
            {
                $('input[name=Maggio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#MaggioCB').trigger('change');



        $('#GiugnoCB').change(function(e){

            var textValue;
            //  if ($( "#Giugnocb option:checked")) {
            if (GiugnoCB.checked==1) {

                $('input[name=Giugno]').val('x');
            }
            else
            {
                $('input[name=Giugno]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#GiugnoCB').trigger('change');

        $('#LuglioCB').change(function(e){

            var textValue;
            //  if ($( "#Lugliocb option:checked")) {
            if (LuglioCB.checked==1) {

                $('input[name=Luglio]').val('x');
            }
            else
            {
                $('input[name=Luglio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#LuglioCB').trigger('change');

        $('#AgostoCB').change(function(e){

            var textValue;
            //  if ($( "#Agostocb option:checked")) {
            if (AgostoCB.checked==1) {

                $('input[name=Agosto]').val('x');
            }
            else
            {
                $('input[name=Agosto]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#AgostoCB').trigger('change');
        $('#SettembreCB').change(function(e){

            var textValue;
            //  if ($( "#Settembrecb option:checked")) {
            if (SettembreCB.checked==1) {

                $('input[name=Settembre]').val('x');
            }
            else
            {
                $('input[name=Settembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#SettembreCB').trigger('change');

        $('#OttobreCB').change(function(e){

            var textValue;
            //  if ($( "#Ottobrecb option:checked")) {
            if (OttobreCB.checked==1) {

                $('input[name=Ottobre]').val('x');
            }
            else
            {
                $('input[name=Ottobre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#OttobreCB').trigger('change');

        $('#NovembreCB').change(function(e){

            var textValue;
            //  if ($( "#Novembrecb option:checked")) {
            if (NovembreCB.checked==1) {

                $('input[name=Novembre]').val('x');
            }
            else
            {
                $('input[name=Novembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#NovembreCB').trigger('change');

        $('#DicembreCB').change(function(e){

            var textValue;
            //  if ($( "#Dicembrecb option:checked")) {
            if (DicembreCB.checked==1) {

                $('input[name=Dicembre]').val('x');
            }
            else
            {
                $('input[name=Dicembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#DicembreCB').trigger('change');



    });
</script>

<div class="fase form large-9 medium-8 columns content">
    <?= $this->Form->create($fase) ?>
    <fieldset>
        <legend><?= __('Edit Fase') ?></legend>
        <?php
            echo $this->Form->control('CheckIdFase',array('label'=>'','hidden'=>true));
        echo
        $this->Form->input('Struttura_id', array(
            'type'=>'select',
            'label'=>'Struttura',
            'options'=>$CStrutturaFiltered
            //'value'=> 2 // StrutturaFiltered  // indica il valore da selezionare
        ));


            echo $this->Form->control('CheckIdStruttura',array('label'=>'','hidden'=>true));
            echo $this->Form->control('NomeFase');
            echo $this->Form->control('RiferimentiNormativi');
            echo $this->Form->control('TipoEventoAvvio');
            echo $this->Form->control('TipoOutput');
            echo $this->Form->control('AltreUnitaOrganizzativeCoinvolte');
            echo $this->Form->control('Periodicita', array('label'=>'','hidden'=>true));
        echo  'Periodicità';
        echo $this->Form->select('periodicitaselect', [
            'SI' => 'SI',
            'NO' => 'NO',
            'ND' => 'NON DEFINIBILE'
        ], array('default' => $fase->Periodicita, 'id'=>"periodicitaselect"));


            echo $this->Form->control('Gennaio', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Febbraio', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Marzo', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Aprile', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Maggio', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Giugno', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Luglio', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Agosto', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Settembre', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Ottobre', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Novembre', array('label'=>'','hidden'=>true));
            echo $this->Form->control('Dicembre', array('label'=>'','hidden'=>true));
?>

        <input type="checkbox" name="GennaioCB" value='x' id="GennaioCB"
               <?php

               if ($fase->Gennaio == 'x') echo 'checked';
               ?>
        ><label for="gennaio"> gennaio</label>

        <input type="checkbox" name="FebbraioCB" value='x' id="FebbraioCB"
            <?php
            if($fase->Febbraio == 'x') echo 'checked';
            ?>
        ><label for="Febbraio"> Febbraio </label>

        <input type="checkbox" name="MarzoCB" value='x' id="MarzoCB"
            <?php
            if($fase->Marzo == 'x') echo 'checked';
            ?>
        ><label for="Marzo"> Marzo </label>

        <input type="checkbox" name="AprileCB" value='x' id="AprileCB"
            <?php
            if($fase->Aprile == 'x') echo 'checked';
            ?>
        ><label for="Aprile"> Aprile </label>
        <input type="checkbox" name="MaggioCB" value='x' id="MaggioCB"
            <?php
            if($fase->Maggio == 'x') echo 'checked';
            ?>
        ><label for="Maggio"> Maggio </label>
        <input type="checkbox" name="GiugnoCB" value='x' id="GiugnoCB"
            <?php
            if($fase->Giugno == 'x') echo 'checked';
            ?>
        ><label for="Giugno"> Giugno </label>

        <input type="checkbox" name="LuglioCB" value='x' id="LuglioCB"
            <?php
            if($fase->Luglio == 'x') echo 'checked';
            ?>
        ><label for="Luglio"> Luglio </label>
        <input type="checkbox" name="AgostoCB" value='x' id="AgostoCB"
            <?php
            if($fase->Agosto == 'x') echo 'checked';
            ?>
        ><label for="Agosto"> Agosto </label>
        <input type="checkbox" name="SettembreCB" value='x' id="SettembreCB"
            <?php
            if($fase->Settembre == 'x') echo 'checked';
            ?>
        ><label for="Settembre"> Settembre </label>
        <input type="checkbox" name="OttobreCB" value='x' id="OttobreCB"
            <?php
            if($fase->Ottobre == 'x') echo 'checked';
            ?>
        ><label for="Ottobre"> Ottobre </label>
        <input type="checkbox" name="NovembreCB" value='x' id="NovembreCB"
            <?php
            if($fase->Novembre == 'x') echo 'checked';
            ?>
        ><label for="Novembre"> Novembre </label>
        <input type="checkbox" name="DicembreCB" value='x' id="DicembreCB"
            <?php
            if($fase->Dicembre == 'x') echo 'checked';
            ?>
        ><label for="Dicembre"> Dicembre </label>


        <?php

            echo $this->Form->control('NumMedioGiorniComplet');
            echo $this->Form->control('NumIterazioniAnnue');
            echo $this->Form->control('TotaleGiornateLavoroAnnue');

            echo $this->Form->input('Processo_id',array('type'=>'text','value'=>$IDProcesso,'label'=>'','hidden'=>true));

            echo $this->Form->control('CheckIdProcesso',array('label'=>'','hidden'=>true));

            echo $this->Form->control('IdRilevamento',array('label'=>'','hidden'=>true));
         //   echo $this->Form->control('dipendente._ids', ['options' => $dipendente]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
