<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase $fase
 */
?>
</div>
<div class="col-lg-3">
    <p class="well">

        <?= $this->Html->link(__('Elenco Fasi'), ['action' => 'index']) ?>
        <br/>
        <?= $this->Html->link(__('Elenco processi'), ['controller' => 'Processo', 'action' => 'index']) ?></li>
    </p>
</div>

<script>
    $(document).ready(function(){


        // And now fire change event when the DOM is ready

        $('#struttura-id').change(function(e){

            var textValue;
            textValue = $( "#struttura-id option:selected" ).text();
            $('input[name=CheckIdStruttura]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#IdStruttura').trigger('change');

        $('#periodicitaselect').change(function(e){

            var textValue;
            textValue = $( "#periodicitaselect option:selected" ).text();
            $('input[name=Periodicita]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#periodicitaselect').trigger('change');

        $('#GennaioCB').change(function(e){

            var textValue;
            //  if ($( "#Gennaiocb option:checked")) {
            if (GennaioCB.checked==1) {

                $('input[name=Gennaio]').val('x');
            }
            else
            {
                $('input[name=Gennaio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#GennaioCB').trigger('change');

        $('#FebbraioCB').change(function(e){

            var textValue;
            //  if ($( "#Febbraiocb option:checked")) {
            if (FebbraioCB.checked==1) {

                $('input[name=Febbraio]').val('x');
            }
            else
            {
                $('input[name=Febbraio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#FebbraioCB').trigger('change');

        $('#MarzoCB').change(function(e){

            var textValue;
            //  if ($( "#Marzocb option:checked")) {
            if (MarzoCB.checked==1) {

                $('input[name=Marzo]').val('x');
            }
            else
            {
                $('input[name=Marzo]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#MarzoCB').trigger('change');

        $('#AprileCB').change(function(e){

            var textValue;
            //  if ($( "#Aprilecb option:checked")) {
            if (AprileCB.checked==1) {

                $('input[name=Aprile]').val('x');
            }
            else
            {
                $('input[name=Aprile]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#AprileCB').trigger('change');

        $('#MaggioCB').change(function(e){

            var textValue;
            //  if ($( "#Maggiocb option:checked")) {
            if (MaggioCB.checked==1) {

                $('input[name=Maggio]').val('x');
            }
            else
            {
                $('input[name=Maggio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#MaggioCB').trigger('change');



        $('#GiugnoCB').change(function(e){

            var textValue;
            //  if ($( "#Giugnocb option:checked")) {
            if (GiugnoCB.checked==1) {

                $('input[name=Giugno]').val('x');
            }
            else
            {
                $('input[name=Giugno]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#GiugnoCB').trigger('change');

        $('#LuglioCB').change(function(e){

            var textValue;
            //  if ($( "#Lugliocb option:checked")) {
            if (LuglioCB.checked==1) {

                $('input[name=Luglio]').val('x');
            }
            else
            {
                $('input[name=Luglio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#LuglioCB').trigger('change');

        $('#AgostoCB').change(function(e){

            var textValue;
            //  if ($( "#Agostocb option:checked")) {
            if (AgostoCB.checked==1) {

                $('input[name=Agosto]').val('x');
            }
            else
            {
                $('input[name=Agosto]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#AgostoCB').trigger('change');
        $('#SettembreCB').change(function(e){

            var textValue;
            //  if ($( "#Settembrecb option:checked")) {
            if (SettembreCB.checked==1) {

                $('input[name=Settembre]').val('x');
            }
            else
            {
                $('input[name=Settembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#SettembreCB').trigger('change');

        $('#OttobreCB').change(function(e){

            var textValue;
            //  if ($( "#Ottobrecb option:checked")) {
            if (OttobreCB.checked==1) {

                $('input[name=Ottobre]').val('x');
            }
            else
            {
                $('input[name=Ottobre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#OttobreCB').trigger('change');

        $('#NovembreCB').change(function(e){

            var textValue;
            //  if ($( "#Novembrecb option:checked")) {
            if (NovembreCB.checked==1) {

                $('input[name=Novembre]').val('x');
            }
            else
            {
                $('input[name=Novembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#NovembreCB').trigger('change');

        $('#DicembreCB').change(function(e){

            var textValue;
            //  if ($( "#Dicembrecb option:checked")) {
            if (DicembreCB.checked==1) {

                $('input[name=Dicembre]').val('x');
            }
            else
            {
                $('input[name=Dicembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#DicembreCB').trigger('change');



    });
</script>

<div class="col-lg-8">

    <p class="bs-component">
    <fieldset>
        <h1> Inserisci un una nuova fase al processo: <?php echo($CheckIdProcesso)?></h1>
        <div class="well">
    <?= $this->Form->create($fase) ?>

        <?php

        echo
        $this->Form->input('Struttura_id', array(
            'type'=>'select',
            'label'=>'Struttura',
            'options'=>$CStrutturaFiltered,
            'class' =>'form-control'
            //'value'=> 2 // StrutturaFiltered  // indica il valore da selezionare
        ));
            echo $this->Form->control('Nome Fase',array('class'=>'form-control'));
            echo $this->Form->control('Riferimenti Normativi',array('class'=>'form-control'));
            echo $this->Form->control('Evento di Avvio',array('class'=>'form-control'));
            echo $this->Form->control('Tipo di Output',array('class'=>'form-control'));
            echo $this->Form->control('Altre Unita Organizzative Coinvolte',array('class'=>'form-control'));
        echo $this->Form->control('Periodicita', array('label'=>'','hidden'=>true));?>
            <strong>Periodicità</strong>
        <?php echo $this->Form->select('periodicitaselect', [
            'SI' => 'SI',
            'NO' => 'NO',
            'ND' => 'NON DEFINIBILE'
        ], array('default' => $fase->Periodicita, 'id'=>"periodicitaselect",'class'=>'form-control'));



        ?>

        <input type="checkbox" name="GennaioCB" value='x' id="GennaioCB"><label for="ennaio">Gennaio</label>

        <input type="checkbox" name="FebbraioCB" value='x' id="FebbraioCB"><label for="Febbraio"> Febbraio </label>

        <input type="checkbox" name="MarzoCB" value='x' id="MarzoCB"><label for="Marzo"> Marzo </label>

        <input type="checkbox" name="AprileCB" value='x' id="AprileCB"><label for="Aprile"> Aprile </label>
        <input type="checkbox" name="MaggioCB" value='x' id="MaggioCB"><label for="Maggio"> Maggio </label>
        <input type="checkbox" name="GiugnoCB" value='x' id="GiugnoCB"><label for="Giugno"> Giugno </label>

        <input type="checkbox" name="LuglioCB" value='x' id="LuglioCB"><label for="Luglio"> Luglio </label>
        <input type="checkbox" name="AgostoCB" value='x' id="AgostoCB"><label for="Agosto"> Agosto </label>
        <input type="checkbox" name="SettembreCB" value='x' id="SettembreCB"><label for="Settembre"> Settembre </label>
        <input type="checkbox" name="OttobreCB" value='x' id="OttobreCB"><label for="Ottobre"> Ottobre </label>
        <input type="checkbox" name="NovembreCB" value='x' id="NovembreCB"><label for="Novembre"> Novembre </label>
        <input type="checkbox" name="DicembreCB" value='x' id="DicembreCB"><label for="Dicembre"> Dicembre </label>


        <?php
            echo $this->Form->control('Numero Medio di Giorni per il completamento',array('class'=>'form-control'));
            echo $this->Form->control('Numero di Iterazioni Annue',array('class'=>'form-control'));
            echo $this->Form->control('Totale Giornate Lavoro Annue',array('class'=>'form-control'));


        $IdRilevamento = $this->request->session()->read('IdRilevamento');
        echo $this->Form->input('IdRilevamento',array('type'=>'text','value'=>$IdRilevamento,'label'=>'','hidden'=>true));

      //   echo $this->Form->control('dipendente._ids', ['options' => $dipendente]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary btn-lg"'));
    //tutti i campi hidden
    echo $this->Form->input('Processo_id',array('type'=>'text','value'=>$IDprocesso,'label'=>'','hidden'=>'true'));
    echo $this->Form->input('CheckIdprocesso',array('type'=>'text','value'=>$CheckIdProcesso,'label'=>'','hidden'=>'true'));
    echo $this->Form->control('CheckIdFase',array('label'=>'','hidden'=>true));
    echo $this->Form->control('CheckIdStruttura',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Gennaio',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Febbraio',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Marzo',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Aprile',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Maggio',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Giugno',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Luglio',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Agosto',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Settembre',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Ottobre',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Novembre',array('label'=>'','hidden'=>true));
    echo $this->Form->control('Dicembre',array('label'=>'','hidden'=>true));
     $this->Form->end() ?>
</div>
