<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase $fase
 */
?>

</div>
<div class="col-lg-3">
    <ul class="well">
        <?= $this->Html->link(__('Elenco Fasi'), ['action' => 'index']) ?> <br/>
        <?= $this->Html->link(__('Elenco processi'), ['controller' => 'Processo', 'action' => 'index']) ?> </br>
        <?= $this->Html->link(__('Modifica Fase'), ['action' => 'edit', $fase->IdFase]) ?> <br/>
        <?= $this->Html->link(__('Nuova Fase'), ['action' => 'add']) ?> <br/>
        <?= $this->Form->postLink(__('Cancella Fase'), ['action' => 'delete', $fase->IdFase], ['confirm' => __('Are you sure you want to delete # {0}?', $fase->IdFase)]) ?> <br/>


    </ul>
</div>
<div class="col-lg-8">

    <p class="bs-component">
    <div class="fase view large-9 medium-8 columns content">
        <table class="vertical-table">
            <tr>
            <th scope="row">
            <td><h1> FASE:
                    <?= $this->Text->autoParagraph($fase->NomeFase) ?></h1> </td>
        </tr>
        </table>
        <div class="row">
            <h4><?= __('Tipo Evento Avvio') ?></h4>
            <?php if(((string)($fase->TipoEventoAvvio)) == ""): ?>

                <h5>Non è stato indicato l'evento di avvio </h5>

            <?php else:  ?>
                <?= $this->Text->autoParagraph($fase->TipoEventoAvvio) ?>
            <?php endif; ?>

        </div>
        <div class="row">
        <h4><?= __('TipoOutput') ?></h4>
            <?php if(((string)($fase->TipoOutput)) == ""): ?>

                <h5>Non è stato indicato il tipo di output </h5>

            <?php else:  ?>
                <?=$this->Text->autoParagraph($fase->TipoOutput) ?>
            <?php endif; ?>

        </div>
        <div class="row">
            <h4><?= __('Altre Unita Organizzative Coinvolte') ?></h4>
            <?php if(((string)($fase->AltreUnitaOrganizzativeCoinvolte)) == ""): ?>

                <h5>Non vi sono altre unità organizzative coinvolte </h5>

            <?php else:  ?>
                <?=$this->Text->autoParagraph($fase->AltreUnitaOrganizzativeCoinvolte) ?>
            <?php endif; ?>
                  </div>

        <div class="row">

            <h4><?= __('Periodicita') ?></h4>
            <?php if(((string)$fase->Periodicita) == ""): ?>

                <h5>Non è stato fornito il dato relativo alla periodicità </h5>

            <?php else:  ?>
                <?= $this->Text->autoParagraph($fase->Periodicita) ?>

            <?php endif; ?>

        </div>

        <div class="row">

            <h4><?= __('La fase è eseguita nei mesi di: ') ?></h4>

            <?= __('Gennaio') ?>
            <?= h($fase->Gennaio) ?>

        <tr>
            <th scope="row"><?= __('Febbraio') ?></th>
            <td><?= h($fase->Febbraio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Marzo') ?></th>
            <td><?= h($fase->Marzo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Aprile') ?></th>
            <td><?= h($fase->Aprile) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Maggio') ?></th>
            <td><?= h($fase->Maggio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Giugno') ?></th>
            <td><?= h($fase->Giugno) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Luglio') ?></th>
            <td><?= h($fase->Luglio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Agosto') ?></th>
            <td><?= h($fase->Agosto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Settembre') ?></th>
            <td><?= h($fase->Settembre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ottobre') ?></th>
            <td><?= h($fase->Ottobre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Novembre') ?></th>
            <td><?= h($fase->Novembre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dicembre') ?></th>
            <td><?= h($fase->Dicembre) ?></td>
        </tr>
            <div class="row">

                <h4><?= __('Numero Medio Giorni per il Completamento')?></h4>

                <td><?= $this->Text->autoParagraph($this->Number->format($fase->NumMedioGiorniComplet)) ?></td>

            </div>
            <div class="row">

                <h4><?= __('Numero Iterazioni Annue')?></h4>
                <td><?= $this->Text->autoParagraph($this->Number->format($fase->NumIterazioniAnnue)) ?></td>

            </div>

            <div class="row">

                <h4><?= __('TotaleGiornateLavoroAnnue')?></h4>
                <td><?= $this->Text->autoParagraph($this->Number->format($fase->TotaleGiornateLavoroAnnue)) ?></td>

            </div>
            <div class="row">

                <h4><?= __('Riferimenti Normativi')?></h4>
                <td><?=  $this->Text->autoParagraph(h($fase->RiferimentiNormativi)) ?></td>

            </div>


    <div class="related">
        <?php if (!empty($fase->dipendente)): ?>
        <nav class="navbar navbar-default" >
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Dipendenti che lavorano sulla fase</a>
                </div>
            </div>
        </nav>
        <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">


            <thead>
            <tr>
                <th scope="col"><?= __('IdDipendente') ?></th>
                <th scope="col"><?= __('Categoria Id') ?></th>
                <th scope="col"><?= __('Dipendente') ?></th>
                <th scope="col"><?= __('COGNOME') ?></th>
                <th scope="col"><?= __('NOME') ?></th>
                <th scope="col"><?= __('DESC QUALIFICA') ?></th>
                <th scope="col"><?= __('PO AP') ?></th>
                <th scope="col"><?= __('PO') ?></th>
                <th scope="col"><?= __('AP') ?></th>
                <th scope="col"><?= __('AltrePOAP') ?></th>
                <th scope="col"><?= __('DATA NAS') ?></th>
                <th scope="col"><?= __('DESC TITOLO STUDIO') ?></th>
                <th scope="col"><?= __('DIP') ?></th>
                <th scope="col"><?= __('SEZIONE') ?></th>
                <th scope="col"><?= __('SERVIZIO') ?></th>
                <th scope="col"><?= __('Dipartimento Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($fase->dipendente as $dipendente): ?>
            <tr>
                <td><?= h($dipendente->IdDipendente) ?></td>
                <td><?= h($dipendente->Categoria_id) ?></td>
                <td><?= h($dipendente->Dipendente) ?></td>
                <td><?= h($dipendente->COGNOME) ?></td>
                <td><?= h($dipendente->NOME) ?></td>
                <td><?= h($dipendente->DESC_QUALIFICA) ?></td>
                <td><?= h($dipendente->PO_AP) ?></td>
                <td><?= h($dipendente->PO) ?></td>
                <td><?= h($dipendente->AP) ?></td>
                <td><?= h($dipendente->AltrePOAP) ?></td>
                <td><?= h($dipendente->DATA_NAS) ?></td>
                <td><?= h($dipendente->DESC_TITOLO_STUDIO) ?></td>
                <td><?= h($dipendente->DIP) ?></td>
                <td><?= h($dipendente->SEZIONE) ?></td>
                <td><?= h($dipendente->SERVIZIO) ?></td>
                <td><?= h($dipendente->Dipartimento_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Dipendente', 'action' => 'view', $dipendente->IdDipendente]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Dipendente', 'action' => 'edit', $dipendente->IdDipendente]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dipendente', 'action' => 'delete', $dipendente->IdDipendente], ['confirm' => __('Are you sure you want to delete # {0}?', $dipendente->IdDipendente)]) ?>
                </td>
            </tr>
            </tbody>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

