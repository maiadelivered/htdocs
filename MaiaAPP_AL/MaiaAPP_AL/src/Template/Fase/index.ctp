<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase[]|\Cake\Collection\CollectionInterface $fase
 */
use Cake\Routing\Router;
?>
</div>

<script type="text/javascript">
    function AlertProcesso() {


        alert ('Per inserire una fase, è obbligatorio selezionare il processo su cui inserire la fase!!');
    }
</script>

<?php
//se processo filtered è > 0 allora si stanno visualizzando le fasi di uno specifico processo
// ed è inutile visualizzare il Divricerca
if (is_null($FasiSpecificoProcesso) == true ) {
?>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <?= $this->Form->create('Post') ?>
    <fieldset>
        <legend><?= __('Ricerca fase del Processo') ?></legend>
        <?php
        echo
        $this->Form->input('RicercaFasiProcesso', array(
            'type'=>'select',
            'label'=>'Nome Processo',
            'class'=>'form-control',
            'options'=>$processoFiltered,
            'value'=> $FasiSpecificoProcesso  // indica il valore da selezionare
        ));

        ?>

    </fieldset>
    <?= $this->Form->button(
        'Ricerca',
        array('class'=>'btn btn-primary btn-lg',
            'formaction' => Router::url(
                array('controller' => 'fase','action' => 'SearchFasiProcesso')
            )
        )
    );
    ?>

    <?= $this->Form->end() ?>

</div>
<?php
    }
?>

<nav class="navbar navbar-default" >
    <div class="container-fluid">
        <div class="navbar-header">
            <ul class="nav navbar-nav navbar-left">
                <li  class="nav navbar-nav">
            <?php
            // se processo filtered è > 0 allora si stanno visualizzando le fasi di uno specifico processo
            // ed è inutile visualizzare il Divricerca
            if (is_null($FasiSpecificoProcesso) == true ) {
                echo '<h3>Fasi di tutti processi </h3>';
            }else{
                echo '<h3>Fasi del processo:'  . ($fase->first()->CheckIdProcesso) .'</h3>';

            }
            ?>


        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav">    <?= $this->Html->link(__('Cambia processo '), array('controller' => 'Fase', 'action' => 'index'));?></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav"> <?= $this->Html->link(__('Elenco dei processi'), ['controller' => 'Processo', 'action' => 'index']) ?></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav">
                <?php
                // debug($FasiSpecificoProcesso);
                if (is_null($FasiSpecificoProcesso) == true ) {
                    echo   "<a href=\"javascript:AlertProcesso();\"> Nuova Fase </a>";
                }
                else {
                    echo    $this->Html->link(__('Nuova Fase'),
                        array('controller' => 'Fase', 'action' => 'Add',
                            '?' => array('IDprocesso' => $FasiSpecificoProcesso)));
                }?>
                </li>
            </ul>


            </div>
    </div>


</nav>
    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
        <thead>
        <tr>

                <th scope="col" class="info"><?= $this->Paginator->sort('IdFase') ?></th>

                <th scope="col" class="info"><?= $this->Paginator->sort('Struttura_id') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('CheckIdStruttura') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Nome Fase') ?></th>
                <th scope="col" class="info" ><?= $this->Paginator->sort('Tipo Evento Avvio') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Tipo Output') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Altre UO coinvolte') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Periodicita') ?></th>

                <th scope="col" class="info"><?= $this->Paginator->sort('GG Medi') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Iterazioni Annue') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Totale Giornate Lavoro Annue') ?></th>

                <th scope="col" class="info"><?= $this->Paginator->sort('CheckIdProcesso') ?></th>

                <th scope="col" class="info"><?= __('') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($fase as $fase): ?>
            <tr>
                <td><?= $this->Number->format($fase->IdFase) ?></td>

                <td><?= $fase->has('struttura') ? $this->Html->link($fase->struttura->IdStruttura, ['controller' => 'Struttura', 'action' => 'view', $fase->struttura->IdStruttura]) : '' ?></td>
                <td><?= h($fase->CheckIdStruttura) ?></td>
                <td><?= h($fase->NomeFase) ?></td>
                <td><?= h($fase->TipoEventoAvvio) ?></td>
                <td><?= h($fase->TipoOutput) ?></td>
                <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
                <td><?= h($fase->Periodicita) ?></td>
                <td><?= $this->Number->format($fase->NumMedioGiorniComplet) ?></td>
                <td><?= $this->Number->format($fase->NumIterazioniAnnue) ?></td>
                <td><?= $this->Number->format($fase->TotaleGiornateLavoroAnnue) ?></td>
                <td><?= h($fase->CheckIdProcesso) ?></td>

                <td class="actions">

                    <div class="btn-group">
                        <a href="#" class="btn btn-default">Azioni</a>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu">


                            <li><?= $this->Html->link(__('View'),
                                    array('controller' => 'Fase', 'action' => 'view',
                                        '?'=> array('idprocesso'=>$FasiSpecificoProcesso ,'Idfase'=>$fase->IdFase)));
                                ?></li>
                            <li>

                            <li><?= $this->Html->link(__('Edit'),
                                    array('controller' => 'Fase', 'action' => 'edit',
                                        '?'=> array('idprocesso'=>$FasiSpecificoProcesso ,'Idfase'=>$fase->IdFase)));
                                ?></li>
                            <li>

                            </li>
                            <li><?= $this->Form->postLink(__('Delete'),
                                    ['action' => 'delete', $fase->IdFase],
                                    ['confirm' => __('Si è sicuri di voler eliminare la fase {0}? 
 L\'eliminazione della fase comporterà  l\'eliminazione dei relativi  carichi di lavoro. ', $fase->NomeFase)]) ?> </li>

                        </ul>
                    </div>

                </td>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pager">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="btn-group btn-group-justified" ><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
