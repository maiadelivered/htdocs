<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Routing\Router;
use Cake\View\Helper;

$cakeDescription = 'CakePHP: the rapid development php framework';
?>

<!DOCTYPE html>

<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php // echo  $this->Html->css('base.css') ?>
    <?php // echo $this->Html->css('cake.css') ?>


    <?php echo  $this->Html->css('bootstrap.min') ?>
    <?php //   echo  $this->Html->css('bootstrap') ?>

    <?php  echo  $this->Html->script('jquery-3.1.0') ?>
    <?php echo  $this->Html->script('bootstrap.min') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body>

<nav class="navbar navbar-inverse" >
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div>
                <table style="text-align: left; width: 1131px; height: 224px;"
                       border="0" cellpadding="2" cellspacing="2">
                    <tbody>
                    <tr>
                    <tr><td style="vertical-align: top;">
                            <?= $this->Html->image('Regione.jpg',['height'=>'130', 'width'=>'150']) ?>
                        </td>
                        <td style="vertical-align: top;">
                            <div style="text-align: center;">  <h3>MAIA - Modello Ambidestro per l’innovazione della macchina Amministrativa regionale</h3>
                            </div>
                        </td>
                        <td style="vertical-align: top;">
                            <?= $this->Html->image('ipres.jpg',['height'=>'130', 'width'=>'150']) ?>
                        </td>
                    </tr>
                    </tr>
                    </tbody>
                </table>
            </div>

            </div>
            <ul class="nav navbar-nav">
                <li> <?php echo($this->Html->link('Home Page', ['controller' => 'Pages', 'action' => 'display']));?></li>
                <li >
                    <?php echo($this->Html->link(
                        'Gestione Processi',
                        ['controller' => 'Processo', 'action' => 'index', '_full' => true]));?>

                </li>

                <li><?php echo ($this->Html->link('Gestione Fasi', ['controller' => 'fase', 'action'=>'index']));?></li>

                <li> <?php echo ($this->Html->link('Cambia rilevamento corrente', ['controller' => 'rilevamento', 'action'=>'index']));?></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li> <?= $this->Html->image('button-160595_960_720.png', ['height'=>'30', 'width'=>'30','url' => ['controller' => 'Users','action'=>'logout']]) ?> </a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <!-- <a href="#"> User: SezXXX - Dipartimento: Sviluppo Economico - Sezione: Tutela del Lavoro - Rilevamento: SSSSS
                    </a> -->

                    <?php if($loggedIn): ?>

                    <h5>Sezione: id Sezione <?php echo($this->request->session()->read('User.idSezione')); echo(" :"); echo($this->request->session()->read('Sezione.nomeSezione')); ?>
                        Dipartimento: <?php  echo($this->request->session()->read('User.idDipartimento')); echo(" :"); echo($this->request->session()->read('Dipartimento.nomeDipartimento')) ?> <span class="sr-only">(current)</span></h5></li>


                <?php else : ?>
                        <h1></h1>

                    <?php endif; ?>



                    </li>

            </ul>


        </div>
    </div>


</nav>


<!--  <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
      <ul class="dropdown-menu" role="menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
          <li class="divider"></li>
          <li><a href="#">One more separated link</a></li>
      </ul>
  </li>

</ul>
<form class="navbar-form navbar-left" role="search">
  <div class="form-group">
      <input type="text" class="form-control" placeholder="Search">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>-->


        <!--
            <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <li><a target="_blank" href="https://book.cakephp.org/3.0/">Documentation</a></li>
                <li><a target="_blank" href="https://api.cakephp.org/3.0/">API</a></li>
            </ul>
        </div>
        -->


<div class="col-lg-8 col-md-7 col-sm-6">
    <div >

        <ul class="right">
            <?php if(!($loggedIn)): ?>

                <h1>Per iniziare effettuare il login</h1>

                       <?php endif; ?>
        </ul>
    </div>


</div>
<?= $this->Flash->render() ?>


<div class="container clearfix">

    <?= $this->fetch('content') ?>

</div>
    <footer>
    </footer>
</body>
</html>
