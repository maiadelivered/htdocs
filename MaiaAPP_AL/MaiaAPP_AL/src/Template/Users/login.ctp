<!-- File: src/Template/Users/login.ctp -->
<!DOCTYPE html>
<html>
<body>

<div>
   <fieldset>
       <h1> LOGIN</h1>
       <div class="form-group">
    <?= $this->Form->create() ?>
           <div class = "well">

               <?= $this->Form->input('username', array('class'=>'form-control')); ?>
               <?= $this->Form->input('password', array('type' => 'password','class'=>'form-control'));?>
               <?= $this->Form->submit('Login', array('class' => 'btn btn-primary btn-lg'));?>

           </div>
              <?= $this->Form->end() ?>
   </fieldset>
</div>
</div>
</body>
</html>
