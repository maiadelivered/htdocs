<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id_user],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id_user)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('address');
            echo $this->Form->control('idDipartimento');
            echo $this->Form->control('idSezione');
            echo $this->Form->control('idrilevamento');
            echo $this->Form->control('birthdate', ['empty' => true]);
            echo $this->Form->control('community');
            echo $this->Form->control('gender');
            echo $this->Form->control('language');
            echo $this->Form->control('location');
            echo $this->Form->control('short_bio');
            echo $this->Form->control('surname');
            echo $this->Form->control('username');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
