<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;

/**
 * Dipendente Controller
 *
 * @property \App\Model\Table\DipendenteTable $Dipendente
 *
 * @method \App\Model\Entity\Dipendente[] paginate($object = null, array $settings = [])
 */
class DipendenteController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $conditions = array('conditions' => array('dipartimento_id' => $idDipartimento, 'sezione_id' => $idSezione),
            'order'=>array('dipendente'=> 'asc') );

        $DipendenteFiltered =  $this->Dipendente->find('all', $conditions);
        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Dipendente' . " --- ".
            'Action = '. 'Index' . " --- ".
            'Query = ' . $DipendenteFiltered .  " --- ".
            'Parameter' . serialize($conditions));



        /*$this->paginate = array(
            'DipendenteFiltered' => array(
                'sort' => array(
                    'dipendente' =>  'DESC'
                )
            )
        );
        */
        //  $this->paginate = array('order' => array('dipendente' => 'asc'));

       $dipendente = $this->paginate($DipendenteFiltered);
       //  $dipendente= $DipendenteFiltered;
        $this->set(compact('dipendente'));
        $this->set('_serialize', ['dipendente']);
    }




    public function SearchDipendente()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

//        $this->loadModel('vlistprocesso');
        $passedArgs = $this->request->getData('DipendenteToSearch');

        $condition =  array(
            'Dipendente LIKE' => '%'.$passedArgs.'%',
            'iddipartimento' =>  $idDipartimento,
            'idsezione' => $idSezione
             // ,'IdRilevamento'=>$IdRilevamento
            );
        $DipendenteFiltered = $this->Dipendente->find('all',
            array('conditions' => array(
                'Dipendente LIKE' => ''.$passedArgs.'%',
                'dipartimento_id' =>  $idDipartimento,
                'sezione_id' => $idSezione
            //    ,'IdRilevamento'=>$IdRilevamento
            )));


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Dipendente' . " --- ".
            'Action = '. 'SearchDipendente' . " --- ".
            'Query = ' . $DipendenteFiltered .  " --- ".
            'Parameter' . serialize($condition));

        $dipendente = $this->paginate($DipendenteFiltered);
        $azionetodo = 'ricerca';
        $this->set(compact('dipendente','passedArgs', 'azionetodo'));
        $this->set('_serialize', ['dipendente']);

        $this->render('index');

    }
    /**
     * View method
     *
     * @param string|null $id Dipendente id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dipendente = $this->Dipendente->get($id, [
            'contain' => ['Categorias', 'Dipartimentos', 'Seziones', 'Fase', 'FaseBackupdati', 'Struttura']
        ]);

        $this->set('dipendente', $dipendente);
        $this->set('_serialize', ['dipendente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dipendente = $this->Dipendente->newEntity();
        if ($this->request->is('post')) {
            $dipendente = $this->Dipendente->patchEntity($dipendente, $this->request->getData());
            if ($this->Dipendente->save($dipendente)) {
                $this->Flash->success(__('The dipendente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dipendente could not be saved. Please, try again.'));
        }
        $categorias = $this->Dipendente->Categorias->find('list', ['limit' => 200]);
        $dipartimentos = $this->Dipendente->Dipartimentos->find('list', ['limit' => 200]);
        $seziones = $this->Dipendente->Seziones->find('list', ['limit' => 200]);
        $fase = $this->Dipendente->Fase->find('list', ['limit' => 200]);
        $faseBackupdati = $this->Dipendente->FaseBackupdati->find('list', ['limit' => 200]);
        $struttura = $this->Dipendente->Struttura->find('list', ['limit' => 200]);
        $this->set(compact('dipendente', 'categorias', 'dipartimentos', 'seziones', 'fase', 'faseBackupdati', 'struttura'));
        $this->set('_serialize', ['dipendente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dipendente id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dipendente = $this->Dipendente->get($id, [
            'contain' => ['Fase', 'FaseBackupdati', 'Struttura']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dipendente = $this->Dipendente->patchEntity($dipendente, $this->request->getData());
            if ($this->Dipendente->save($dipendente)) {
                $this->Flash->success(__('The dipendente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dipendente could not be saved. Please, try again.'));
        }
        $categorias = $this->Dipendente->Categorias->find('list', ['limit' => 200]);
        $dipartimentos = $this->Dipendente->Dipartimentos->find('list', ['limit' => 200]);
        $seziones = $this->Dipendente->Seziones->find('list', ['limit' => 200]);
        $fase = $this->Dipendente->Fase->find('list', ['limit' => 200]);
        $faseBackupdati = $this->Dipendente->FaseBackupdati->find('list', ['limit' => 200]);
        $struttura = $this->Dipendente->Struttura->find('list', ['limit' => 200]);
        $this->set(compact('dipendente', 'categorias', 'dipartimentos', 'seziones', 'fase', 'faseBackupdati', 'struttura'));
        $this->set('_serialize', ['dipendente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dipendente id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dipendente = $this->Dipendente->get($id);
        if ($this->Dipendente->delete($dipendente)) {
            $this->Flash->success(__('The dipendente has been deleted.'));
        } else {
            $this->Flash->error(__('The dipendente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
