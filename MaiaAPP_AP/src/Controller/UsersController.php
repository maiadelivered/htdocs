<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Session;
use Cake\ORM\Behavior;
use Cake\Collection\Collection;
use Cake\Auth;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;



/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $this->loadModel('vlistusers');

        $users = $this->paginate($this->vlistusers);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $this->loadModel('vlistusers');

         $Iduser=isset($this->request->query['Iduser']) ? $this->request->query['Iduser'] : null;


        $vlistuser = $this->vlistusers->find('all',
            array('conditions' => array(
                'id_user' => $Iduser)));


        $this->set('vlistuser', $vlistuser);
        $this->set('_serialize', ['vlistuser']);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
          //  $Cpassword = ($user->password);
           // $user->password = (new  DefaultPasswordHasher)->hash($Cpassword);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $users = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $users = $this->Users->patchEntity($users, $this->request->getData());
            // $Cpassword = ($user->password);
            // $user->password = (new  DefaultPasswordHasher)->hash($Cpassword);
            if ($this->Users->save($users)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function login()
    {
        if ($this->request->is('post')) {

            $user = $this->Auth->identify();


            if($user)
            {

                $this->Auth->setUser($user);
                //ACQUISISCO I PARAMETRI SEZIONE E DIPARTIMENTO DA UTENTE
                $idSezione = $this->Auth->user('idSezione');
                $idDipartimento = $this->Auth->user('idDipartimento');
                $idUtente = $this->Auth->user('id_user');
                $UserRole = $this->Auth->user('UserRole');
                // debug($UserRole);
                $this->loadModel('vlistusers');
                $query = $this->vlistusers->find('all',array('conditions' => array('idSezione' => $idSezione,
                    'idDipartimento' => $idDipartimento, 'id_user' => $idUtente), 'fields' => array('CheckIdSezione', 'CheckIdDipartimento')));

                $nomeSezione = (string)$query->first()->CheckIdSezione;
                $nomeDipartimento = (string)$query->first()->CheckIdDipartimento;
                //scrivo NELLA SESSIONE
                $this->request->session()->write('User.idSezione',$idSezione);
                $this->request->session()->write('User.idDipartimento',$idDipartimento);
                $this->request->session()->write('User.UserRole',$UserRole);

                $this->request->session()->write('Sezione.nomeSezione',$nomeSezione);
                $this->request->session()->write('Dipartimento.nomeDipartimento',$nomeDipartimento);

                $this->set(compact('user'));
                $this->set('_serialize', ['user']);

                //return $this->redirect(['controller'=>'processo']);
                return $this->redirect(['controller'=>'rilevamento']);
               // return $this->redirect('/');

            }
            else{


                //bad login

                $this->Flash->error('username o password non validi');
                $this->redirect(['controller' => 'Pages', 'action' => 'display']);
            }

        }
    }
    public function Changerole()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');
        $UserRole = $this->request->session()->read('User.UserRole');

        $this->loadModel('vlistdipasezione');


        if  ((strcasecmp($UserRole, 'DirettoreDipartimento') === 0)
            or (strcasecmp($UserRole, 'Analista') === 0))
        {
            $conditions= array('conditions' => array('iddipartimento' => $idDipartimento));
            $DipartimentoFiltered =  $this->vlistdipasezione->find('all', $conditions);
        };
        if  ((strcasecmp($UserRole, 'DirettoreGenerale') === 0) or
            (strcasecmp($UserRole, 'DirettoreGenerale') === 0) or
            (strcasecmp($UserRole, 'Analista') === 0) ){
            $conditions = "";
            $DipartimentoFiltered =  $this->vlistdipasezione->find('all');
        }





        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Users' . " --- ".
            'Action = '. 'ChangeRole-GetSezioni' . " --- ".
            'Query = ' . $DipartimentoFiltered .  " --- ".
            'Parameter-IdDipartimento' . serialize( $conditions));

        $DipartimentoList = $this->paginate($DipartimentoFiltered);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $idSezionetochange = $this->request->query['idSezionetochange'];
            $idDipartimentotochange = $this->request->query['idDipartimentotochange'];
            $nomeSezionetochange = $this->request->query['nomeSezionetochange'];
            $nomeDipartimentotochange = $this->request->query['nomeDipartimentotochange'];

            Log::write('warning',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                'controller = ' . 'Users' . " --- ".
                'Action = '. 'ChangeRole-ChangedUSer' . " --- ".
                'PreviousUserRole = ' . serialize( $this->request->session())  .  " --- ".
                'NewUserRole' . serialize( $this->request->getData()));

            $this->request->session()->write('User.idSezione',$idSezionetochange);
            $this->request->session()->write('User.idDipartimento',$idDipartimentotochange);

            $this->request->session()->write('Sezione.nomeSezione',$nomeSezionetochange);
            $this->request->session()->write('Dipartimento.nomeDipartimento',$nomeDipartimentotochange);
            $this->Flash->success('Hai cambiato Utente. Hai la piena operatività sui dati del '.
            'dipartimento: ' . $nomeDipartimentotochange .
            ' sezione: ' .   $nomeSezionetochange );

        }

        $this->set(compact('DipartimentoList'));
        $this->set('_serialize', ['DipartimentoList']);
    }
    public function logout() {
        $this->Flash->success("Arrivederci");
        $this->Auth->logout();
       $this->request->getSession()->destroy();
    // $this->redirect($this->Auth->logout());
        $this->redirect(['controller' => 'Pages', 'action' => 'display']);

    }
    public function register()
    {
        $user = $this->Users->newEntity();
        if($this->request->is('post')){
            $user=$this->Users->patchEntity($user, $this->request->getData());
            if($this->Users->save($user)) {
                $this->Flash->success('Sei registrato e puoi fare il login');
                return $this->redirect(['action'=>'login']);
            }else
            {
                $this->Flash->error("non sei registrato");
            }
        }
        $this->set(compact('uses'));
        $this->set('_serialize',['user']);

    }
    public function beforeFilter(Event $event)
    {
       $this->Auth->allow('login');
      //  $this->Auth->allow('users');


    }




}
