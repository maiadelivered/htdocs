<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Rilevamento;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use Cake\Network\Session;

/**
 * Processo Controller
 *
 * @property \App\Model\Table\ProcessoTable $Processo
 *
 * @method \App\Model\Entity\Processo[] paginate($object = null, array $settings = [])
 */
class ProcessoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {



        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');

        $IdRilevamento = $this->request->session()->read('IdRilevamento');
        $IdStato = $this->request->session()->read('IdStato');
        //capisco se c'è già stata la scelta del rilevamento
        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Processo' . " --- ".
            'Action = '. 'index' . " --- ".
            'Query = ' . 'NONE' );

        if($IdRilevamento == null)
        {
            return $this->redirect(['controller'=>'rilevamento']);
        }



        $this->loadModel('vlistprocesso');

        $conditions =  array('conditions'=>
            array('iddipartimento' =>  $idDipartimento,
                'idsezione' => $idSezione,
                'IdRilevamento'=>$IdRilevamento ));
        $processoFiltered =  $this->vlistprocesso->find('all', $conditions);

        $processo = $this->paginate($processoFiltered);
        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);


    }

    /**
     * View method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $getID=isset($this->request->query['id']) ? $this->request->query['id'] : null;

        $processo = $this->Processo->get($getID, [
            'contain' => []
        ]);

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Processo' . " --- ".
            'Action = '. 'view' . " --- ".
            'Query = ' . $processo .  " --- ".
            'Parameter = ' . $getID);

        $this->set('processo', $processo);
        $this->set('_serialize', ['processo']);

        $this->loadModel('Fase');
        $FaseFiltered= $this->Fase->find('all', array('conditions' => array('processo_id = ' => ($getID))));


        $this->set('fase', $FaseFiltered);
        $this->set('_serialize', ['fase']);

        $this->loadModel('Vdipeprorownp');
        $vdipeprorownpAllproFiltered= $this->Vdipeprorownp->find('all', array('conditions' => array('Idprocesso = ' => ($getID))));


        $this->set(compact('vdipeprorownpAllproFiltered'));
        $this->set('_serialize', ['vdipeprorownpAllproFiltered']);
    }

    public function SearchProcesso()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $this->loadModel('vlistprocesso');
        $passedArgs = $this->request->getData('descrizione');
        $condition =  array(
            'Nomeprocesso LIKE' => '%'.$passedArgs.'%',
            'iddipartimento' =>  $idDipartimento,
            'idsezione' => $idSezione,
            'IdRilevamento'=>$IdRilevamento );
        $processoFiltered = $this->vlistprocesso->find('all',
            array('conditions' => array(
                'Nomeprocesso LIKE' => '%'.$passedArgs.'%',
                'iddipartimento' =>  $idDipartimento,
                'idsezione' => $idSezione,
                'IdRilevamento'=>$IdRilevamento )));

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Processo' . " --- ".
            'Action = '. 'SearchProcesso' . " --- ".
            'Query = ' . $processoFiltered .  " --- ".
            'Parameter' . serialize($condition));

        $processo = $this->paginate($processoFiltered);
        $azionetodo = 'ricerca';
        $this->set(compact('processo', 'azionetodo'));
        $this->set('_serialize', ['processo']);

        $this->render('index');

    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Processo' . " --- ".
            'Action = '. 'add' . " --- ".
            'Query = ' . 'NONE');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
            //  return $this->redirect(['action' => 'index']);
        }

            $this->loadModel('struttura');

            $CStrutturaFiltered = $this->struttura->find('list', [
                    'keyField' => 'IdStruttura',
                    'valueField' => 'NomeStruttura']
            )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione]);

            $this->set(compact('CStrutturaFiltered'));

            // Tipo processo per la dropbox
            $this->loadModel('Tipoprocesso');
            $CTipoProcesso = $this->Tipoprocesso->find('list', [
                'keyField' => 'IdTipoProcesso',
                'valueField' => 'TipoProcesso'
            ]);
            $this->set(compact('CTipoProcesso'));


            $processo = $this->Processo->newEntity();
            if ($this->request->is('post')) {

                Log::write('warning',
                    'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                    'controller = ' . 'Processo' . " --- ".
                    'Action = '. 'add' . " --- ".
                   //  'Query = ' . $processoFiltered .  " --- ".
                    'Parameter ProcessToADD' . serialize( $this->request->getData()));

                // Verifica che il nome processo sia unico nel rilevamento, sul dipartimento e nella sezione
                $errore = false;
                $erroremsg = '';
                $NomeNuovoProcesso = $this->request->getData('NomeProcesso');
                $this->loadModel('vlistprocesso');
                $Checkprocesso = $this->vlistprocesso->find('all',
                    array('conditions' => array(
                        'Nomeprocesso like' => $NomeNuovoProcesso . '%',
                        'iddipartimento' => $idDipartimento,
                        'idsezione' => $idSezione,
                        'IdRilevamento' => $IdRilevamento)));
                if ($Checkprocesso->count() > 0) {
                    $errore = true;
                    $erroremsg = 'ERRORE: non è possibile inserire un processo con lo stesso nome di un processo esistente';
                }
                // fine verifica
                if ($errore) {
                    $this->Flash->error($erroremsg);
                } else {
                        $processo = $this->Processo->patchEntity($processo, $this->request->getData());
                        if ($this->Processo->save($processo)) {
                            $this->Flash->success(__('Il processo è stato inserito correttamente.'));

                            return $this->redirect(['action' => 'index']);
                        }
                        $this->Flash->error(__('Si è verificato un errore nell\'inserimento del processo. Contattare l\'amministratore del sistema'));
                }
            }
            $this->set(compact('processo'));
            $this->set('_serialize', ['processo']);

    }
    /**
     * Edit method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }

        $this->loadModel('struttura');
        $CStrutturaFiltered = $this->struttura->find('list',array('fields' =>array(
            'keyField' => 'IdStruttura',
            'valueField' => 'NomeStruttura'
        ), 'conditions'=> array('Dipartimento_Id' =>  $idDipartimento,'Sezione_Id' => $idSezione )));
        $CStrutturaFiltered = $this->struttura->find('list',[
                'keyField' => 'IdStruttura',
                'valueField' => 'NomeStruttura']
            )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione ]);

        $this->set(compact('CStrutturaFiltered'));


        // Recupero Tipo processo per la dropbox
        $this->loadModel('Tipoprocesso');
        $CTipoProcesso = $this->Tipoprocesso->find('list',[
            'keyField' => 'IdTipoProcesso',
            'valueField' => 'TipoProcesso'
        ]);
        $this->set(compact('CTipoProcesso'));

        $processo = $this->Processo->get($id, [
            'contain' => []
        ]);

        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Processo' . " --- ".
            'Action = '. 'Edit-ProcessoTOEdit' . " --- ".
            'Query = ' . $processo .  " --- ".
            'Parameter = ' . serialize($id));

        if ($this->request->is(['patch', 'post', 'put'])) {

            $errore = false;
            $erroremsg = '';
            // verifica campi inseriti
            $NomeNuovoProcesso = $this->request->getData('NomeProcesso');
            $IdProcessoNuovoProcesso = $id;
            $this->loadModel('vlistprocesso');
            $conditions =  array(
                'Nomeprocesso like' => $NomeNuovoProcesso ,
                'iddipartimento' => $idDipartimento,
                'idsezione' => $idSezione,
                'IdRilevamento' => $IdRilevamento,
                'idprocesso != '=>$IdProcessoNuovoProcesso);

            $Checkprocesso = $this->vlistprocesso->find('all',
                array('conditions' => array(
                    'Nomeprocesso like' => $NomeNuovoProcesso ,
                    'iddipartimento' => $idDipartimento,
                    'idsezione' => $idSezione,
                    'IdRilevamento' => $IdRilevamento,
                    'idprocesso != '=>$IdProcessoNuovoProcesso)));
            Log::write('info',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                'controller = ' . 'Processo' . " --- ".
                'Action = '. 'Edit-CheckNomeProcesso' . " --- ".
                'Query = ' . $Checkprocesso .  " --- ".
                'Parameter = ' . serialize($conditions));
            if ($Checkprocesso->count() > 0 ) {
                $errore = true;
                $erroremsg = 'ERRORE: non è possibile modificare il nome di un processo con lo stesso nome di un processo esistente.';
            }

            if ($errore) {
                $this->Flash->error($erroremsg);
            } else {

                Log::write('warning',
                    'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                    'controller = ' . 'Processo' . " --- ".
                    'Action = '. 'Edit' . " --- ".
                    //  'Query = ' . $processoFiltered .  " --- ".
                    'Parameter ProcessoModified = ' . serialize( $this->request->getData()));

                $processo = $this->Processo->patchEntity($processo, $this->request->getData());
                if ($this->Processo->save($processo)) {
                    $this->Flash->success(__('La modifica al processo è stata effettuata correttamente.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Si è verificato un errore nella modifica del processo. Contattare l\'amministratore del sistema'));

            }

        }


        $this->set(compact('processo'));
        $this->set('_serialize', ['processo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Processo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {


        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }

        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Processo' . " --- ".
            'Action = '. 'Delete' . " --- ".
         //   'Query = ' . $processoFiltered .  " --- ".
            'Parameter ProcessToDelete = ' . $id);

        $this->request->allowMethod(['post', 'delete']);
        $processo = $this->Processo->get($id);

        // NOTA ho dovuto impostare  'quoteIdentifiers' => true, in app.php in config
        $conn = ConnectionManager::get('default');
        $SqlStored =  "call DeleteProcesso (" . $id . ")";
        $results = $conn->execute($SqlStored);
        $this->Flash->success(__('Il processo è stato cancellato correttamente.'));
        /*if ($this->Processo->delete($processo))
        {
            $this->Flash->success(__('The processo has been deleted.'));
        } else {
            $this->Flash->error(__('The processo could not be deleted. Please, try again.'));
        }
*/
        return $this->redirect(['action' => 'index']);
    }
}
