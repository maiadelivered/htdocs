<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;

/**
 * DipendenteFase Controller
 *
 * @property \App\Model\Table\DipendenteFaseTable $DipendenteFase
 *
 * @method \App\Model\Entity\DipendenteFase[] paginate($object = null, array $settings = [])
 */
class DipendenteFaseController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');


        $getIDFase = isset($this->request->query['idfase']) ? $this->request->query['idfase'] : null;
        $getIDProcesso = isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        $idprocesso = $getIDProcesso;
        $idfase = $getIDFase;

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            //   return $this->redirect($this->referer());
        }

        $this->set(compact('idprocesso', 'idfase'));


        // cerco il nome del proceeo e della fase
        $this->loadModel('vlistfase');
        $conditions = array('conditions' => array('iddipartimento' => $idDipartimento,
            'idsezione' => $idSezione,
            'idrilevamento' => $IdRilevamento,
            'idfase' => $getIDFase,
            'processo_id' => $getIDProcesso
        ));
        $ProcessoTable = $this->vlistfase->find('all', $conditions);
        $ProcessoData = $ProcessoTable->toArray();
        $NomeProcesso = $ProcessoData[0]->CheckIdProcesso;
        $NomeFase = $ProcessoData[0]->NomeFase;
        $NomeSezione = $ProcessoData[0]->NomeSezione;
        $NomeDipartimento = $ProcessoData[0]->NomeDipartimento;
        $NomeStruttura = $ProcessoData[0]->CheckIdStruttura;
        $this->set(compact('NomeProcesso', 'NomeFase', 'NomeSezione',
            'NomeDipartimento', 'getIDFase', 'getIDProcesso', 'NomeStruttura'));

        $this->loadModel('vlistdipendentefase');
        $vlistdipendenteFaseFiltered = $this->vlistdipendentefase->find('all',
            array('conditions' => array('idprocesso = ' => ($idprocesso),
                'idfase = ' => ($idfase)
            )));

        $this->set(compact('vlistdipendenteFaseFiltered'));
        $this->set('_serialize', ['vlistdipendenteFaseFiltered']);

        $this->loadModel('dipendente');

        $CDipendenteFiltered = $this->dipendente->find('list', [
                'keyField' => 'IdDipendente',
                'valueField' => 'Dipendente']
        )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione])
            ->order(['Dipendente']);


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'Index-SearchDipendenteFase' . " --- " .
            'Query = ' . $CDipendenteFiltered . " --- " .
            'Parameter Dipartimento_Id -Sezione_Id ' . $idDipartimento . '-' . $idSezione);

        $this->set(compact('CDipendenteFiltered'));

        $azionetodo = isset($this->request->query['Azione']) ? $this->request->query['Azione'] : null;
        if ((isset($azionetodo)) and (strcasecmp($azionetodo, 'edit') == 0)) {

            $getID = isset($this->request->query['id']) ? $this->request->query['id'] : null;
            $this->set(compact('azionetodo'));

            $dipendenteFaseEdit = $this->DipendenteFase->get($getID, ['contain' => []]);


            $this->set(compact('dipendenteFaseEdit', 'NomeDipendente'));// , 'dipendente', 'fase' ));
            $this->set('_serialize', ['dipendenteFaseEdit']);

        } else {


            // INSERT CARICO
            // ComboBox Dipendente

            $dipendenteFase = $this->DipendenteFase->newEntity();
            $this->set(compact('dipendenteFase')); // , 'dipendente', 'fase'));
            $this->set('_serialize', ['dipendenteFase']);


        }

    }


    public function indexd()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.nomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.nomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');


        $getIDDipendente = isset($this->request->query['IdDipendente']) ? $this->request->query['IdDipendente'] : null;
        $NomeFase = isset($this->request->query['NomeFase']) ? $this->request->query['NomeFase'] : null;
        $NomeProcesso = isset($this->request->query['NomeProcesso']) ? $this->request->query['NomeProcesso'] : null;
        $NomeStruttura = isset($this->request->query['NomeStruttura']) ? $this->request->query['NomeStruttura'] : null;
        $IdDipendente = $getIDDipendente;

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            //   return $this->redirect($this->referer());
        }


        // mostra l'elenco dei processi e delle fasi su cui è allocato il dipendente
        $this->loadModel('vlistdipendentefase');
        $vlistdipendenteFaseFiltered = $this->vlistdipendentefase->find('all',
            array('conditions' => array('iddipendente' => $getIDDipendente, 'Idrilevamento' => $IdRilevamento)));


        $vlistdipendenteFaseFilteredData = $vlistdipendenteFaseFiltered->toArray();

        $this->loadModel('dipendente');
        $dipendenteFiltered = $this->dipendente->find('all',
            array('conditions' => array('iddipendente' => $getIDDipendente)));

        $dipendenteFiltered = $dipendenteFiltered->toArray();
        $Nomedipendente = $dipendenteFiltered[0]->Dipendente;


        $this->set(compact('NomeProcesso', 'NomeFase'
            , 'NomeStruttura', 'Nomedipendente', 'IdDipendente',
            'nomeSezione', 'nomeDipartimento'));

        $this->set(compact('vlistdipendenteFaseFiltered'));
        $this->set('_serialize', ['vlistdipendenteFaseFiltered']);


        // serve per visualizzare i dati nella combo di selezione carico
        // i parametri $idSezione , $IdDipartiemnto sono presi dalla session
        $this->loadModel('vlistfase');
        $CProcessiFaseFiltered = $this->vlistfase->find('list', [
                'keyField' => 'IdFase',
                'valueField' => 'ProcessoFase']
        )->where(['IdDipartimento' => $idDipartimento, 'IdSezione' => $idSezione, 'Idrilevamento' => $IdRilevamento])
            ->order(['ProcessoFase']);


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'Indexd' . " --- " .
            'Query = ' . $CProcessiFaseFiltered . " --- " .
            'Parameter Dipartimento_Id -Sezione_Id ' . $idDipartimento . '-' . $idSezione);

        $this->set(compact('CProcessiFaseFiltered'));

        $this->loadModel('DipendenteFase');
        $azionetodo = isset($this->request->query['Azione']) ? $this->request->query['Azione'] : null;
        $this->set(compact('azionetodo'));
        if ((isset($azionetodo)) and (strcasecmp($azionetodo, 'edit') == 0)) {

            $getID = isset($this->request->query['id']) ? $this->request->query['id'] : null;
            $IdDipendente = isset($this->request->query['IdDipendente']) ? $this->request->query['IdDipendente'] : null;

            $dipendenteFaseEdit = $this->DipendenteFase->get($getID, ['contain' => []]);


            $this->set(compact('azionetodo'));


            $this->set(compact('dipendenteFaseEdit', 'IdDipendente', 'NomeDipendente'));// , 'dipendente', 'fase' ));
            $this->set('_serialize', ['dipendenteFaseEdit']);

        } else {

            // INSERT CARICO
            // ComboBox Dipendente

            $dipendenteFase = $this->DipendenteFase->newEntity();
            $this->set(compact('dipendenteFase')); // , 'dipendente', 'fase'));
            $this->set('_serialize', ['dipendenteFase']);

        }

    }


    public function indexp()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $NomeFase = isset($this->request->query['NomeFase']) ? $this->request->query['NomeFase'] : null;
        $NomeProcesso = isset($this->request->query['NomeProcesso']) ? $this->request->query['NomeProcesso'] : null;
        $NomeStruttura = isset($this->request->query['NomeStruttura']) ? $this->request->query['NomeStruttura'] : null;


        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
         //   return $this->redirect($this->referer());
        }

/// visualizzazione riepilogo per processo
        $getIDProcesso = isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;


        // Elenco di tutti i dipendenti che lavorano sul processo
        $this->loadModel('vlistdipendentefase');

        $conditionsDipendente = array('order' => array('Dipendente'),
            'fields' => array('Dipendente',
                'IdDipendente'));

        $CDipendenteFiltered = $this->vlistdipendentefase->find('all', $conditionsDipendente)
            ->distinct('Dipendente')
            ->where(['idprocesso' => $getIDProcesso]);
        $CDipendenteFilteredData = $CDipendenteFiltered->toArray();


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'indexp - $CDipendenteFiltered' . " --- " .
            'Query = ' . $CDipendenteFiltered . " --- " .
            'Parameter  ' . serialize($conditionsDipendente). '-- idprocesso = ' .$getIDProcesso);

        //  $vlistdipendenteFaseFilteredData contiene l'elenco il carico dei dipendenti sulle fase


        $conditionsListCarico = array('order' => array('NomeProcesso', 'NomeFase', 'Dipendente'),
            'fields' => array('NomeProcesso',
                'NomeFase',
                'Dipendente',
                'PercImpegnoSuFase',
                'IdFase',
                'IdProcesso',
                'IdDipendente',
                'IdEsegue',
                'NomeStruttura'),

        );

        $vlistdipendenteFaseFiltered = $this->vlistdipendentefase->find('all', $conditionsListCarico)->
        where(['Sezione_id' => $idSezione, 'Idrilevamento' => $IdRilevamento, 'idprocesso' => $getIDProcesso]);
        $vlistdipendenteFaseFilteredData = $vlistdipendenteFaseFiltered->toArray();


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'indexp - $vlistdipendenteFaseFiltered' . " --- " .
            'Query = ' . $vlistdipendenteFaseFiltered . " --- " .
            'Parameter  ' . serialize($conditionsListCarico) .
                '--' . 'Sezione_id=' . $idSezione . '-- Idrilevamento= '. $IdRilevamento . '-- idprocesso=' . $getIDProcesso);

        //
        $this->loadModel('vlistfase');

        $sqlvlistfase = array('order' => array('CheckIdProcesso', 'NomeFase'),
            'fields' => array('CheckIdProcesso',
                'NomeFase',
                'Processo_id',
                'IdFase',
                'CheckIdStruttura'
            ),

        );
        $FaseFiltered = $this->vlistfase->find('all', $sqlvlistfase)->
        where(['IdSezione' => $idSezione, 'Idrilevamento' => $IdRilevamento, 'processo_id' => $getIDProcesso]);

        $FaseFilteredData = $FaseFiltered->toArray();

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'indexp - $FaseFiltered' . " --- " .
            'Query = ' . $FaseFiltered . " --- " .
            'Parameter  ' . serialize($sqlvlistfase) .
            'IdSezione= ' . $idSezione . '-- Idrilevamento=' . $IdRilevamento . '-- processo_id= ' . $getIDProcesso);

        $this->set(compact('vlistdipendenteFaseFilteredData', 'FaseFilteredData', 'CDipendenteFilteredData'));


/// Fine visualizzazione riepilogo per processo



        $idprocesso = $getIDProcesso;

        $this->set(compact('idprocesso', 'idfase', 'NomeProcesso'));


        // creo l'elenco delle fasi
        $this->loadModel('vlistfase');
        $conditions = array('conditions' => array('iddipartimento' => $idDipartimento,
            'idsezione' => $idSezione,
            'idrilevamento' => $IdRilevamento,
            'processo_id' => $getIDProcesso
        ));

        $CFaseFiltered = $this->vlistfase->find('list', [
                'keyField' => 'IdFase',
                'valueField' => 'NomeFase']
        )->where(['iddipartimento' => $idDipartimento,
            'idsezione' => $idSezione,
            'idrilevamento' => $IdRilevamento,
            'processo_id' => $getIDProcesso])
            ->order(['NomeFase']);

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'indexp - $CFaseFiltered' . " --- " .
            'Query = ' . $CFaseFiltered . " --- " .
            'Parameter  ' . serialize($conditionsListCarico));


        $this->set(compact('CFaseFiltered'));

        $this->set(compact('NomeProcesso', 'NomeFase', 'NomeSezione',
            'NomeDipartimento', 'getIDFase', 'getIDProcesso', 'NomeStruttura'));


        // devo mostrare tutti i dipendenti della sezione

        $this->loadModel('dipendente');

        $CDipendenteFiltered = $this->dipendente->find('list', [
                'keyField' => 'IdDipendente',
                'valueField' => 'Dipendente']
        )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione])
            ->order(['Dipendente']);


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'Indexp' . " --- " .
            'Query = ' . $CDipendenteFiltered . " --- " .
            'Parameter Dipartimento_Id -Sezione_Id ' . $idDipartimento . '-' . $idSezione);

        $this->set(compact('CDipendenteFiltered'));

        $azionetodo = isset($this->request->query['Azione']) ? $this->request->query['Azione'] : null;
        if ((isset($azionetodo)) and (strcasecmp($azionetodo, 'edit') == 0)) {

            $getID = isset($this->request->query['id']) ? $this->request->query['id'] : null;
//debug($getID);
            $this->set(compact('azionetodo'));

            $dipendenteFaseEdit = $this->DipendenteFase->get($getID, ['contain' => []]);

            //debug($idprocesso);
            $Dipendente = isset($this->request->query['Dipendente']) ? $this->request->query['Dipendente'] : null;

            $this->set(compact('dipendenteFaseEdit', 'Dipendente', 'NomeFase', 'idprocesso'));// , 'dipendente', 'fase' ));
            $this->set('_serialize', ['dipendenteFaseEdit']);

        } else {

            // INSERT CARICO
            // ComboBox Dipendente

            $dipendenteFase = $this->DipendenteFase->newEntity();
            $this->set(compact('dipendenteFase')); // , 'dipendente', 'fase'));
            $this->set('_serialize', ['dipendenteFase']);


        }

    }

    public function riepilogoimpegni()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.nomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.nomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');


        // Elenco di tutti i dipendenti
        $this->loadModel('dipendente');

        $conditionsDipendente = array('order' => array('Dipendente'),
            'fields' => array('Dipendente',
                'IdDipendente'),
        );

        $CDipendenteFiltered = $this->dipendente->find('all', $conditionsDipendente)
            ->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione]);;
        $CDipendenteFilteredData = $CDipendenteFiltered->toArray();


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'riepilogoimpegni' . " --- " .
            'Query = ' . $CDipendenteFiltered . " --- " .
            'Parameter  ' . serialize($conditionsDipendente));

        //  $vlistdipendenteFaseFilteredData contiene l'elenco il carico dei dipendenti  sulle fase

        $this->loadModel('vlistdipendentefase');


        //$conditionsListCarico =  array('conditions' => array('Sezione_id'=> $idSezione,  'Idrilevamento' => $IdRilevamento));
        $conditionsListCarico = array('order' => array('NomeProcesso', 'NomeFase', 'Dipendente'),
            'fields' => array('NomeProcesso',
                'NomeFase',
                'Dipendente',
                'PercImpegnoSuFase',
                'IdFase',
                'IdProcesso',
                'IdDipendente'),

        );

        $vlistdipendenteFaseFiltered = $this->vlistdipendentefase->find('all', $conditionsListCarico)->where(['Sezione_id' => $idSezione, 'Idrilevamento' => $IdRilevamento]);
        $vlistdipendenteFaseFilteredData = $vlistdipendenteFaseFiltered->toArray();


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'riepilogoimpegni' . " --- " .
            'Query = ' . $vlistdipendenteFaseFiltered . " --- " .
            'Parameter  ' . serialize($conditionsListCarico));

        //
        $this->loadModel('vlistfase');

        $sqlvlistfase = array('order' => array('CheckIdProcesso', 'NomeFase'),
            'fields' => array('CheckIdProcesso',
                'NomeFase',
                'Processo_id',
                'IdFase',
            ),

        );
        //  $conditionsListFase = array('conditions' => array('iddipartimento' => $idDipartimento, 'idsezione' => $idSezione, 'idrilevamento'=> $IdRilevamento));
        // $FaseFiltered =  $this->vlistfase->query($sqlvlistfase);
        $FaseFiltered = $this->vlistfase->find('all', $sqlvlistfase)->where(['IdSezione' => $idSezione, 'Idrilevamento' => $IdRilevamento]);

        $FaseFilteredData = $FaseFiltered->toArray();

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'riepilogoimpegni' . " --- " .
            'Query = ' . $FaseFiltered . " --- " .
            'Parameter  ' . serialize($sqlvlistfase));

        $this->set(compact('$nomeSezione', '$nomeSezione',
            'vlistdipendenteFaseFilteredData', 'FaseFilteredData', 'CDipendenteFilteredData'));

    }


    /**
     * View method
     *
     * @param string|null $id Dipendente Fase id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dipendenteFase = $this->DipendenteFase->get($id, [
            'contain' => ['Dipendente', 'Fase']
        ]);

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'View' . " --- " .
            'Query = ' . $dipendenteFase . " --- " .
            'Parameter-Idesegue' . $id);

        $this->set('dipendenteFase', $dipendenteFase);
        $this->set('_serialize', ['dipendenteFase']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendneteFase' . " --- " .
            'Action = ' . 'add' . " --- " .
            'Query = ' . 'NONE');

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }


        if ($this->request->is('post')) {
            $getIDFase = $this->request->getData('idfase');
            $getIDProcesso = $this->request->getData('idprocesso');


            Log::write('warning',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
                'controller = ' . 'DipendenteFase' . " --- " .
                'Action = ' . 'add' . " --- " .
                //  'Query = ' . $processoFiltered .  " --- ".
                'Parameter DipendenteFaseToADD' . serialize($this->request->getData()));

            // Verifica che il carico di lavoro sia unico nel rilevamento, sul dipartimento e nella sezione
            $errore = false;
            $erroremsg = '';
            $NuovoDipendente_id = trim($this->request->getData('Dipendente_id'));
            $fase_id = $this->request->getData('Fase_id');
            $this->loadModel('vlistfase');
            $Checkdipendente = $this->DipendenteFase->find('all',
                array('conditions' => array(
                    'Dipendente_id ' => $NuovoDipendente_id,
                    'fase_id' => $fase_id,
                    'IdRilevamento' => $IdRilevamento,
                    'percImpegnoSuFase >' => 0)));
            if ($Checkdipendente->count() > 0) {
                $errore = true;
                $erroremsg = 'ERRORE: non è possibile inserire un ulteriore carico di lavoro su una risorsa 
                     già allocata sul processo. Utilizzare il pulsante modifica per aggiornare il carico del dipendente.';
            }
            // fine verifica
            if ($errore) {
                $this->Flash->error($erroremsg);
                return $this->redirect($this->referer());
            } else {

                $dipendenteFase = $this->DipendenteFase->newEntity();
                $dipendenteFase = $this->DipendenteFase->patchEntity($dipendenteFase, $this->request->getData());
                if ($this->DipendenteFase->save($dipendenteFase)) {
                    $this->Flash->success(__('Il carico di lavoro è stato inserito correttamente.'));

                    $returnpage = isset($this->request->query['returnpage']) ? $this->request->query['returnpage'] : null;

                    if ((isset($returnpage)) and (strcasecmp($returnpage, 'indexd') == 0)) {
                        return $this->redirect(
                            array(
                                "controller" => "DipendenteFase",
                                "action" => "indexd",
                                "?" => array(
                                    "IdDipendente" => $NuovoDipendente_id
                                )
                            ));

                    }
                    if ((isset($returnpage)) and (strcasecmp($returnpage, 'index') == 0)) {

                        return $this->redirect(
                            array(
                                "controller" => "DipendenteFase",
                                "action" => "index",
                                "?" => array(
                                    "idprocesso" => $getIDProcesso,
                                    "idfase" => $getIDFase
                                )
                            ));
                            $this->render('index');
                    }
                    if ((isset($returnpage)) and (strcasecmp($returnpage, 'indexp') == 0)) {
                        return $this->redirect(
                            array(
                                "controller" => "DipendenteFase",
                                "action" => "indexp",
                                "?" => array(
                                    "idprocesso" => $getIDProcesso,
                                    "idfase" => $getIDFase
                                )
                            ));
                        $this->render('indexp');
                    }
                }
                $this->Flash->error(__('Si è verificato un errore nell\'inserimento del carico di lavoro.'));
            }
        }
        $this->set(compact('dipendenteFase')); // , 'dipendente', 'fase'));
        $this->set('_serialize', ['dipendenteFase']);

    }


    /**
     * Edit method
     *
     * @param string|null $id Dipendente Fase id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }

        $getIDEsegue = $this->request->getData('IdEsegue');

        $getIDFase = $this->request->getData('idfase');
        $getIDProcesso = $this->request->getData('idprocesso');

        $dipendenteFase = $this->DipendenteFase->get($getIDEsegue, [
            'contain' => []
        ]);
        $this->set(compact('dipendenteFase'));// , 'dipendente', 'fase' ));
        $this->set('_serialize', ['dipendenteFase']);

        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'Edit-DipendenteFaseTOEdit' . " --- " .
            'Query = ' . $dipendenteFase . " --- " .
            'Parameter $getIDEsegueFase = ' . $getIDEsegue);


        if ($this->request->is(['patch', 'post', 'put'])) {
            // Verifica che il carico di lavoro sia unico nel rilevamento, sul dipartimento e nella sezione
            $errore = false;
            $erroremsg = '';
            $NuovoDipendente_id = trim($this->request->getData('Dipendente_id'));
            $fase_id = $this->request->getData('Fase_id');
            $this->loadModel('vlistfase');
            $Checkdipendente = $this->DipendenteFase->find('all',
                array('conditions' => array(
                    'Dipendente_id ' => $NuovoDipendente_id,
                    'fase_id' => $fase_id,
                    'IdRilevamento' => $IdRilevamento,
                    'percImpegnoSuFase >' => 0,
                    'idesegue !=' => $getIDEsegue)
                ));
            $conditions2 = array(
                'Dipendente_id ' => $NuovoDipendente_id,
                'fase_id' => $fase_id,
                'IdRilevamento' => $IdRilevamento,
                'percImpegnoSuFase >' => 0,
                'idesegue !=' => $getIDEsegue);

            Log::write('info',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
                'controller = ' . 'Fase' . " --- " .
                'Action = ' . 'Edit-CheckCaricoEsistente' . " --- " .
                'Query = ' . $Checkdipendente . " --- " .
                'Parameter = ' . serialize($conditions2));

            if ($Checkdipendente->count() > 0) {

                $errore = true;
                $erroremsg = 'ERRORE: non è possibile inserire un ulteriore carico di lavoro su una risorsa 
                     già allocata sul processo. Utilizzare il pulsante modifica per aggiornare il carico.';
            }
            // fine verifica
            if ($errore) {
                $this->Flash->error($erroremsg);
                return $this->redirect($this->referer());
            } else {
                Log::write('warning',
                    'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
                    'controller = ' . 'DipendenteFase' . " --- " .
                    'Action = ' . 'Edit' . " --- " .
                    //  'Query = ' . $processoFiltered .  " --- ".
                    'Parameter DipendenteFaseModified = ' . serialize($this->request->getData()));
                $dipendenteFase = $this->DipendenteFase->patchEntity($dipendenteFase, $this->request->getData());

                if ($this->DipendenteFase->save($dipendenteFase)) {
                    $this->Flash->success(__(' La modifica sul carico di lavoro è stata salvata correttamente '));
                    $returnpage = isset($this->request->query['returnpage']) ? $this->request->query['returnpage'] : null;
                    $Dipendente_id = isset($this->request->query['IdDipendente']) ? $this->request->query['IdDipendente'] : null;
                    $NomeProcesso = isset($this->request->query['NomeProcesso']) ? $this->request->query['NomeProcesso'] : null;

                    if ((isset($returnpage)) and (strcasecmp($returnpage, 'indexd') == 0)) {

                        return $this->redirect(
                            array(
                                "controller" => "DipendenteFase",
                                "action" => "indexd",
                                "?" => array(
                                    "IdDipendente" => $Dipendente_id
                                )
                            ));

                    };
                    if ((isset($returnpage)) and (strcasecmp($returnpage, 'index') == 0)) {
                        return $this->redirect(
                            array(
                                "controller" => "DipendenteFase",
                                "action" => "index",
                                "?" => array(
                                    "idprocesso" => $getIDProcesso,
                                    "idfase" => $getIDFase
                                )
                            ));
                    }
                    if ((isset($returnpage)) and (strcasecmp($returnpage, 'indexp') == 0)) {
                        return $this->redirect(
                            array(
                                "controller" => "DipendenteFase",
                                "action" => "indexp",
                                "?" => array(
                                    "idprocesso" => $getIDProcesso,
                                    "idfase" => $getIDFase,
                                    "NomeProcesso"=> $NomeProcesso
                                )
                            ));
                    }
                }
                $this->Flash->error(__('ERRORE: La modifica sul carico di lavoro NON è stata salvata correttamente.'));
            }

        }

    }


    /**
     * Delete method
     *
     * @param string|null $id Dipendente Fase id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }

        $getIDFase = isset($this->request->query['idfase']) ? $this->request->query['idfase'] : null;
        $getIDProcesso = isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        $getID = isset($this->request->query['id']) ? $this->request->query['id'] : null;

        $this->loadModel('DipendenteFase');
        $dipendenteFase = $this->DipendenteFase->get($getID);
        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- " .
            'controller = ' . 'DipendenteFase' . " --- " .
            'Action = ' . 'Delete' . " --- " .
            //   'Query = ' . $processoFiltered .  " --- ".
            'Parameter CaricoToDelete = ' . $getID);

        if ($this->DipendenteFase->delete($dipendenteFase)) {
            $this->Flash->success(__('Il carico di lavoro è stato eliminato con successo.'));
        } else {
            $this->Flash->error(__('Errore: Il carico di lavoro NON è stato eliminato. Contattare l\'amministratore del sistema'));
        }
        $returnpage = isset($this->request->query['returnpage']) ? $this->request->query['returnpage'] : null;
        $Dipendente_id = isset($this->request->query['IdDipendente']) ? $this->request->query['IdDipendente'] : null;
        $NomeProcesso = isset($this->request->query['NomeProcesso']) ? $this->request->query['NomeProcesso'] : null;

        //debug($Dipendente_id);
        //debug($returnpage);
        // $Dipendente_id = trim($this->request->getData('Dipendente_id'));

        if ((isset($returnpage)) and (strcasecmp($returnpage, 'indexd') == 0)) {
            return $this->redirect(
                array(
                    "controller" => "DipendenteFase",
                    "action" => "indexd",
                    "?" => array(
                        "IdDipendente" => $Dipendente_id
                    )
                ));

        };
        if ((isset($returnpage)) and (strcasecmp($returnpage, 'index') == 0)) {
            return $this->redirect(
                array(
                    "controller" => "DipendenteFase",
                    "action" => "index",
                    "?" => array(
                        "idprocesso" => $getIDProcesso,
                        "idfase" => $getIDFase
                    )
                ));
        }
        if ((isset($returnpage)) and (strcasecmp($returnpage, 'indexp') == 0)) {
            return $this->redirect(
                array(
                    "controller" => "DipendenteFase",
                    "action" => "indexp",
                    "?" => array(
                        "idprocesso" => $getIDProcesso,
                        "idfase" => $getIDFase,
                        'NomeProcesso'=>$NomeProcesso
                    )
                ));;
        };
    }
}
