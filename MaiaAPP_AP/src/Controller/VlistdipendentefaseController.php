<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Vlistdipendentefase Controller
 *
 * @property \App\Model\Table\VlistdipendentefaseTable $Vlistdipendentefase
 *
 * @method \App\Model\Entity\Vlistdipendentefase[] paginate($object = null, array $settings = [])
 */
class VlistdipendentefaseController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sezione']
        ];
        $vlistdipendentefase = $this->paginate($this->Vlistdipendentefase);

        $this->set(compact('vlistdipendentefase'));
        $this->set('_serialize', ['vlistdipendentefase']);
    }

    /**
     * View method
     *
     * @param string|null $id Vlistdipendentefase id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vlistdipendentefase = $this->Vlistdipendentefase->get($id, [
            'contain' => ['Sezione']
        ]);

        $this->set('vlistdipendentefase', $vlistdipendentefase);
        $this->set('_serialize', ['vlistdipendentefase']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vlistdipendentefase = $this->Vlistdipendentefase->newEntity();
        if ($this->request->is('post')) {
            $vlistdipendentefase = $this->Vlistdipendentefase->patchEntity($vlistdipendentefase, $this->request->getData());
            if ($this->Vlistdipendentefase->save($vlistdipendentefase)) {
                $this->Flash->success(__('The vlistdipendentefase has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vlistdipendentefase could not be saved. Please, try again.'));
        }
        $sezione = $this->Vlistdipendentefase->Sezione->find('list', ['limit' => 200]);
        $this->set(compact('vlistdipendentefase', 'sezione'));
        $this->set('_serialize', ['vlistdipendentefase']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vlistdipendentefase id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vlistdipendentefase = $this->Vlistdipendentefase->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vlistdipendentefase = $this->Vlistdipendentefase->patchEntity($vlistdipendentefase, $this->request->getData());
            if ($this->Vlistdipendentefase->save($vlistdipendentefase)) {
                $this->Flash->success(__('The vlistdipendentefase has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vlistdipendentefase could not be saved. Please, try again.'));
        }
        $sezione = $this->Vlistdipendentefase->Sezione->find('list', ['limit' => 200]);
        $this->set(compact('vlistdipendentefase', 'sezione'));
        $this->set('_serialize', ['vlistdipendentefase']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vlistdipendentefase id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vlistdipendentefase = $this->Vlistdipendentefase->get($id);
        if ($this->Vlistdipendentefase->delete($vlistdipendentefase)) {
            $this->Flash->success(__('The vlistdipendentefase has been deleted.'));
        } else {
            $this->Flash->error(__('The vlistdipendentefase could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
