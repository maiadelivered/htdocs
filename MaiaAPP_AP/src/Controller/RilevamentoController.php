<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;
/**
 * Rilevamento Controller
 *
 * @property \App\Model\Table\RilevamentoTable $Rilevamento
 *
 * @method \App\Model\Entity\Rilevamento[] paginate($object = null, array $settings = [])
 */
class RilevamentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $rilevamento = $this->paginate($this->Rilevamento);

        $this->set(compact('rilevamento'));
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * View method
     *
     * @param string|null $id Rilevamento id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function SetRilevamento($id = null)
    {

        $this->loadModel('rilevamento');

        $passedArgs=isset($this->request->query['id']) ? $this->request->query['id'] : null;

        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Rilevamento' . " --- ".
            'Action = '. 'SetRilevamento' . " --- ".
            //   'Query = ' . $processoFiltered .  " --- ".
            'Parameter RilevamentoScelto = ' . $passedArgs);


        $rilevamentoFiltered = $this->rilevamento->find('all', array('conditions' => array('IdRilevamento  =' => $passedArgs)));

        $this->request->session()->write('IdRilevamento', $rilevamentoFiltered->first()->IdRilevamento);
        $this->request->session()->write('StatoRilevamento', $rilevamentoFiltered->first()->IdStato);
        $this->request->session()->write('Rilevamento', $rilevamentoFiltered->first()->Rilevamento);
        $this->redirect('/processo');

    }

    public function view($id = null)
    {
        $rilevamento = $this->Rilevamento->get($id, [
            'contain' => []
        ]);

        $this->set('rilevamento', $rilevamento);
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $rilevamento = $this->Rilevamento->newEntity();
        if ($this->request->is('post')) {
         //   debug($this->request->getData());

            $conn = ConnectionManager::get('default');
            $SqlStored =  "";

            $RilevamentoName= $this->request->getData('Rilevamento');
            $Descrizione= $this->request->getData('Descrizione');
            $Note= $this->request->getData('Note');
            $Creatore= $this->request->getData('Creatore');
            $StartDate= $this->request->getData('StartDate');
            $EndDate= $this->request->getData('EndDate');
            $IdStato= $this->request->getData('IdStato');
            $CheckIdStato= $this->request->getData('CheckIdStato');

            $SqlStored =  "insert into rilevamento(Rilevamento,StartDate, EndDate, Descrizione,Note,IdStato, CheckIdStato,
Creatore) values (" .
                "'" . str_replace("'","''",$RilevamentoName ) . "'," .
                "'" . $StartDate . "'," .
                "'" . $EndDate . "'," .
                "'" . str_replace("'","''",$Descrizione) . "'," .
                "'" . str_replace("'","''",$Note) . "'," .
                "'" . $IdStato . "'," .
                "'" . $CheckIdStato . "'," .
                "'" . str_replace("'","''",$Creatore) . "')";

       //     debug($SqlStored);
            Log::write('warning',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                'controller = ' . 'Rilevamento' . " --- ".
                'Action = '. 'ADD' . " --- ".
                //   'Query = ' . $processoFiltered .  " --- ".
                'Parameter $SqlStored = ' . $SqlStored);

            $results = $conn->execute($SqlStored);
            $this->Flash->success(__('Il rilevamento è stato Inserito correttamente.'));
            return $this->redirect(['action' => 'index']);

           // $rilevamento = $this->Rilevamento->patchEntity($rilevamento, $this->request->getData());
           /* if ($this->Rilevamento->save($rilevamento)) {
                $this->Flash->success(__('The rilevamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rilevamento could not be saved. Please, try again.'));
           */
        }
        $this->set(compact('rilevamento'));
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rilevamento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $idtoedit=isset($this->request->query['idtoedit']) ? $this->request->query['idtoedit'] : null;
        $rilevamento = $this->Rilevamento->get($idtoedit, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $conn = ConnectionManager::get('default');
            $SqlStored =  "";

            $IdRilevamentoTOChange= $this->request->getData('IdRilevamentoTOChange');
            $RilevamentoName= $this->request->getData('Rilevamento');
            $Descrizione= $this->request->getData('Descrizione');
            $Note= $this->request->getData('Note');
            $Creatore= $this->request->getData('Creatore');
            $StartDate= $this->request->getData('StartDate');
            $EndDate= $this->request->getData('EndDate');
            $IdStato= $this->request->getData('IdStato');
            $CheckIdStato= $this->request->getData('CheckIdStato');

            $SqlStored =  "Update rilevamento set " .
            " Rilevamento = '" . str_replace("'","''",$RilevamentoName )  . "'," .
            "StartDate= '" .  $StartDate . "'," .
             "EndDate = '" .  $EndDate . "'," .
             "Descrizione= '" . str_replace("'","''",$Descrizione) . "'," .
             "Note = '" . str_replace("'","''",$Note ). "'," .
             "IdStato = '" . $IdStato . "'," .
             "CheckIdStato = '" . $CheckIdStato . "'," .
             "Creatore= '" . str_replace("'","''", $Creatore) . "'" .
            " where idrilevamento = ". $IdRilevamentoTOChange;
           // debug ($SqlStored);


            Log::write('warning',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                'controller = ' . 'Rilevamento' . " --- ".
                'Action = '. 'Edit' . " --- ".
                //   'Query = ' . $processoFiltered .  " --- ".
                'Parameter $SqlStored = ' . $SqlStored);

            $results = $conn->execute($SqlStored);
            $this->Flash->success(__('Il rilevamento è stato Modificato correttamente.'));
            return $this->redirect(['action' => 'index']);

            /*
            $rilevamento = $this->Rilevamento->patchEntity($rilevamento, $this->request->getData());
            if ($this->Rilevamento->save($rilevamento)) {
                $this->Flash->success(__('The rilevamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rilevamento could not be saved. Please, try again.'));
            */
        }
        $this->set(compact('rilevamento'));
        $this->set('_serialize', ['rilevamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rilevamento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rilevamento = $this->Rilevamento->get($id);
        if ($this->Rilevamento->delete($rilevamento)) {
            $this->Flash->success(__('The rilevamento has been deleted.'));
        } else {
            $this->Flash->error(__('The rilevamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function ClonaRilevamento($id = null)
    {


        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        // Verifica se il rilevamento è aperto
        $IdRilevamento=isset($this->request->query['id']) ? $this->request->query['id'] : null;

        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Rilevamento' . " --- ".
            'Action = '. 'Clona' . " --- ".
            //   'Query = ' . $processoFiltered .  " --- ".
            'Parameter RilevamentoToClone = ' . $IdRilevamento);

        // $this->request->allowMethod(['post', 'delete']);



       // $IdRilevamento = $this->Rilevamento->get($id);



        $conn = ConnectionManager::get('default');
        $SqlStored =  "call CloneRilevamento (" . $IdRilevamento . ")";
        $results = $conn->execute($SqlStored);
        $this->Flash->success(__('Il rilevamento è stato CLONATO correttamente.'));
        return $this->redirect(['action' => 'index']);
    }
}
