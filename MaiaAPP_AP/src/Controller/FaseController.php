<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Session;
use Cake\Log\Log;
/**
 * Fase Controller
 *
 * @property \App\Model\Table\FaseTable $Fase
 *
 * @method \App\Model\Entity\Fase[] paginate($object = null, array $settings = [])
 */
class FaseController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $FasiSpecificoProcesso=isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        $this->set(compact('FasiSpecificoProcesso'));

            $this->loadModel('vlistprocesso');

            $processoFiltered = $this->vlistprocesso->find('list',[
                    'keyField' => 'IdProcesso',
                    'valueField' => 'NomeProcesso']
            )->where(['IDDipartimento' => $idDipartimento, 'IDSezione' => $idSezione, 'idrilevamento'=> $IdRilevamento ]);

          $this->set(compact('processoFiltered'));




        $this->loadModel('vlistfase');
        if (is_null($FasiSpecificoProcesso)==true) {
            $conditions = array('conditions' => array('iddipartimento' => $idDipartimento, 'idsezione' => $idSezione, 'idrilevamento'=> $IdRilevamento));
        }else{
            $conditions = array('conditions' => array('iddipartimento' => $idDipartimento, 'idsezione' => $idSezione,
                        'processo_id' => $FasiSpecificoProcesso,
                         'idrilevamento'=> $IdRilevamento));

        }


        $FaseFiltered =  $this->vlistfase->find('all', $conditions);
        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Fase' . " --- ".
            'Action = '. 'Index-SearchProcessFase' . " --- ".
            'Query = ' . $FaseFiltered .  " --- ".
            'Parameter' . serialize($conditions));

        $fase = $this->paginate($FaseFiltered);
        $this->set(compact('fase'));
        $this->set('_serialize', ['fase']);
    }

    public function SearchFasiProcesso()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $passedArgs = $this->request->getData('RicercaFasiProcesso');

        $this->loadModel('vlistfase');
        $conditions =  array('conditions'=>array('iddipartimento' =>  $idDipartimento,'idsezione' => $idSezione,
            'processo_id'=>$passedArgs ));

        $FaseFiltered =  $this->vlistfase->find('all', $conditions);


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Fase' . " --- ".
            'Action = '. 'SearchFasiProcesso' . " --- ".
            'Query = ' . $FaseFiltered .  " --- ".
            'Parameter' . serialize($conditions));

        $this->loadModel('vlistprocesso');

        $processoFiltered = $this->vlistprocesso->find('list',[
                'keyField' => 'IdProcesso',
                'valueField' => 'NomeProcesso']
        )->where(['IDDipartimento' => $idDipartimento, 'IDSezione' => $idSezione, 'idrilevamento'=> $IdRilevamento ]);

        $this->set(compact('processoFiltered'));

        $fase = $this->paginate($FaseFiltered);
        $this->set(compact('fase'));
        $this->set('_serialize', ['fase']);

        $FasiSpecificoProcesso=$passedArgs;
        $this->set(compact('FasiSpecificoProcesso'));

        $this->render('index');

    }


    /**
     * View method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $getIDFase=isset($this->request->query['idfase']) ? $this->request->query['idfase'] : null;
        $getIDProcesso=isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        $IDProcesso= $getIDProcesso;
        $this->set(compact('IDProcesso'));


        $this->loadModel('vlistdipendentefase');
        $vlistdipendenteFaseFiltered= $this->vlistdipendentefase->find('all', array('conditions' => array('idprocesso = ' => ($IDProcesso),
            'idfase = ' => ($getIDFase)
            )));

        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Fase' . " --- ".
            'Action = '. 'View' . " --- ".
            'Query = ' . $vlistdipendenteFaseFiltered .  " --- ".
            'Parameter-Idprocesso' . $IDProcesso);

        $this->set(compact('vlistdipendenteFaseFiltered'));
        $this->set('_serialize', ['vlistdipendenteFaseFiltered']);

        // modifica nome processo
        $this->loadModel('Processo');
        $ProcessoConditions = array('conditions' => array('idrilevamento'=> $IdRilevamento, 'Idprocesso'=>$IDProcesso));

        $ProcessoFiltered =  $this->Processo->find('all', $ProcessoConditions);
   //     debug($ProcessoFiltered->count());
        $this->set('ProcessoFiltered',$ProcessoFiltered);
        $this->set('_serialize', ['ProcessoFiltered']);

        $fase = $this->Fase->get($getIDFase, [
            'contain' => ['Struttura', 'Processo', 'Dipendente']
        ]);

        $this->set('fase', $fase);
        $this->set('_serialize', ['fase']);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');


        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Fase' . " --- ".
            'Action = '. 'add' . " --- ".
            'Query = ' . 'NONE');

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }

        $getIDProcesso=isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        $fase = $this->Fase->newEntity();


        if ($this->request->is('post')) {

            Log::write('warning',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                'controller = ' . 'Fase' . " --- ".
                'Action = '. 'add' . " --- ".
                //  'Query = ' . $processoFiltered .  " --- ".
                'Parameter FaseToADD' . serialize( $this->request->getData()));

            // Verifica che il nome fase sia unico nel rilevamento, sul dipartimento e nella sezione
            $errore = false;
            $erroremsg = '';
            $NomeNuovaFase = trim($this->request->getData('NomeFase'));
            $Processo_id = $this->request->getData('Processo_id');
            $this->loadModel('vlistfase');
            $CheckFase = $this->vlistfase->find('all',
                array('conditions' => array(
                    'NomeFase like' => $NomeNuovaFase . '%',
                    'processo_id' => $Processo_id,
                    'IdRilevamento' => $IdRilevamento)));
            if ($CheckFase->count() > 0) {
                $errore = true;
                $erroremsg = 'ERRORE: non è possibile inserire una fase con lo stesso nome di un fase già esistente sul processo';
            }
            // fine verifica

            if ($errore) {
                $this->Flash->error($erroremsg);
            }else
            {
                $fase = $this->Fase->patchEntity($fase, $this->request->getData());
                if ($this->Fase->save($fase)) {
                    $this->Flash->success(__('La fase è stata salvata correttamente.'));
                    return $this->redirect(
                        array(
                            "controller" => "fase",
                            "action" => "index",
                            "?" => array(
                                "idprocesso" => $getIDProcesso
                            )
                        ));
            }
            $this->Flash->error(__('Errore. Si è verficato un errore nell\'inserimento della fase. Contattare l\'amministratore del sistema'));
            }
        }


        $this->loadModel('struttura');
        $CStrutturaFiltered = $this->struttura->find('list',array('fields' =>array(
            'keyField' => 'IdStruttura',
            'valueField' => 'NomeStruttura'
        ), 'conditions'=> array('Dipartimento_Id' =>  $idDipartimento,'Sezione_Id' => $idSezione )));
        $CStrutturaFiltered = $this->struttura->find('list',[
                'keyField' => 'IdStruttura',
                'valueField' => 'NomeStruttura']
        )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione ]);

        $this->set(compact('CStrutturaFiltered'));

        $idprocesso = $getIDProcesso;

       // $dipendente = $this->Fase->Dipendente->find('list', ['limit' => 200]);
        $this->loadModel('processo');
        $processoTable = $this->processo->find('all', array('conditions' => array('Idprocesso =' =>$getIDProcesso )));

        $CheckIdProcesso = $processoTable->first()->NomeProcesso;
       //  debug($CheckIdProcesso);
        $this->set(compact('fase', 'struttura', 'idprocesso','CheckIdProcesso'));
        $this->set('_serialize', ['fase']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');

        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }


        $getIDFase=isset($this->request->query['idfase']) ? $this->request->query['idfase'] : null;
        $getIDProcesso=isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        $IDProcesso= $getIDProcesso;
        $this->set(compact('IDProcesso'));


        $fase = $this->Fase->get($getIDFase, [
            'contain' => ['Dipendente']
        ]);


        $this->loadModel('struttura');
        $CStrutturaFiltered = $this->struttura->find('list',array('fields' =>array(
            'keyField' => 'IdStruttura',
            'valueField' => 'NomeStruttura'
        ), 'conditions'=> array('Dipartimento_Id' =>  $idDipartimento,'Sezione_Id' => $idSezione )));
        $CStrutturaFiltered = $this->struttura->find('list',[
                'keyField' => 'IdStruttura',
                'valueField' => 'NomeStruttura']
        )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione ]);

        $this->set(compact('CStrutturaFiltered'));

        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Fase' . " --- ".
            'Action = '. 'Edit-FaseTOEdit' . " --- ".
            'Query = ' . $fase .  " --- ".
            'Parameter $getIDFase = ' . $getIDFase);

        if ($this->request->is(['patch', 'post', 'put'])) {

            // Verifica che il nome fase sia unico nel rilevamento, sul dipartimento e nella sezione
            $errore = false;
            $erroremsg = '';
            $NomeNuovaFase = trim($this->request->getData('NomeFase'));
            $Processo_id = $this->request->getData('Processo_id');

            $this->loadModel('vlistfase');

            $conditions2 = array(
                'NomeFase like' => $NomeNuovaFase . '%',
                'processo_id' => $Processo_id,
                'idfase !=' => $getIDFase,
                'IdRilevamento' => $IdRilevamento);
            $CheckFase = $this->vlistfase->find('all',
                array('conditions' => array(
                    'NomeFase like' => $NomeNuovaFase . '%',
                    'processo_id' => $Processo_id,
                    'idfase !=' => $getIDFase,
                    'IdRilevamento' => $IdRilevamento)));

            Log::write('info',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                'controller = ' . 'Fase' . " --- ".
                'Action = '. 'Edit-CheckNomeFase' . " --- ".
                'Query = ' . $CheckFase .  " --- ".
                'Parameter = ' . serialize($conditions2));

            if ($CheckFase->count() > 0) {
                $errore = true;
                $erroremsg = 'ERRORE: non è possibile modificare il nome  fase con lo stesso nome di un fase già esistente sul processo';
            }
            // fine verifica

            if ($errore) {
                $this->Flash->error($erroremsg);
            } else {
                Log::write('warning',
                    'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                    'controller = ' . 'Fase' . " --- ".
                    'Action = '. 'Edit' . " --- ".
                    //  'Query = ' . $processoFiltered .  " --- ".
                    'Parameter FaseModified = ' . serialize( $this->request->getData()));

                $fase = $this->Fase->patchEntity($fase, $this->request->getData());
                if ($this->Fase->save($fase)) {
                    $this->Flash->success(__('The modifica della fase è stata effettuata regolarmente.'));
                    return $this->redirect(
                        array(
                            "controller" => "fase",
                            "action" => "index",
                            "?" => array(
                                "idprocesso" => $getIDProcesso
                            )
                        ));
                }
                $this->Flash->error(__('Errore. Si è verficato un errore nella modifica della fase. Contattare l\'amministratore del sistema'));
            }
        }
        $dipendente = $this->Fase->Dipendente->find('list', ['limit' => 200]);
        $this->set(compact( 'dipendente', 'fase'));

        $this->set('_serialize', ['fase']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
        // Verifica se il rilevamento è aperto
        If ($StatoRilevamento > 0) {
            $erroremsg = ' RILEVAMENTO CHIUSO: Non è possibile inserire, modificare, cancellare informazioni relative a processi, fasi e carichi di lavoro sulle risorse  per un rilevamento chiuso';
            $this->Flash->error($erroremsg);
            return $this->redirect($this->referer());
        }

        $getIDFase=isset($this->request->query['idfase']) ? $this->request->query['idfase'] : null;
        $getIDProcesso=isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        Log::write('warning',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'Fase' . " --- ".
            'Action = '. 'Delete' . " --- ".
            //   'Query = ' . $processoFiltered .  " --- ".
            'Parameter FaseToDelete = ' . $getIDFase. '-- RelatedProcess= '. $getIDProcesso);

        $this->request->allowMethod(['post', 'delete']);
        $fase = $this->Fase->get($getIDFase);

        // NOTA ho dovuto impostare  'quoteIdentifiers' => true, in app.php in config
        $conn = ConnectionManager::get('default');
        $SqlStored =  "call DeleteFase (" . $getIDFase . ")";

        $results = $conn->execute($SqlStored);
        $this->Flash->success(__('La fase è stata cancellata correttamente.'));

        return $this->redirect(
            array(
                "controller" => "fase",
                "action" => "index",
                "?" => array(
                    "idprocesso" => $getIDProcesso
                )
            ));
    }
}
