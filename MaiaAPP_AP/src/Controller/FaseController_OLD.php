<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Session;
/**
 * Fase Controller
 *
 * @property \App\Model\Table\FaseTable $Fase
 *
 * @method \App\Model\Entity\Fase[] paginate($object = null, array $settings = [])
 */
class FaseController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $FasiSpecificoProcesso=isset($this->request->query['idprocesso']) ? $this->request->query['idprocesso'] : null;

        $this->set(compact('FasiSpecificoProcesso'));
//      debug($FasiSpecificoProcesso);
        if (is_null($FasiSpecificoProcesso)==true){
            $this->loadModel('vlistprocesso');

            $processoFiltered = $this->vlistprocesso->find('list',[
                    'keyField' => 'IdProcesso',
                    'valueField' => 'NomeProcesso']
            )->where(['IDDipartimento' => $idDipartimento, 'IDSezione' => $idSezione ]);

        $this->set(compact('processoFiltered'));

        }


        $this->loadModel('vlistfase');
        if (is_null($FasiSpecificoProcesso)==true) {
            $conditions = array('conditions' => array('iddipartimento' => $idDipartimento, 'idsezione' => $idSezione));
        }else{
            $conditions = array('conditions' => array('iddipartimento' => $idDipartimento, 'idsezione' => $idSezione, 'processo_id' => $FasiSpecificoProcesso));

        }

        $FaseFiltered =  $this->vlistfase->find('all', $conditions);

        $fase = $this->paginate($FaseFiltered);

        $this->set(compact('fase'));
        $this->set('_serialize', ['fase']);
    }

    public function SearchFasiProcesso()
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $passedArgs = $this->request->getData('RicercaFasiProcesso');

        $this->loadModel('vlistfase');
        $conditions =  array('conditions'=>array('iddipartimento' =>  $idDipartimento,'idsezione' => $idSezione,
            'processo_id'=>$passedArgs ));
        $FaseFiltered =  $this->vlistfase->find('all', $conditions);

        $fase = $this->paginate($FaseFiltered);

        $this->set(compact('fase'));
        $this->set('_serialize', ['fase']);

        $FasiSpecificoProcesso=$passedArgs;
        $this->set(compact('FasiSpecificoProcesso'));

        $this->render('index');

    }


    /**
     * View method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $getIDFase=isset($this->request->query['Idfase']) ? $this->request->query['Idfase'] : null;
        $getIDProcesso=isset($this->request->query['Idprocesso']) ? $this->request->query['Idprocesso'] : null;

        $IDProcesso= $getIDProcesso;
        $this->set(compact('IDProcesso'));

        $fase = $this->Fase->get($getIDFase, [
            'contain' => ['Struttura', 'Processo', 'Dipendente']
        ]);

        $this->set('fase', $fase);
        $this->set('_serialize', ['fase']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $getIDProcesso=isset($this->request->query['IDprocesso']) ? $this->request->query['IDprocesso'] : null;


        $fase = $this->Fase->newEntity();


        if ($this->request->is('post')) {
            $fase = $this->Fase->patchEntity($fase, $this->request->getData());
            if ($this->Fase->save($fase)) {
                $this->Flash->success(__('The fase è stata salvata correttamente.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Errore. Si è verficato un errore nell\'inserimento della fase. Contattare l\'amministratore del sistema'));
        }


        $this->loadModel('struttura');
        $CStrutturaFiltered = $this->struttura->find('list',array('fields' =>array(
            'keyField' => 'IdStruttura',
            'valueField' => 'NomeStruttura'
        ), 'conditions'=> array('Dipartimento_Id' =>  $idDipartimento,'Sezione_Id' => $idSezione )));
        $CStrutturaFiltered = $this->struttura->find('list',[
                'keyField' => 'IdStruttura',
                'valueField' => 'NomeStruttura']
        )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione ]);

        $this->set(compact('CStrutturaFiltered'));

        $IDprocesso = $getIDProcesso;

       // $dipendente = $this->Fase->Dipendente->find('list', ['limit' => 200]);
        $this->loadModel('processo');
        $processoTable = $this->processo->find('all', array('conditions' => array('Idprocesso =' =>$getIDProcesso )));

        $CheckIdProcesso = $processoTable->first()->NomeProcesso;
       //  debug($CheckIdProcesso);
        $this->set(compact('fase', 'struttura', 'IDprocesso','CheckIdProcesso'));
        $this->set('_serialize', ['fase']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');

        $getIDFase=isset($this->request->query['Idfase']) ? $this->request->query['Idfase'] : null;
        $getIDProcesso=isset($this->request->query['Idprocesso']) ? $this->request->query['Idprocesso'] : null;

        $IDProcesso= $getIDProcesso;
        $this->set(compact('IDProcesso'));


        $fase = $this->Fase->get($getIDFase, [
            'contain' => ['Dipendente']
        ]);


        $this->loadModel('struttura');
        $CStrutturaFiltered = $this->struttura->find('list',array('fields' =>array(
            'keyField' => 'IdStruttura',
            'valueField' => 'NomeStruttura'
        ), 'conditions'=> array('Dipartimento_Id' =>  $idDipartimento,'Sezione_Id' => $idSezione )));
        $CStrutturaFiltered = $this->struttura->find('list',[
                'keyField' => 'IdStruttura',
                'valueField' => 'NomeStruttura']
        )->where(['Dipartimento_Id' => $idDipartimento, 'Sezione_Id' => $idSezione ]);

        $this->set(compact('CStrutturaFiltered'));




        if ($this->request->is(['patch', 'post', 'put'])) {
            $fase = $this->Fase->patchEntity($fase, $this->request->getData());
            if ($this->Fase->save($fase)) {
                $this->Flash->success(__('The fase has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fase could not be saved. Please, try again.'));
        }
   //     $struttura = $this->Fase->Struttura->find('list', ['limit' => 200]);
   //     $processo = $this->Fase->Processo->find('list', ['limit' => 200]);

   //     $this->set(compact('fase', 'struttura', 'processo', 'dipendente'));
        $dipendente = $this->Fase->Dipendente->find('list', ['limit' => 200]);
        $this->set(compact( 'dipendente', 'fase'));

        $this->set('_serialize', ['fase']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fase id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fase = $this->Fase->get($id);

        // NOTA ho dovuto impostare  'quoteIdentifiers' => true, in app.php in config
        $conn = ConnectionManager::get('default');
        $SqlStored =  "call DeleteFase (" . $id . ")";
        //   debug($SqlStored);
        $results = $conn->execute($SqlStored);
        $this->Flash->success(__('La fase è stata cancellata correttamente.'));

        return $this->redirect(['action' => 'index']);
    }
}
