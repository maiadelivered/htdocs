<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Iterator\ExtractIterator;
use Cake\Log\Log;

/**
 * Vlistusers Controller
 *
 * @property \App\Model\Table\VlistusersTable $Vlistusers
 *
 * @method \App\Model\Entity\Vlistuser[] paginate($object = null, array $settings = [])
 */
class VlistusersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $vlistusers = $this->paginate($this->Vlistusers);

        $this->set(compact('vlistusers'));
        $this->set('_serialize', ['vlistusers']);
    }

    /**
     * View method
     *
     * @param string|null $id Vlistuser id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vlistuser = $this->Vlistusers->get($id, [
            'contain' => []
        ]);

        $this->set('vlistuser', $vlistuser);
        $this->set('_serialize', ['vlistuser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Users');
        $vlistuser = $this->Users->newEntity();


        $this->loadModel('sezione');
        $CStrutturaFiltered = $this->sezione->find('list',[
            'keyField' => 'idsezione', 'valueField' => ['CheckIdDipartimento', 'NomeSezione'],
            'order'=> array('CheckIdDipartimento' => 'asc','NomeSezione' => 'asc') ]);
        $this->set(compact('CStrutturaFiltered'));

        if ($this->request->is('post')) {

            $vlistuser = $this->Users->patchEntity($vlistuser, $this->request->getData());

            //verifico che lo username sia unico
            $errore = false;
            $erroremsg = '';
            $CheckUserName = $this->Users->find('all',
                array('conditions' => array(
                    'Username =' => $vlistuser->username)));
            if ($CheckUserName->count() > 0) {
                $errore = true;
                $erroremsg = 'ERRORE: username già assegnato ad un altro user cambiare username';
            }
            // fine verifica

            if ($errore) {
                Log::write('error',
                    'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                    'controller = ' . 'VlistUserController' . " --- ".
                    'Action = '. 'ADD' . " --- ".
                    'Query = ' . 'NONE' .
                    'MSG= tentato inserimento di uno username già assegnato' );
                $this->Flash->error($erroremsg);

            }else{


                $getCurrentSezione=$vlistuser->idSezione;

                // cerco la sezione per sapere il dipartimento

                $CheckDip = $this->sezione->find('all',
                    array('conditions' => array(
                        'idsezione' => $getCurrentSezione)));

                $getCurrentDipartimento = 0;

                if ($CheckDip->count() > 0) {
                    $getCurrentDipartimento = $CheckDip->first()->Dipartimento_id;
                }

                $vlistuser->idDipartimento=$getCurrentDipartimento;

                if ($this->Users->save($vlistuser)) {
                    Log::write('info',
                        'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                        'controller = ' . 'VlistUserController' . " --- ".
                        'Action = '. 'ADD' . " --- ".
                        'Query = ' . 'NONE' .
                        'MSG= utente inserito correttamente' );
                    $this->Flash->success(__("L'utente è stato inserito con successo"));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__("ERRORE: L'utente NON è stato inserito"));
            }
        }
        $this->set(compact('vlistuser'));
        $this->set('_serialize', ['vlistuser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vlistuser id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');




        Log::write('info',
            'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
            'controller = ' . 'VlistUserController' . " --- ".
            'Action = '. 'edit' . " --- ".
            'Query = ' . 'NONE');



        $this->loadModel('Users');
        $vlistuser = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->loadModel('sezione');

        $CStrutturaFiltered = $this->sezione->find('list',[
                'keyField' => 'IdSezione',
                'valueField' => ['CheckIdDipartimento', 'NomeSezione'],
                'order'=> array('CheckIdDipartimento' => 'asc','NomeSezione' => 'asc') ]);

        $this->set(compact('CStrutturaFiltered'));

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadModel('users');
            $vlistuser = $this->Users->patchEntity($vlistuser, $this->request->getData());

        //    debug($vlistuser->idSezione);


            $errore = false;
            $erroremsg= "";

            if (strcasecmp($vlistuser->name, 'administrator') === 0){
                $errore = true;
                $erroremsg= "NON E' POSSIBILE MODIFICARE I DATI DELL'UTENTE AMMINISTRATORE";

            };


            if ($errore) {
                Log::write('warning',
                    'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                    'controller = ' . 'VlistUserController' . " --- ".
                    'Action = '. 'EDIT' . " --- ".
                    'Query = ' . 'NONE' .
                    'MSG= TENTATA MODIFICA UTENTE' );
                $this->Flash->error($erroremsg);
            }else {

                $getCurrentSezione = $vlistuser->idSezione;
              //  debug('$getCurrentSezione=' . $getCurrentSezione);


                // cerco la sezione per sapere il dipartimento

                $CheckDip = $this->sezione->find('all',
                    array('conditions' => array(
                        'idsezione' => $getCurrentSezione)));

                $getCurrentDipartimento = 0;

                if ($CheckDip->count() > 0) {
                    $getCurrentDipartimento = $CheckDip->first()->Dipartimento_id;
                    //       debug($getCurrentDipartimento);
                }
                $vlistuser->idDipartimento = $getCurrentDipartimento;

                if ($this->Vlistusers->save($vlistuser)) {
                    $this->Flash->success(__('I dati dell\'utente sono stati aggiornati.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Si è verificato un ERRORE. Contattare l\'amministratote del sistema'));
            }
        }
        $this->set(compact('vlistuser'));
        $this->set('_serialize', ['vlistuser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vlistuser id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $idSezione = $this->request->session()->read('User.idSezione');
        $idDipartimento = $this->request->session()->read('User.idDipartimento');
        $nomeSezione = $this->request->session()->read('Sezione.NomeSezione');
        $nomeDipartimento = $this->request->session()->read('Dipartimento.NomeDipartimento');
        $IdRilevamento = $this->request->session()->read('IdRilevamento');


        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Users');

        $vlistuser = $this->Users->get($id);


        $errore = false;
        $erroremsg= "";

        if (strcasecmp($vlistuser->name, 'administrator') === 0){
            Log::write('error',
                'UserName = ' . $this->request->session()->read('Auth.User.name') . " --- ".
                'controller = ' . 'VlistUserController' . " --- ".
                'Action = '. 'delete' . " --- ".
                'Query = ' . 'NONE' .
                'MSG= tentata cancellazione utente amministratore' );
            $errore = true;
            $erroremsg= "NON E' POSSIBILE ELIMINARE L'UTENTE AMMINISTRATORE";
        };
        if ($errore) {
            $this->Flash->error($erroremsg);
        }else {
            if ($this->Users->delete($vlistuser)) {
                $this->Flash->success(__('L\'utente è stato cancellato.'));
            } else {
                $this->Flash->error(__('ERRORE. Contattare l\' amministratore del sistema'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }


}
