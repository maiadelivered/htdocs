<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DipendenteFase[]|\Cake\Collection\CollectionInterface $dipendenteFase
 */
use Cake\Routing\Router;
?>
<br/>
<div class="related">
    <nav class="navbar navbar-default" height=40px >
        <div class="container-fluid">
            <a class="navbar-brand"  href="#" > Allocazione delle risorse sul processo
                <?=strtoupper($NomeProcesso)?>
            </a>
        </div>
    </nav>


<?php
// inizio parte riepilogo
$StatoRilevamento = $this->request->session()->read('StatoRilevamento');
if ((sizeof($vlistdipendenteFaseFilteredData)==0))
{
    ?>
    <h3 align="center" > Non sono stati allocate risorse sulla fase  </h3>
<?php }

else{
    ?>

    <table class="table table-striped table-hover" width="100%" cellpadding="0" cellspacing="0">

        <thead>
        <tr>

            <th scope="col" class="info" ><?= 'PROCESSO' ?></th>
            <th scope="col" class="info"><?=  'FASE' ?>

                <?php
                for($i = 0; $i < (sizeof($CDipendenteFilteredData));$i++) {
                ?>
            <th scope="col" class="info" align="left"> <?= $this->Form->postLink(__($CDipendenteFilteredData[$i]['Dipendente']),
                    ['controller'=>'dipendentefase',
                        'action' => 'indexd', '?'=>
                        array('IdDipendente'=>$CDipendenteFilteredData[$i]['IdDipendente']
                        )], array('style'=> 'align:left; ')
                );

                // $CDipendenteFilteredData[$i]['Dipendente'] ?>

            </th>


            <?php
            }
            ?>

        </tr>
        </thead>
        <tbody>

        <?php
        $conteggioinse= 0 ;
        $fasecorrrente = 0;
        $conteggioCaricoEffettivo= 0;

       //  debug (sizeof($FaseFilteredData));
        for($j = 0; $j < ((sizeof($FaseFilteredData)));$j++) {
        $faseProcesso = $FaseFilteredData[$j]['IdFase'];
        ?>
        <tr>
            <td><?= $FaseFilteredData[$j]['CheckIdProcesso'];
                $NomeProcesso= $FaseFilteredData[$j]['CheckIdProcesso'];
                $IdProcesso= $FaseFilteredData[$j]['Processo_id'];
                ?></td>
            <td>
                <?php
                $NomeFase= $FaseFilteredData[$j]['NomeFase'];
                $IdFase= $FaseFilteredData[$j]['IdFase'];
                $NomeStruttura= $FaseFilteredData[$j]['CheckIdStruttura'];

                echo $this->Html->link(__( $FaseFilteredData[$j]['NomeFase']),
                    array('controller' => 'DipendenteFase', 'action' => 'Index',
                        '?'=> array('idprocesso'=>$FaseFilteredData[$j]['Processo_id']
                        ,'idfase'=>$FaseFilteredData[$j]['IdFase'])));
                ?>
            </td>
            <?php

            if ($conteggioCaricoEffettivo<sizeof($vlistdipendenteFaseFilteredData)) {
                $faseCaricoCorrrente= $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdFase'];
                $IddipendenteCaricoCorrrente =  $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdDipendente'];
            }
            if ($faseCaricoCorrrente != $faseProcesso){
                for ($d=0; $d < (sizeof($CDipendenteFilteredData));$d++) {
                    //  echo '<td>' . $i . '= i - Ciclo4' . $CDipendenteFilteredData[$d]['Dipendente'] . '--' . '</td>';
                    echo '<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -- </td>';
                }

            }else {
                // $faseCaricoCorrrente == $faseProcesso
                // almeno un carico è stato assegnato alla fase
                for ($d = 0; $d < (sizeof($CDipendenteFilteredData)); $d++) {
                    if (($IddipendenteCaricoCorrrente == $CDipendenteFilteredData[$d]['IdDipendente'])
                        and ($faseCaricoCorrrente == $faseProcesso)) {
                        echo '<td align="left" ' .
                            'title= \'PROCESSO:' . $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['NomeProcesso'] . ' &#013;FASE: ' .
                            $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['NomeFase'] . '&#013;DIPENDENTE: ' .
                            $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['Dipendente'] . '\'' .
                            ' > <b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' .
                            $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['PercImpegnoSuFase'].' </b> </br>' ;
                        If ($StatoRilevamento = 0) {
                            echo $this->Form->postLink(__('Modifica'),
                                ['action' => 'indexp', '?' =>
                                    array('id' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdEsegue'],
                                        'Azione' => 'edit',
                                        'idprocesso' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdProcesso'],
                                        'idfase' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdFase'],
                                        'IdDipendente' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdDipendente'],
                                        'Dipendente' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['Dipendente'],
                                        'NomeProcesso' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['NomeProcesso'],
                                        'NomeFase' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['NomeFase'],
                                        'NomeStruttura' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['NomeStruttura'],
                                        'returnpage' => 'indexp'
                                    )],
                                array('class' => 'btn btn-warning btn-sm', 'style' => 'height:25px;width:60px'));
                            echo '   ' . $this->Form->postLink(__('Elimina'),
                                    ['action' => 'delete', '?' =>
                                        array('id' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdEsegue'],
                                            'idprocesso' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdProcesso'],
                                            'idfase' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdFase'],
                                            'returnpage' => 'indexp',
                                            'NomeProcesso' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['NomeProcesso'],
                                            'IdDipendente' => $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdDipendente'])],
                                    array('class' => 'btn btn-danger btn-sm', 'style' => 'height:25px;width:60px', 'confirm' => __(
                                        'Si è sicuri di voler eliminare il carico di lavoro del dipendente {0} sulla fase?',
                                        $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['Dipendente'])));
                        }
                        echo '</td>';
                        $conteggioinse = $conteggioinse + 1;
                        // se effettuo un inserimento allora devo verificare se anche gli altri sono carichi
                        //  per la stessa fase quindi aumento di uno la righa dei carichi

                        $conteggioCaricoEffettivo = $conteggioCaricoEffettivo + 1;
                        if ($conteggioCaricoEffettivo < sizeof($vlistdipendenteFaseFilteredData)) {
                            $faseCaricoCorrrente = $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdFase'];
                            $IddipendenteCaricoCorrrente = $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdDipendente'];

                        }
                    } else {
                        // echo '<td>' .  $CDipendenteFilteredData[$d]['Dipendente'] . '--' .'</td>';
                        echo '<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -- </td>';
                    }
                };

            }

            echo '</tr>';
            }

            ?>



        </tbody>

    </table>

<?php }
// fine parte riepilogo
?>

<?php
$StatoRilevamento = $this->request->session()->read('StatoRilevamento');
if ($StatoRilevamento == 0){
if (!(isset($azionetodo)) or (strcasecmp($azionetodo, 'edit') != 0)) {
    if ($CFaseFiltered->count() == 0) {

        echo '<h3 align="center" > Non è possibile inserire carichi perchè non state definite le fasi del processo.  </h3>';

    } else { ?>
        <div class="form-group" id="NewCarico">
        <nav class="navbar navbar-default" height=100px>
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    Inserimento carico di lavoro nel processo <?= strtoupper($NomeProcesso); ?>
                </a>
                </br></br></br></br></br>

            </div>
        </nav>

        <script>
            $(document).ready(function () {

                $('#fase-id').change(function (e) {
                    var textValue;
                    textValue = $("#fase-id option:selected").text();

                    $('input[name=CheckIdFase]').val(textValue);

                });
                $('#fase-id').trigger('change');


                $('#dipendente-id').change(function (e) {
                    var textValue;
                    textValue = $("#dipendente-id option:selected").text();
                    $('input[name=checkDipendente]').val(textValue);
                });
                $('#dipendente-id').trigger('change');

                // And now fire change event when the DOM is ready


                $('#CPercImpegnoSuFase').change(function (e) {
                    var textValue;
                    textValue = $("#CPercImpegnoSuFase option:selected").val();
                    $('input[name=PercImpegnoSuFase]').val(textValue);
                });

                // And now fire change event when the DOM is ready
                $('#CPercImpegnoSuFase').trigger('change');

            });
        </script>


    <?= $this->Form->create($dipendenteFase) ?>
        <div class="well">

            <table class="table table-active" cellpadding="0" cellspacing="0">
                <tbody>
                <fieldset>
                    <tr>
                        <td valign="middle" align="center">
                            <?php

                            echo $this->Form->input('Fase_id', array(
                                'type' => 'select',
                                'label' => 'Fase',
                                'options' => $CFaseFiltered,
                                'value' => 1,
                                'class' => 'form-control',
                                'style' => 'width: 400px;'

                            ));
                            ?>
                        </td>
                        <td valign="middle" align="center">
                            <?php

                            echo $this->Form->input('Dipendente_id', array(
                                'type' => 'select',
                                'label' => 'Dipendente',
                                'options' => $CDipendenteFiltered,
                                'value' => 1,
                                'class' => 'form-control',
                                'style' => 'width: 400px;'

                            )); ?>
                        </td>
                        <td valign="middle" align="center">
                            <b> NUOVO carico di lavoro </b>
                            <select id="CPercImpegnoSuFase" class="form-control" , style="width: 100px;">
                                <option value="0" disabled>0 % (per inserire 0% eliminare il carico)</option>
                                <option value="5" selected>5 %</option>
                                <option value="10">10 %</option>
                                <option value="15">15 %</option>
                                <option value="20">20 %</option>
                                <option value="25">25 %</option>
                                <option value="30">30 %</option>
                                <option value="35">35 %</option>
                                <option value="40">40 %</option>
                                <option value="45">45 %</option>
                                <option value="50">50 %</option>
                                <option value="55">55 %</option>
                                <option value="60">60 %</option>
                                <option value="65">65 %</option>
                                <option value="70">70 %</option>
                                <option value="75">75 %</option>
                                <option value="80">80 %</option>
                                <option value="85">85 %</option>
                                <option value="90">90 %</option>
                                <option value="95">95 %</option>
                                <option value="100">100 %</option>
                            </select>

                        </td>
                    </tr>
                    <div hidden="true">
                        <input type="text" name="returnpage" id="returnpage" value="indexp"/>
                        <?php
                        // echo $this->Form->control('Fase_id', array('value' => $getIDFase, 'type' => 'text'));
                        echo $this->Form->control('idFase', array('type' => 'text'));
                        echo $this->Form->control('CheckIdFase', array('type' => 'text'));
                        echo $this->Form->control('STRUTTURA', array('value' => $NomeStruttura));
                        echo $this->Form->control('checkDipendente', array());
                        echo $this->Form->control('PercImpegnoSuFase', array('hidden' => true, 'label' => ''));
                        echo $this->Form->control('idprocesso', array('value' => $getIDProcesso, 'hidden' => true, 'label' => ''));
                        $IdRilevamento = $this->request->session()->read('IdRilevamento');
                        echo $this->Form->control('idfase', array('value' => $getIDFase, 'hidden' => true, 'label' => ''));
                        $IdRilevamento = $this->request->session()->read('IdRilevamento');
                        echo $this->Form->control('IdRilevamento', array('value' => $IdRilevamento, 'hidden' => true, 'label' => '')); ?>

                    </div>
                </fieldset>
                <tr>
                    <td align="center" colspan="3">
                        <?php

                        echo $this->Form->button(
                            'Inserisci nuovo carico',
                            array('class' => 'btn btn-primary',
                                'formaction' => Router::url(
                                    array('controller' => 'DipendenteFase', 'action' => 'Add', '?' =>
                                        array('idprocesso' => $idprocesso, 'returnpage'=>'indexp')
                                    )
                                ))); ?>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <?= $this->Form->end() ?>
        <?php
    }
}
    ?>

<?php

if (isset($azionetodo)) :

if ((strcasecmp( $azionetodo, 'edit' ) == 0 )) :

    ?>
    <div class="form-group" id="EditCarico" >
    <nav class="navbar navbar-default" height=100px >
        <div class="container-fluid">
            <a class="navbar-brand"  href="#" >
                Modifica carico di lavoro del dipendente
                    <?= $Dipendente// $NomeProcesso?>

                nella fase "<?= $NomeFase ?>"
                del processo "<?= $NomeProcesso ?>"

              </a>
            </br></br></br>
            </br>


        </div>
    </nav>
    <script>
        $(document).ready(function(){

            $('#dipendente-id').change(function(e){
                var textValue;
                textValue = $( "#dipendente-id option:selected" ).text();
                $('input[name=checkDipendente]').val(textValue);
            });
            $('#dipendente-id').trigger('change');

            // And now fire change event when the DOM is ready


            $('#CPercImpegnoSuFase').change(function(e){
                var textValue;
                textValue = $( "#CPercImpegnoSuFase option:selected" ).val();
                $('input[name=PercImpegnoSuFase]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#CPercImpegnoSuFase').trigger('change');

        });
    </script>

    <?= $this->Form->create($dipendenteFaseEdit) ?>
        <div class="well">
        <table class="table table-active" cellpadding="0" cellspacing="0">
            <tbody>
            <fieldset>
                <tr>
                    <td valign="middle" align="center">
                        <b> Dipendente </b> <br/><?php
                        echo $Dipendente;
                        ?>

                    </td>
                    <td valign="middle" align="center">
                        <b> Fase </b> <br/>
                        <?php

                        $IdEsegue=$dipendenteFaseEdit->IdEsegue;
                        $Fase_id=$dipendenteFaseEdit->CheckIdFase;
                        $CheckIdFase=$dipendenteFaseEdit->CheckIdFase;
                        $STRUTTURA=$dipendenteFaseEdit->STRUTTURA;
                        $PercImpegnoSuFase = (int)($dipendenteFaseEdit->PercImpegnoSuFase);

                        $checkDipendente2 = strtoupper(trim($dipendenteFaseEdit->checkDipendente));


                        echo $CheckIdFase;
                        /*echo  $this->Form->input('Dipendente_id', array(
                            'type'=>'select',
                            'label'=>'Dipendente',
                            'options'=>$CDipendenteFiltered,
                            'value'=> $dipendenteFaseEdit->dipendente_id,
                            'class' =>'form-control',
                            'style'=> 'width: 400px;'

                        ));
*/
                        ?>
                    </td>
                    <td valign="middle" align="center">
                       <b> Modifica il carico di lavoro del dipendente selezionato</b>
                        <select id="CPercImpegnoSuFase" class="form-control"  class="form-control",  style="width: 100px;">
                            <option value="0" selected disabled>0 % (per inserire 0% eliminare il carico)</--option>
                            <option value="5" <?php if ( $PercImpegnoSuFase == 5) { echo ' selected '; } ?> >5 %</option>
                            <option value="10" <?php if ( $PercImpegnoSuFase == 10) { echo ' selected '; } ?>>10 %</option>
                            <option value="15" <?php if ( $PercImpegnoSuFase == 15) { echo ' selected '; } ?>>15 %</option>
                            <option value="20" <?php if ( $PercImpegnoSuFase == 20) { echo ' selected '; } ?>>20 %</option>
                            <option value="25" <?php if ( $PercImpegnoSuFase == 25) { echo ' selected '; } ?>>25 %</option>
                            <option value="30" <?php if ( $PercImpegnoSuFase == 30) { echo ' selected '; } ?>>30 %</option>
                            <option value="35" <?php if ( $PercImpegnoSuFase == 35) { echo ' selected '; } ?>>35 %</option>
                            <option value="40" <?php if ( $PercImpegnoSuFase == 40) { echo ' selected '; } ?>>40 %</option>
                            <option value="45" <?php if ( $PercImpegnoSuFase == 45) { echo ' selected '; } ?>>45 %</option>
                            <option value="50" <?php if ( $PercImpegnoSuFase == 50) { echo ' selected '; } ?>>50 %</option>
                            <option value="55" <?php if ( $PercImpegnoSuFase == 55) { echo ' selected '; } ?>>55 %</option>
                            <option value="60" <?php if ( $PercImpegnoSuFase == 60) { echo ' selected '; } ?>>60 %</option>
                            <option value="65" <?php if ( $PercImpegnoSuFase == 65) { echo ' selected '; } ?>>65 %</option>
                            <option value="70" <?php if ( $PercImpegnoSuFase == 70) { echo ' selected '; } ?>>70 %</option>
                            <option value="75" <?php if ( $PercImpegnoSuFase == 75) { echo ' selected '; } ?>>75 %</option>
                            <option value="80" <?php if ( $PercImpegnoSuFase == 80) { echo ' selected '; } ?>>80 %</option>
                            <option value="85" <?php if ( $PercImpegnoSuFase == 85) { echo ' selected '; } ?>>85 %</option>
                            <option value="90" <?php if ( $PercImpegnoSuFase == 90) { echo ' selected '; } ?>>90 %</option>
                            <option value="95" <?php if ( $PercImpegnoSuFase == 95) { echo ' selected '; } ?>>95 %</option>
                            <option value="100" <?php if ( $PercImpegnoSuFase == 100) { echo ' selected '; } ?>>100 %</option>

                        </select>
                        <div hidden="true">
                            <input type="text" name="returnpage" id="returnpage" value="indexp"/>
                            <?php
                            echo $this->Form->control('IdEsegue', array('value' => $IdEsegue, 'type'=>'imput'));
                            echo $this->Form->control('Fase_id', array('value' => $getIDFase, 'type'=>'text'));
                            echo $this->Form->control('CheckIdFase',array('value'=>$CheckIdFase));
                            echo $this->Form->control('STRUTTURA',array('value'=>$STRUTTURA));
                            echo $this->Form->control('checkDipendente',array('value'=> $checkDipendente2));
                            echo $this->Form->control('PercImpegnoSuFase', array('hidden'=>true, 'label'=>'', 'value'=>$PercImpegnoSuFase));
                            echo $this->Form->control('idprocesso', array('value'=> $getIDProcesso,'hiddenwwww'=>true, 'label'=>''));
                            $IdRilevamento = $this->request->session()->read('IdRilevamento');
                            echo $this->Form->control('idfase', array('value'=>$vlistdipendenteFaseFiltered->IdFase,'hiddenwwww'=>true, 'label'=>''));
                            $IdRilevamento = $this->request->session()->read('IdRilevamento');
                            echo $this->Form->control('IdRilevamento', array('value'=>$IdRilevamento,'hidden'=>true, 'label'=>''));
                            ?>
                        </div>
                    </td>
                </tr>
            </fieldset>
                <tr >
                    <td colspan="3" align="center">
                        <?=
                        $this->Form->button(
                            'Modifica il carico di lavoro',
                            array('class' => 'btn btn-primary',
                                'formaction' => Router::url(
                                    array('controller' => 'DipendenteFase','action' => 'edit', 'returnpage'=>'indexp','NomeProcesso'=>$NomeProcesso )
                                )
                            )
                        );

                        ?>
                        <?=$this->Form->button(_('Ripristina'), array('type'=>'reset','class' => 'btn btn-primary"'))?>
                        <?=
                        $this->Form->button(
                            'Annulla Modifica',
                            array('class' => 'btn btn-primary',
                                'formaction' => Router::url(
                                    array('controller' => 'DipendenteFase','action' => 'indexp','?'=>
                                    array('idprocesso'=> $idprocesso, 'NomeProcesso'=>$NomeProcesso )
                                )
                            ))); ?>


                    </td>
                </tr>

            </tbody>
        </table>
        </div>

    <?= $this->Form->end() ?>
</div>
    </div>
    <?php
endif;
endif;
}?>


