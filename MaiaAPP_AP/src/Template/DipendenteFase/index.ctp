<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DipendenteFase[]|\Cake\Collection\CollectionInterface $dipendenteFase
 */
use Cake\Routing\Router;
?>
<br/>
<div class="related">
    <nav class="navbar navbar-default" height=100px >
        <div class="container-fluid">
            <a class="navbar-brand"  href="#" > Riepilogo dei carichi di lavoro sulla fase
                <?= strtoupper($NomeFase) ?>
                del processo <?= strtoupper($NomeProcesso) ?></a>

            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav">
                    <?php
                    echo ($this->Html->link(__('Elenco Fasi'),
                        array('controller' => 'Fase', 'action' => 'index',
                            '?'=>
                                array('idprocesso'=>$idprocesso

                                )
                        )))
                    ?>
                </li>
            </ul>

        </div>
    </nav>
    <?php
    $StatoRilevamento = $this->request->session()->read('StatoRilevamento');

    if (($vlistdipendenteFaseFiltered->count()==0)) {
        ?>
        <h3 align="center" > Non sono stati inseriti carichi di lavoro sulla fase </h3>
        <?php }
        else{

        ?>


        <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">

            <thead>
            <tr>
                <th scope="col" class="info"><?= $this->Paginator->sort('Nominativo') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Categoria','Qualifica') ?></th>
                <th scope="col"  class="info"><?= $this->Paginator->sort('PO_AP', 'Posizione Organizzativa') ?></th>
                <th scope="col"  class="info"><?= $this->Paginator->sort('PercImpegnoSuFase', 'Percentuale di Impegno su Fase') ?></th>

               <?php   if ($StatoRilevamento == 0) { ?>
                   <th scope="col" class="info" width="120"><?= __('Modifica') ?></th>
                    <th scope="col" class="info" width="120"><?= __('Elimina') ?></th>
                <?php   } ?>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($vlistdipendenteFaseFiltered as $vlistdipendenteFaseFiltered): ?>
            <tr>
                <td><?= h($vlistdipendenteFaseFiltered->Dipendente) ?></td>
                <td><?= h($vlistdipendenteFaseFiltered->Categoria) ?></td>
                <td><?= h($vlistdipendenteFaseFiltered->PO_AP) ?></td>
                <td><?= h($vlistdipendenteFaseFiltered->PercImpegnoSuFase) ?></td>
                <?php   if ($StatoRilevamento == 0) { ?>
                <td class="actions">  <?= $this->Form->postLink(__('Modifica carico'),
                        ['action' => 'index', '?'=>
                            array('id'=> $vlistdipendenteFaseFiltered->IdEsegue,
                                'Azione'=>'edit',
                                'idprocesso'=>$vlistdipendenteFaseFiltered->IdProcesso,
                                'idfase'=>$vlistdipendenteFaseFiltered->IdFase
                            )],
                        array('class'=>'btn btn-warning btn-sm')
                        ); ?>
                </td>
                <td>
                <?= $this->Form->postLink(__('Cancella carico'),
                    ['action' => 'delete', '?'=>
                        array('id'=> $vlistdipendenteFaseFiltered->IdEsegue,
                            'idprocesso'=>$vlistdipendenteFaseFiltered->IdProcesso,
                            'idfase'=>$vlistdipendenteFaseFiltered->IdFase,
                            'returnpage'=>'index')],

                 array('class'=>'btn btn-danger btn-sm','confirm' => __(
                     'Si è sicuri di voler eliminare il carico di lavoro del dipendente {0} sulla fase?',
                     $vlistdipendenteFaseFiltered->Dipendente))); ?>
                </td>
                    <?php  } ?>
            </tr>
            </tbody>
            <?php endforeach; ?>
        </table>
    <?php } ?>
</div>


<?php
$StatoRilevamento = $this->request->session()->read('StatoRilevamento');
if ($StatoRilevamento == 0){
if (!(isset($azionetodo)) or (strcasecmp($azionetodo, 'edit') != 0)) :

?>
<div class="form-group" id="NewCarico">
    <nav class="navbar navbar-default" height=100px >
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                Inserimento carico di lavoro nella fase <?= strtoupper($NomeFase) ?>
                del processo <?= strtoupper($NomeProcesso) ?>
            </a>
<br/><br/><br/><br/>
        </div>

    </nav>

    <script>
        $(document).ready(function () {

            $('#dipendente-id').change(function (e) {
                var textValue;
                textValue = $("#dipendente-id option:selected").text();
                $('input[name=checkDipendente]').val(textValue);
            });
            $('#dipendente-id').trigger('change');

            // And now fire change event when the DOM is ready


            $('#CPercImpegnoSuFase').change(function (e) {
                var textValue;
                textValue = $("#CPercImpegnoSuFase option:selected").val();
                $('input[name=PercImpegnoSuFase]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#CPercImpegnoSuFase').trigger('change');

        });
    </script>

    <?= $this->Form->create($dipendenteFase) ?>
    <div class="well">
        <table class="table table-active" cellpadding="0" cellspacing="0">
            <tbody>
            <fieldset>
                <tr>
                    <td valign="middle" align="center">
                        <?php
                        echo $this->Form->input('Dipendente_id', array(
                            'type' => 'select',
                            'label' => 'Dipendente',
                            'options' => $CDipendenteFiltered,
                            'value' => 1,
                            'class' => 'form-control',
                            'style' => 'width: 400px;'

                        )); ?>
                    </td>
                    <td valign="middle" align="center">
                        <b> Seleziona carico di lavoro </b>
                        <select id="CPercImpegnoSuFase" class="form-control" , style="width: 100px;">
                            <option value="5" selected>5 %</option>
                            <option value="10">10 %</option>
                            <option value="15">15 %</option>
                            <option value="20">20 %</option>
                            <option value="25">25 %</option>
                            <option value="30">30 %</option>
                            <option value="35">35 %</option>
                            <option value="40">40 %</option>
                            <option value="45">45 %</option>
                            <option value="50">50 %</option>
                            <option value="55">55 %</option>
                            <option value="60">60 %</option>
                            <option value="65">65 %</option>
                            <option value="70">70 %</option>
                            <option value="75">75 %</option>
                            <option value="80">80 %</option>
                            <option value="85">85 %</option>
                            <option value="90">90 %</option>
                            <option value="95">95 %</option>
                            <option value="100">100 %</option>
                        </select>

                    </td>
                </tr>
                <div hidden="true">

                    <?php
                    echo $this->Form->control('Fase_id', array('value' => $getIDFase, 'type' => 'text'));
                    echo $this->Form->control('CheckIdFase', array('value' => $NomeFase));
                    echo $this->Form->control('STRUTTURA', array('value' => $NomeStruttura));
                    echo $this->Form->control('checkDipendente', array());
                    echo $this->Form->control('PercImpegnoSuFase', array('hidden' => true, 'label' => ''));
                    echo $this->Form->control('idprocesso', array('value' => $getIDProcesso, 'hidden' => true, 'label' => ''));
                    $IdRilevamento = $this->request->session()->read('IdRilevamento');
                    echo $this->Form->control('idfase', array('value' => $getIDFase, 'hidden' => true, 'label' => ''));
                    $IdRilevamento = $this->request->session()->read('IdRilevamento');
                    echo $this->Form->control('IdRilevamento', array('value' => $IdRilevamento, 'hidden' => true, 'label' => '')); ?>

                </div>
            </fieldset>
            <tr>
                <td align="center" colspan="2">
                    <?php

                   echo  $this->Form->button(
                        'Inserisci nuovo carico',
                        array('class' => 'btn btn-primary',
                            'formaction' => Router::url(
                                array('controller' => 'DipendenteFase', 'action' => 'Add', '?' =>
                                    array('idprocesso' => $idprocesso, 'idfase' => $idfase, 'returnpage' =>'index')
                                )
                            ))); ?>

                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <?= $this->Form->end() ?>
    <?php ;
    endif;
    ?>

<?php

if (isset($azionetodo)) :

if ((strcasecmp( $azionetodo, 'edit' ) == 0 )) :

    ?>
    <div class="form-group" id="EditCarico" >
    <nav class="navbar navbar-default" height=100px >
        <div class="container-fluid">
            <a class="navbar-brand"  href="#" >
                Modifica il carico di lavoro già assegnato al dipendente
                    <?= strtoupper($dipendenteFaseEdit->checkDipendente) ;?>
                nella fase <?= strtoupper($NomeFase) ?>
                del processo <?= strtoupper($NomeProcesso) ?>

              </a>
            </br></br></br>
            </br>
        </div>
    </nav>
    <script>
        $(document).ready(function(){

            $('#dipendente-id').change(function(e){
                var textValue;
                textValue = $( "#dipendente-id option:selected" ).text();
                $('input[name=checkDipendente]').val(textValue);
            });
            $('#dipendente-id').trigger('change');

            // And now fire change event when the DOM is ready


            $('#CPercImpegnoSuFase').change(function(e){
                var textValue;
                textValue = $( "#CPercImpegnoSuFase option:selected" ).val();
                $('input[name=PercImpegnoSuFase]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#CPercImpegnoSuFase').trigger('change');

        });
    </script>

    <?= $this->Form->create($dipendenteFaseEdit) ?>
        <div class="well">
        <table class="table table-active" cellpadding="0" cellspacing="0">
            <tbody>
            <fieldset>
                <tr>
                    <td valign="middle" align="center">
                        <?php

                        $IdEsegue=$dipendenteFaseEdit->IdEsegue;
                        $Fase_id=$dipendenteFaseEdit->CheckIdFase;
                        $CheckIdFase=$dipendenteFaseEdit->CheckIdFase;
                        $STRUTTURA=$dipendenteFaseEdit->STRUTTURA;
                        $PercImpegnoSuFase = (int)($dipendenteFaseEdit->PercImpegnoSuFase);

                        $checkDipendente2 = strtoupper(trim($dipendenteFaseEdit->checkDipendente));
                        echo  $this->Form->input('Dipendente_id', array(
                            'type'=>'select',
                            'label'=>'Dipendente',
                            'options'=>$CDipendenteFiltered,
                            'value'=> $dipendenteFaseEdit->dipendente_id,
                            'class' =>'form-control',
                            'style'=> 'width: 400px;'

                        ));

                        ?>
                    </td>
                    <td valign="middle" align="center">
                       <b> Modifica il carico di lavoro del dipendente selezionato</b>
                        <select id="CPercImpegnoSuFase" class="form-control"  class="form-control",  style="width: 100px;">
                            <option value="5" <?php if ( $PercImpegnoSuFase == 5) { echo ' selected '; } ?> >5 %</option>
                            <option value="10" <?php if ( $PercImpegnoSuFase == 10) { echo ' selected '; } ?>>10 %</option>
                            <option value="15" <?php if ( $PercImpegnoSuFase == 15) { echo ' selected '; } ?>>15 %</option>
                            <option value="20" <?php if ( $PercImpegnoSuFase == 20) { echo ' selected '; } ?>>20 %</option>
                            <option value="25" <?php if ( $PercImpegnoSuFase == 25) { echo ' selected '; } ?>>25 %</option>
                            <option value="30" <?php if ( $PercImpegnoSuFase == 30) { echo ' selected '; } ?> >30 %</option>
                            <option value="35" <?php if ( $PercImpegnoSuFase == 35) { echo ' selected '; } ?>>35 %</option>
                            <option value="40" <?php if ( $PercImpegnoSuFase == 40) { echo ' selected '; } ?>>40 %</option>
                            <option value="45" <?php if ( $PercImpegnoSuFase == 45) { echo ' selected '; } ?>>45 %</option>
                            <option value="50" <?php if ( $PercImpegnoSuFase == 50) { echo ' selected '; } ?>>50 %</option>
                            <option value="55" <?php if ( $PercImpegnoSuFase == 55) { echo ' selected '; } ?>>55 %</option>
                            <option value="60" <?php if ( $PercImpegnoSuFase == 60) { echo ' selected '; } ?>>60 %</option>
                            <option value="65" <?php if ( $PercImpegnoSuFase == 65) { echo ' selected '; } ?>>65 %</option>
                            <option value="70" <?php if ( $PercImpegnoSuFase == 70) { echo ' selected '; } ?>>70 %</option>
                            <option value="75" <?php if ( $PercImpegnoSuFase == 75) { echo ' selected '; } ?>>75 %</option>
                            <option value="80" <?php if ( $PercImpegnoSuFase == 80) { echo ' selected '; } ?>>80 %</option>
                            <option value="85" <?php if ( $PercImpegnoSuFase == 85) { echo ' selected '; } ?>>85 %</option>
                            <option value="90" <?php if ( $PercImpegnoSuFase == 90) { echo ' selected '; } ?>>90 %</option>
                            <option value="95" <?php if ( $PercImpegnoSuFase == 95) { echo ' selected '; } ?>>95 %</option>
                            <option value="100" <?php if ( $PercImpegnoSuFase == 100) { echo ' selected '; } ?>>100 %</option>

                        </select>
                        <div hidden="true">
                            <input type="text" name="returnpage" id="returnpage" value="index"/>
                            <?php
                            echo $this->Form->control('IdEsegue', array('value' => $IdEsegue, 'type'=>'imput'));
                            echo $this->Form->control('Fase_id', array('value' => $getIDFase, 'type'=>'text'));
                            echo $this->Form->control('CheckIdFase',array('value'=>$CheckIdFase));
                            echo $this->Form->control('STRUTTURA',array('value'=>$STRUTTURA));
                            echo $this->Form->control('checkDipendente',array('value'=> $checkDipendente2));
                            echo $this->Form->control('PercImpegnoSuFase', array('hidden'=>true, 'label'=>'', 'value'=>$PercImpegnoSuFase));
                            echo $this->Form->control('idprocesso', array('value'=> $getIDProcesso,'hiddenwwww'=>true, 'label'=>''));
                            $IdRilevamento = $this->request->session()->read('IdRilevamento');
                            echo $this->Form->control('idfase', array('value'=>$vlistdipendenteFaseFiltered->IdFase,'hiddenwwww'=>true, 'label'=>''));
                            $IdRilevamento = $this->request->session()->read('IdRilevamento');
                            echo $this->Form->control('IdRilevamento', array('value'=>$IdRilevamento,'hidden'=>true, 'label'=>''));
                            ?>
                        </div>
                    </td>
                </tr>
            </fieldset>
                <tr >
                    <td colspan="2" align="center">
                        <?=
                        $this->Form->button(
                            'Salva modifica',
                            array('class' => 'btn btn-primary',
                                'formaction' => Router::url(
                                    array('controller' => 'DipendenteFase','action' => 'edit', 'returnpage'=>'index')
                                )
                            )
                        );

                        ?>
                        <?=$this->Form->button(_('Elimina modifiche'), array('type'=>'reset','class' => 'btn btn-primary"'))?>
                        <?=
                        $this->Form->button(
                            'Annulla Modifica',
                            array('class' => 'btn btn-primary',
                                'formaction' => Router::url(
                                    array('controller' => 'DipendenteFase','action' => 'index','?'=>
                                    array('idprocesso'=> $idprocesso , 'idfase'=> $idfase)
                                )
                            ))); ?>


                    </td>
                </tr>

            </tbody>
        </table>
        </div>

    <?= $this->Form->end() ?>
</div>
    </div>
    <?php
endif;
endif;
}?>


