<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DipendenteFase $dipendenteFase
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dipendente Fase'), ['action' => 'edit', $dipendenteFase->IdEsegue]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dipendente Fase'), ['action' => 'delete', $dipendenteFase->IdEsegue], ['confirm' => __('Are you sure you want to delete # {0}?', $dipendenteFase->IdEsegue)]) ?> </li>
        <li><?= $this->Html->link(__('List Dipendente Fase'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dipendente Fase'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dipendente'), ['controller' => 'Dipendente', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dipendente'), ['controller' => 'Dipendente', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fase'), ['controller' => 'Fase', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fase'), ['controller' => 'Fase', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dipendenteFase view large-9 medium-8 columns content">
    <h3><?= h($dipendenteFase->IdEsegue) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dipendente') ?></th>
            <td><?= $dipendenteFase->has('dipendente') ? $this->Html->link($dipendenteFase->dipendente->IdDipendente, ['controller' => 'Dipendente', 'action' => 'view', $dipendenteFase->dipendente->IdDipendente]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fase') ?></th>
            <td><?= $dipendenteFase->has('fase') ? $this->Html->link($dipendenteFase->fase->IdFase, ['controller' => 'Fase', 'action' => 'view', $dipendenteFase->fase->IdFase]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdFase') ?></th>
            <td><?= h($dipendenteFase->CheckIdFase) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('STRUTTURA') ?></th>
            <td><?= h($dipendenteFase->STRUTTURA) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckDipendente') ?></th>
            <td><?= h($dipendenteFase->checkDipendente) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdEsegue') ?></th>
            <td><?= $this->Number->format($dipendenteFase->IdEsegue) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PercImpegnoSuFase') ?></th>
            <td><?= $this->Number->format($dipendenteFase->PercImpegnoSuFase) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdRilevamento') ?></th>
            <td><?= $this->Number->format($dipendenteFase->IdRilevamento) ?></td>
        </tr>
    </table>
</div>
