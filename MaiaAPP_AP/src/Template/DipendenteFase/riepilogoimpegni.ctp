<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DipendenteFase[]|\Cake\Collection\CollectionInterface $dipendenteFase
 */
use Cake\Routing\Router;
?>
</div>
<div class="related">
    <nav class="navbar navbar-default" height=100px >
        <div class="container-fluid">
            <a class="navbar-brand"  href="#" > RIEPILOGO CARICHI LAVORO

               </a>

            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav">
                    <?php

                    ?>
                </li>

            </ul>

        </div>
    </nav>
    <?php
    if ((sizeof($vlistdipendenteFaseFilteredData)==0))
        {
        ?>
        <h3 align="center" > Non sono stati allocate risorse sulla fase  </h3>
        <?php }

        else{
        ?>


        <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">

            <thead>
            <tr>

                <th scope="col" class="info" ><?= 'PROCESSO' ?></th>
                <th scope="col" class="info"><?=  'FASE' ?>

                <?php
                    for($i = 0; $i < (sizeof($CDipendenteFilteredData));$i++) {
    ?>
                <th scope="col" class="info"> <?= $this->Form->postLink(__(strtoupper($CDipendenteFilteredData[$i]['Dipendente'])),
                        ['controller'=>'dipendentefase',
                            'action' => 'indexd', '?'=>
                            array('IdDipendente'=>$CDipendenteFilteredData[$i]['IdDipendente']
                            )]
                    );

                    // $CDipendenteFilteredData[$i]['Dipendente'] ?>

                </th>


                <?php
                    }
                  ?>

            </tr>
            </thead>
            <tbody>

            <?php
            $conteggioinse= 0 ;
            $fasecorrrente = 0;
            $conteggioCaricoEffettivo= 0;
            for($j = 0; $j < (sizeof($FaseFilteredData));$j++) {
                $faseProcesso = $FaseFilteredData[$j]['IdFase'];
                ?>
            <tr>
                    <td><?= $FaseFilteredData[$j]['CheckIdProcesso']; ?></td>
                <td>
                <?php

                echo $this->Html->link(__( $FaseFilteredData[$j]['NomeFase']),
                    array('controller' => 'DipendenteFase', 'action' => 'Index',
                        '?'=> array('idprocesso'=>$FaseFilteredData[$j]['Processo_id']
                            ,'idfase'=>$FaseFilteredData[$j]['IdFase'])));
                ?>
                </td>
                <?php

                if ($conteggioCaricoEffettivo<sizeof($vlistdipendenteFaseFilteredData)) {
                    $faseCaricoCorrrente = $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdFase'];
                    $IddipendenteCaricoCorrrente = $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdDipendente'];
                }
                if ($faseCaricoCorrrente != $faseProcesso){
                    for ($d=0; $d < (sizeof($CDipendenteFilteredData));$d++) {
                        //  echo '<td>' . $i . '= i - Ciclo4' . $CDipendenteFilteredData[$d]['Dipendente'] . '--' . '</td>';
                        echo '<td>' . '--' . '</td>';
                    }

                }else {
                    // $faseCaricoCorrrente == $faseProcesso
                    // almeno un carico è stato assegnato alla fase
                    for ($d = 0; $d < (sizeof($CDipendenteFilteredData)); $d++) {
                        if (($IddipendenteCaricoCorrrente == $CDipendenteFilteredData[$d]['IdDipendente'])
                            and ($faseCaricoCorrrente == $faseProcesso)) {
                            echo '<td ' .
                                'title= \'PROCESSO:' . $FaseFilteredData[$j]['CheckIdProcesso'] . ' &#013;FASE: ' .
                                $FaseFilteredData[$j]['NomeFase'] . '&#013;DIPENDENTE: ' .
                                $CDipendenteFilteredData[$d]['Dipendente'] . '\'' .
                                ' >' . $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['PercImpegnoSuFase'] . '</td>';
                            $conteggioinse = $conteggioinse + 1;
                            // se effettuo un inserimento allora devo verificare se anche gli altri sono carichi
                            //  per la stessa fase quindi aumento di uno la righa dei carichi
                            $conteggioCaricoEffettivo = $conteggioCaricoEffettivo + 1;
                            if ($conteggioCaricoEffettivo < sizeof($vlistdipendenteFaseFilteredData)) {
                                $faseCaricoCorrrente = $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdFase'];
                                $IddipendenteCaricoCorrrente = $vlistdipendenteFaseFilteredData[$conteggioCaricoEffettivo]['IdDipendente'];
                            }
                        } else {
                            // echo '<td>' .  $CDipendenteFilteredData[$d]['Dipendente'] . '--' .'</td>';
                            echo '<td>' . '--' . '</td>';
                        }
                    };

                }

                echo '</tr>';
                }

                ?>



            </tbody>

        </table>

    <?php
        }

 ?>
</div>


