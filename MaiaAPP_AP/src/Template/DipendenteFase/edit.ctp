<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dipendenteFase->IdEsegue],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dipendenteFase->IdEsegue)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dipendente Fase'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dipendente'), ['controller' => 'Dipendente', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dipendente'), ['controller' => 'Dipendente', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fase'), ['controller' => 'Fase', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fase'), ['controller' => 'Fase', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dipendenteFase form large-9 medium-8 columns content">
    <?= $this->Form->create($dipendenteFase) ?>
    <fieldset>
        <legend><?= __('Edit Dipendente Fase') ?></legend>
        <?php
            echo $this->Form->control('Dipendente_id', ['options' => $dipendente, 'empty' => true]);
            echo $this->Form->control('Fase_id', ['options' => $fase, 'empty' => true]);
            echo $this->Form->control('CheckIdFase');
            echo $this->Form->control('STRUTTURA');
            echo $this->Form->control('checkDipendente');
            echo $this->Form->control('PercImpegnoSuFase');
            echo $this->Form->control('IdRilevamento');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
