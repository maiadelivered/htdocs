<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Users[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Users'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
    <thead>
            <tr>
                <th class="info" scope="col"><?= $this->Paginator->sort('name', 'Utente') ?></th>
                <th class="info"  scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th class="info" scope="col"><?= $this->Paginator->sort('CheckIdDipartimento', 'Dipartimento') ?></th>
                <th class="info" scope="col"><?= $this->Paginator->sort('checkIdSezione', 'Sezione') ?></th>
                <th class="info" scope="col"><?= $this->Paginator->sort('username', 'Ueername') ?></th>
                <th  class="info" scope="col"><?= $this->Paginator->sort('UserRole', 'Ruolo') ?></th>
                <th  class="info" scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $users): ?>
            <tr>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->CheckIdDipartimento) ?></td>
                <td><?= h($users->checkIdSezione) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->UserRole) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Dettagli utente'),
                        array('controller' => 'Users', 'action' => 'view',
                            '?'=> array('Iduser'=>$users->id_user)));
                    ?>
                    <?= $this->Html->link(__('View'), ['action' => 'view', $users->id_user]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $users->id_user]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $users->id_user], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id_user)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
