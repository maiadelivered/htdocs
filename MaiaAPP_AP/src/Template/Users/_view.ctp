<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Users $users
 */

?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">

</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?php echo ($vlistusers->name) ?></h3>
    <table  class="table table-striped" class="vertical-table" cellpadding="0" cellspacing="0">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($vlistusers->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($users->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($users->password) ?></td>
        </tr>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($users->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('UserRole') ?></th>
            <td><?= h($usersV->UserRole) ?></td>
        </tr>

        <tr>
            <th scope="row"><?= __('IdDipartimento') ?></th>
            <td><?= h($usersV->idDipartimento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdSezione') ?></th>
            <td><?=  h($usersV->checkIdSezione) ?></td>
        </tr>

    </table>
</div>
