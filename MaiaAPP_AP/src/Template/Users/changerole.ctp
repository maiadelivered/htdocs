<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">

</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Cambia Sezione') ?></h3>
    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"  ><?= $this->Paginator->sort('NomeDipartimento', 'Dipartimento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('NomeSezione', 'Sezione') ?></th>
                <th scope="col" class="actions"> </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($DipartimentoList as $VlistDipaSezione): ?>
            <tr>
                <td><?= h($VlistDipaSezione->NomeDipartimento) ?></td>
                <td><?= h($VlistDipaSezione->NomeSezione) ?></td>

                <td>  <?= $this->Form->postLink(__('Cambia Utente'),
                            ['action' => 'changerole', '?'=>
                                array('idDipartimentotochange'=>$VlistDipaSezione->IdDipartimento,
                                    'idSezionetochange'=>$VlistDipaSezione->IdSezione,
                                    'nomeDipartimentotochange'=>$VlistDipaSezione->NomeDipartimento,
                                    'nomeSezionetochange'=>$VlistDipaSezione->NomeSezione
                                )],
                            array('class'=>'btn btn-warning'),
                            ['confirm' => __('Si è sicuri di voler cambiare utente? ')]); ?>
                     </td>


            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
