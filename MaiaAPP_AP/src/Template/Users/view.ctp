<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vlistuser $vlistuser
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">

</nav>
<div class="vlistusers view large-9 medium-8 columns content">
    <?php foreach ($vlistuser as $vlistusers){ ?>
    <h3><?php echo ($vlistuser->id_user) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($vlistuser->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($vlistuser->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($vlistuser->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($vlistuser->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdDipartimento') ?></th>
            <td><?= h($vlistuser->CheckIdDipartimento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdSezione') ?></th>
            <td><?= h($vlistuser->checkIdSezione) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Community') ?></th>
            <td><?= h($vlistuser->community) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gender') ?></th>
            <td><?= h($vlistuser->gender) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Language') ?></th>
            <td><?= h($vlistuser->language) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Location') ?></th>
            <td><?= h($vlistuser->location) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Short Bio') ?></th>
            <td><?= h($vlistuser->short_bio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Surname') ?></th>
            <td><?= h($vlistuser->surname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($vlistuser->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('UserRole') ?></th>
            <td><?= h($vlistuser->UserRole) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id User') ?></th>
            <td><?= $this->Number->format($vlistuser->id_user) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdDipartimento') ?></th>
            <td><?= $this->Number->format($vlistuser->idDipartimento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdSezione') ?></th>
            <td><?= $this->Number->format($vlistuser->idSezione) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Idrilevamento') ?></th>
            <td><?= $this->Number->format($vlistuser->idrilevamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Birthdate') ?></th>
            <td><?= h($vlistuser->birthdate) ?></td>
        </tr>
    </table>
    <?php
    }?>
</div>
