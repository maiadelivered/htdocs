<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase $fase
 */
use Cake\Routing\Router;
?>
<br/>
<div class="fase view large-9 medium-8 columns content">
    <nav class="navbar navbar-default" >
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Dettagli della fase <?php
                    echo($fase->NomeFase. " del processo " . $ProcessoFiltered->first()->NomeProcesso);

                    ?></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <?php
                    $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
                    if ($StatoRilevamento == 0){
                    ?>

                    <li class="nav navbar-nav"><?= $this->Html->link(__('Modifica fase'),
                            array('controller' => 'Fase', 'action' => 'edit',
                                '?' => array('idprocesso' => $fase->Processo_id, 'idfase' => $fase->IdFase))) ?>
                    <li><?= $this->Form->postLink(__('Elimina fase'),
                            ['action' => 'delete', '?' => array('idprocesso' => $fase->Processo_id, 'idfase' => $fase->IdFase)],
                            ['confirm' => __('Si è sicuri di voler eliminare la fase {0}? 
 L\'eliminazione della fase comporterà  l\'eliminazione dei relativi  carichi di lavoro. ', $fase->NomeFase)]) ?> </li>
                    <li>
                        <?php
                        }
                        ?>
                    </li>
                    <li  class="nav navbar-nav">
                        <?php
                        echo ($this->Html->link(__('Altre fasi dello stesso processo'),
                            array('controller' => 'Fase', 'action' => 'index',
                                '?'=>
                                    array('idprocesso'=>$fase->Processo_id)
                            )));
                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h4><?= __('Nome Fase') ?></h4>
            </td>
            <td>
                <?= $this->Text->autoParagraph($fase->NomeFase) ?>
            </td>
        </tr>

        <tr>
            <td>
                <h4><?= __('Tipo Evento Avvio') ?></h4>
            </td>
            <td>
                <?php if(((string)($fase->TipoEventoAvvio)) == ""): ?>

                    <p>Non è stato indicato l'evento di avvio </p>

                <?php else:  ?>
                    <?= $this->Text->autoParagraph($fase->TipoEventoAvvio) ?>
                <?php endif; ?>
            </td>
        </tr>

        <tr>
            <td>
                <h4><?= __('TipoOutput') ?></h4>
            </td>
            <td>
                <?php if(((string)($fase->TipoOutput)) == ""): ?>

                    <p>Non è stato indicato il tipo di output </p>

                <?php else:  ?>
                    <?=$this->Text->autoParagraph($fase->TipoOutput) ?>
                <?php endif; ?>
            </td>
        </tr>

        <tr>
            <td>
                <h4><?= __('Altre Unita Organizzative Coinvolte') ?></h4>
            </td>
            <td>
                <?php if(((string)($fase->AltreUnitaOrganizzativeCoinvolte)) == ""): ?>

                    <p>Non vi sono altre unità organizzative coinvolte </p>

                <?php else:  ?>
                    <?=$this->Text->autoParagraph($fase->AltreUnitaOrganizzativeCoinvolte) ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>
                  <h4><?= __('Periodicità') ?></h4>

            </td>
            <td>
                <?php if ((((string)$fase->Periodicita) == "")or ((string)$fase->Periodicita) == "ND"): ?>

                    <p>Non è stato fornito il dato relativo alla periodicità </p>

                <?php else:  ?>
                    <?= $this->Text->autoParagraph($fase->Periodicita) ?>

                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>
                <h4><?= __('La fase è eseguita nei mesi di: ') ?></h4>
            </td>
            <td>
                <?php
                $mesi = '';
                if (((string)$fase->Gennaio) === "x") $mesi = 'Gennaio';

                if (((string)$fase->Febbraio) === "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Febbraio';}

                if (((string)$fase->Marzo) === "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Marzo';}
                if (((string)$fase->Aprile) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Aprile';}
                if (((string)$fase->Maggio) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Maggio';};
                if (((string)$fase->Giugno) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Giugno';}
                if (((string)$fase->Luglio) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Luglio';}
                if (((string)$fase->Agosto) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Agosto';}
                if (((string)$fase->Settembre) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Settembre';}
                if (((string)$fase->Ottobre) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Ottobre';}
                if (((string)$fase->Novembre) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Novembre';}
                if (((string)$fase->Dicembre) == "x") {
                    if ($mesi <> '') {$mesi = $mesi . ', ';}
                    $mesi = $mesi .  'Dicembre';}

                if ($mesi === '') {
                    echo "Non è stato dischiato alcun mese";
                }else{
                    echo $mesi;
                };
                ?>

            </td>
        </tr>
        <tr>
            <td>

                <h4><?= __('N° Medio Giorni per il Completamento')?></h4>

            </td>
            <td>
                <p><?= $this->Text->autoParagraph($this->Number->format($fase->NumMedioGiorniComplet)) ?></p>
            </td>
        </tr>

        <tr>
            <td>
                <h4><?= __('Numero Iterazioni Annue')?></h4>
            </td>
            <td>
                <p><?= $this->Text->autoParagraph($this->Number->format($fase->NumIterazioniAnnue)) ?></p>
            </td>
        </tr>

        <tr>
            <td>
                <h4><?= __('Totale Giornate Lavorative Annue')?></h4>
            </td>
            <td>
                <p><?= $this->Text->autoParagraph($this->Number->format($fase->TotaleGiornateLavoroAnnue)) ?></p>
            </td>
        </tr>
        <tr>
            <td>
                <h4><?= __('Riferimenti Normativi')?></h4>
            </td>
            <td>
                <p><?=  $this->Text->autoParagraph(h($fase->RiferimentiNormativi)) ?></p>
            </td>
        </tr>


    </table>




    <div class="related">
        <nav class="navbar navbar-default" >
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Riepilogo delle risorse allocate sulla fase:  <?php echo ($fase->NomeFase)?></a>
                </div>
            </div>
        </nav>
        <?php

        if (($vlistdipendenteFaseFiltered->count()==0)) { ?>
            <h3 align="center" > Non sono presenti risorse allocate sulla fase.  </h3>
        <?php
        }else{ ?>

        <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">


            <thead>
            <tr>
                <th scope="col"><?= __('Dipendente') ?></th>
                <th scope="col"><?= __('DESC QUALIFICA') ?></th>
                <th scope="col"><?= __('PO_AP') ?></th>
                <th scope="col"><?= __('PercImpegnoSuFase') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php

            foreach ($vlistdipendenteFaseFiltered as $vlistdipendenteFaseFiltered): ?>
            <tr>
                <td><?= h($vlistdipendenteFaseFiltered->Dipendente) ?></td>
                <td><?= h($vlistdipendenteFaseFiltered->Categoria) ?></td>
                <td><?= h($vlistdipendenteFaseFiltered->PO_AP) ?></td>
                <td><?= h($vlistdipendenteFaseFiltered->PercImpegnoSuFase) ?></td>
            </tr>
            </tbody>
            <?php endforeach; ?>
        </table>
        <?php } ?>
    </div>
</div>

