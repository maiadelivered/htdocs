<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase[]|\Cake\Collection\CollectionInterface $fase
 */
use Cake\Routing\Router;
?>
</div>

<script type="text/javascript">
    function AlertProcesso() {
        alert ('Per inserire una fase, è obbligatorio selezionare il processo su cui inserire la fase attraverso il link di ricerca!!');
    }
</script>



<nav class="navbar navbar-default" >
    <div class="container-fluid">
        <div class="navbar-header">
            <?php
            // visualizzo il nome del processo se ho fatto una ricerca
            if (is_null($FasiSpecificoProcesso)or ($fase->count() == 0)){
            ?>
            <a class="navbar-brand" href="#"><?= __('Elenco delle fasi') ?></a>
               <?php }else{?>
                <a class="navbar-brand" href="#"> Fasi del processo:
                    <?php
                    $data = $fase->toArray();
                    $NomeProcesso= $data[0]->CheckIdProcesso;
                    echo($NomeProcesso);?>
                </a>

            <?php }?>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav">

                    <?php
                    $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
                    if ($StatoRilevamento == 0){

                        if (!is_null($FasiSpecificoProcesso))
                        {
                        echo $this->Html->image("Newfase.png", array(
                            "alt" => "Nuova Fase",
                            'url' => ['controller' => 'Fase', 'action' => 'add','?' => array('idprocesso' => $FasiSpecificoProcesso)],
                            'height'=>'40', 'width'=>'40'));

                         }else {
                            echo $this->Html->image("Newfase.png", array(
                            'alt' => 'Nuova fase',
                            'url' =>'#',
                            'onclick' => 'AlertProcesso()',
                            'height'=>'40', 'width'=>'40'));
                        }
                    }
                    ?>
                </li>
                <li  class="nav navbar-nav">
                    <a href="#" id= "ImgSearch"  >
                        <?= $this->Html->image('search.png',['height'=>'40', 'width'=>'40']) ?>
                    </a>
                </li>

                <script>
                    $( "#ImgSearch" ).click(function() {
                        if($("#Divricerca").is(':hidden')){
                            $( "#Divricerca").show( "slow" );
                        }else {
                            $( "#Divricerca").hide("slow");
                        }
                    });
                </script>



            </ul>
        </div>

    </div>

</nav>

<div class="form-group" id="Divricerca" hidden="true" >
    <?= $this->Form->create('Post') ?>
    <fieldset>
        <legend><?= __('Ricerca le fasi relative ad un processo') ?></legend>
        <table cellpadding="10" cellspacing="10">
            <tbody>
            <tr  valign="middle">
                <td valign="middle" width="700">
                    <?php
                    echo
                    $this->Form->input('RicercaFasiProcesso', array(
                        'type'=>'select',
                        'label'=>'Seleziona il nome del  Processo',
                        'class'=>'form-control',
                        'options'=>$processoFiltered,
                        'value'=> $FasiSpecificoProcesso  // indica il valore da selezionare
                    ));
                    ?>
                <td valign="bottom">
                    <?= $this->Form->button(
                        'Cerca',
                        array('class'=>'btn btn-primary',
                            'formaction' => Router::url(
                                array('controller' => 'fase','action' => 'SearchFasiProcesso')
                            )
                        )
                    );
                    ?>
                </td>
            </tr>
            </tbody>
        </table>

    </fieldset>

    <?= $this->Form->end() ?>
</div>

<?php if($fase->count()== 0) {

    ?>
<h3 align="center" > Non sono presenti fasi. Per inserire una fase usare il relativo pulsante.  </h3>
    <?php }else {

        ?>
<table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
    <thead>
    <tr>

        <th scope="col" class="info" width="300"><?= $this->Paginator->sort('CheckIdProcesso','Processo') ?></th>
        <th scope="col" class="info" width="300"><?= $this->Paginator->sort('CheckIdFase', 'Nome Fase') ?></th>
        <th scope="col" class="info" ><?=$this->Paginator->sort('TipoEventoAvvio', 'Evento di avvio') ?></th>

        <th scope="col" class="info" width="70px" align="center"><?= $this->Paginator->sort('CheckIdStruttura','Struttura di appartenenza') ?></th>
        <th scope="col" class="info" width="70px"><?= $this->Paginator->sort('AltreUnitaOrganizzativeCoinvolte','Altre UO coinvolte') ?></th>
        <th scope="col" class="info" width="70px"><?= $this->Paginator->sort('Periodicita', 'Periodicità') ?></th>

        <th scope="col" class="info" width="40px"><?= $this->Paginator->sort('NumMedioGiorniComplet', 'GG Medi') ?></th>
        <th scope="col" class="info" width="40px"><?= $this->Paginator->sort('NumIterazioniAnnue','Iterazioni Annue') ?></th>
        <th scope="col" class="info" width="40px"><?= $this->Paginator->sort('TotaleGiornateLavoroAnnue', 'Tot GG Lavoro Annue') ?></th>

        <th scope="col" class="info" width="150"><?= __(' ') ?></th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($fase as $fase): ?>
        <tr>


            <td ><?= h($fase->CheckIdProcesso) ?></td>
            <td><?= h($fase->NomeFase) ?></td>
            <td><?= h($fase->TipoEventoAvvio) ?></td>
            <td><?= h($fase->CheckIdStruttura) ?></td>
            <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
            <td align="center"><?= h($fase->Periodicita) ?></td>
            <td align="center"><?= $this->Number->format($fase->NumMedioGiorniComplet) ?></td>
            <td align="center"><?= $this->Number->format($fase->NumIterazioniAnnue) ?></td>
            <td align="center"><?= $this->Number->format($fase->TotaleGiornateLavoroAnnue) ?></td>

            <td class="actions">

                <div class="btn-group">
                    <a href="#" class="btn btn-default">Gestisci</a>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                    <ul class="dropdown-menu">


                        <li><?= $this->Html->link(__('Dettagli fase'),
                                array('controller' => 'Fase', 'action' => 'view',
                                    '?'=> array('idprocesso'=>$fase->Processo_id ,'idfase'=>$fase->IdFase)));
                            ?></li>
                        <li>

                        <li><?= $this->Html->link(__('Modifica fase'),
                                array('controller' => 'Fase', 'action' => 'edit',
                                    '?'=> array('idprocesso'=>$fase->Processo_id ,'idfase'=>$fase->IdFase)));
                            ?></li>
                        <li>

                        </li>
                        <li><?= $this->Form->postLink(__('Elimina fase'),
                                ['action' => 'delete', '?'=> array('idprocesso'=>$fase->Processo_id ,'idfase'=>$fase->IdFase)],
                                ['confirm' => __('Si è sicuri di voler eliminare la fase {0}? 
 L\'eliminazione della fase comporterà  l\'eliminazione dei relativi  carichi di lavoro. ', $fase->NomeFase)]) ?> </li>
                        <li class="divider"></li>
                        <li><?= $this->Html->link(__(' Aggiungi carichi'),
                                array('controller' => 'DipendenteFase', 'action' => 'Index',
                                    '?'=> array('idprocesso'=>$fase->Processo_id ,'idfase'=>$fase->IdFase)));
                            ?></li>
                    </ul>
                </div>

            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

    <div  align="center" style="width:100%">
        <ul class="pagination pagination"  >
            <?= $this->Paginator->first('<< ' . __('Primo')) ?>
            <?= $this->Paginator->prev('< ' . __('Precedente')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Successivo') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
        </ul>
        <p align="center"><?= $this->Paginator->counter(['format' => __('Pagina {{page}} di {{pages}}. Totale fasi presenti {{count}}')]) ?></p>
    </div>


<?php }?>
</div>
