<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fase $fase
 */
?>
<div>
<script>
    $(document).ready(function(){

        // And now fire change event when the DOM is ready
        $('#nomefase').change(function(e){

            var textValue;
            textValue = $( "#nomefase").val();
            $('input[name=CheckIdFase]').val(textValue);

        });
        // And now fire change event when the DOM is ready
        $('#nomefase').trigger('change');

        $('#nummediogiornicomplet').change(function(e){

            var textValue;
            var textValue2;
            var textValue3;
            textValue = $( "#nummediogiornicomplet").val();
            textValue2 = $( "#numiterazioniannue").val();
            // textValue3 = textValue1 * textValue2;
            textValue3 = parseInt(textValue)* parseInt(textValue2);

            $('input[name=TotaleGiornateLavoroAnnue]').val(textValue3);

        });
        // And now fire change event when the DOM is ready
        $('#nummediogiornicomplet').trigger('change');


        $('#numiterazioniannue').change(function(e){

            var textValue;
            var textValue2;
            var textValue3;
            textValue = $( "#nummediogiornicomplet").val();
            textValue2 = $( "#numiterazioniannue").val();
            textValue3 = parseInt(textValue)* parseInt(textValue2);

            $('input[name=TotaleGiornateLavoroAnnue]').val(textValue3);

        });
        // And now fire change event when the DOM is ready
        $('#numiterazioniannue').trigger('change');


        // And now fire change event when the DOM is ready
        $('#struttura-id').change(function(e){
            var textValue;
            textValue = $( "#struttura-id option:selected" ).text();
            $('input[name=CheckIdStruttura]').val(textValue);

        });
        // And now fire change event when the DOM is ready
        $('#struttura-id').trigger('change');

        $('#periodicitaselect').change(function(e){

            var textValue;
            textValue = $( "#periodicitaselect option:selected" ).text();
            $('input[name=Periodicita]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#periodicitaselect').trigger('change');

        $('#GennaioCB').change(function(e){

            var textValue;
            //  if ($( "#Gennaiocb option:checked")) {
            if (GennaioCB.checked==1) {

                $('input[name=Gennaio]').val('x');
            }
            else
            {
                $('input[name=Gennaio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#GennaioCB').trigger('change');

        $('#FebbraioCB').change(function(e){

            var textValue;
            //  if ($( "#Febbraiocb option:checked")) {
            if (FebbraioCB.checked==1) {

                $('input[name=Febbraio]').val('x');
            }
            else
            {
                $('input[name=Febbraio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#FebbraioCB').trigger('change');

        $('#MarzoCB').change(function(e){

            var textValue;
            //  if ($( "#Marzocb option:checked")) {
            if (MarzoCB.checked==1) {

                $('input[name=Marzo]').val('x');
            }
            else
            {
                $('input[name=Marzo]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#MarzoCB').trigger('change');

        $('#AprileCB').change(function(e){

            var textValue;
            //  if ($( "#Aprilecb option:checked")) {
            if (AprileCB.checked==1) {

                $('input[name=Aprile]').val('x');
            }
            else
            {
                $('input[name=Aprile]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#AprileCB').trigger('change');

        $('#MaggioCB').change(function(e){

            var textValue;
            //  if ($( "#Maggiocb option:checked")) {
            if (MaggioCB.checked==1) {

                $('input[name=Maggio]').val('x');
            }
            else
            {
                $('input[name=Maggio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#MaggioCB').trigger('change');



        $('#GiugnoCB').change(function(e){

            var textValue;
            //  if ($( "#Giugnocb option:checked")) {
            if (GiugnoCB.checked==1) {

                $('input[name=Giugno]').val('x');
            }
            else
            {
                $('input[name=Giugno]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#GiugnoCB').trigger('change');

        $('#LuglioCB').change(function(e){

            var textValue;
            //  if ($( "#Lugliocb option:checked")) {
            if (LuglioCB.checked==1) {

                $('input[name=Luglio]').val('x');
            }
            else
            {
                $('input[name=Luglio]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#LuglioCB').trigger('change');

        $('#AgostoCB').change(function(e){

            var textValue;
            //  if ($( "#Agostocb option:checked")) {
            if (AgostoCB.checked==1) {

                $('input[name=Agosto]').val('x');
            }
            else
            {
                $('input[name=Agosto]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#AgostoCB').trigger('change');
        $('#SettembreCB').change(function(e){

            var textValue;
            //  if ($( "#Settembrecb option:checked")) {
            if (SettembreCB.checked==1) {

                $('input[name=Settembre]').val('x');
            }
            else
            {
                $('input[name=Settembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#SettembreCB').trigger('change');

        $('#OttobreCB').change(function(e){

            var textValue;
            //  if ($( "#Ottobrecb option:checked")) {
            if (OttobreCB.checked==1) {

                $('input[name=Ottobre]').val('x');
            }
            else
            {
                $('input[name=Ottobre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#OttobreCB').trigger('change');

        $('#NovembreCB').change(function(e){

            var textValue;
            //  if ($( "#Novembrecb option:checked")) {
            if (NovembreCB.checked==1) {

                $('input[name=Novembre]').val('x');
            }
            else
            {
                $('input[name=Novembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#NovembreCB').trigger('change');

        $('#DicembreCB').change(function(e){

            var textValue;
            //  if ($( "#Dicembrecb option:checked")) {
            if (DicembreCB.checked==1) {

                $('input[name=Dicembre]').val('x');
            }
            else
            {
                $('input[name=Dicembre]').val('');
            }
        });

        // And now fire change event when the DOM is ready
        $('#DicembreCB').trigger('change');

        $('#CTipoOutput').change(function(e){

            var textValue;
            textValue = $( "#CTipoOutput option:selected" ).text();
            $('input[name=TipoOutput]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#CTipoOutput').trigger('change');

        $('#CTipoEventoAvvio').change(function(e){

            var textValue;
            textValue = $( "#CTipoEventoAvvio option:selected" ).text();
            $('input[name=TipoEventoAvvio]').val(textValue);
        });

        // And now fire change event when the DOM is ready
        $('#CTipoEventoAvvio').trigger('change');
    });
</script>
</div>
<div class="col-lg-8">
    <p class="bs-component">
    <fieldset>
        <h2> Inserisci una nuova fase al processo: </h2>
        <h2><?php echo($CheckIdProcesso)?></h2>
        <div class="well">
         <?= $this->Form->create($fase, array('class'=>'form-horizontal')); ?>

        <?php

            echo $this->Form->control('NomeFase', array
                ('class'=>'form-control',
                    'label'=>'Nome Fase',
                    'required'=>true ));

            echo $this->Form->input('Struttura_id', array(
                'type'=>'select',
                'label'=>'Struttura di apparteneza',
                'options'=>$CStrutturaFiltered,
                'class' =>'form-control',
                'value'=> 1 // StrutturaFiltered  // indica il valore da selezionare
            ));

            echo $this->Form->control('RiferimentiNormativi',array('class'=>'form-control'));

            ?>
            <b>Tipo di Output</b>
            <select id="CTipoOutput" class="form-control">
                <option value="1" selected>Linee strategiche e atti di indirizzo</option>
                <option value="2" >Piano/programma</option>
                <option value="3" >Relazione</option>
                <option value="4" >Report</option>
                <option value="5" >Proposta di Legge/Regolamento</option>
                <option value="6" >Proposta di DGR</option>
                <option value="7" >Determina Dirigenziale</option>
                <option value="8" >Circolare Interna</option>
                <option value="9" >Bando/avviso</option>
                <option value="10" >Convenzione</option>
                <option value="11" >Protocollo di intesa</option>
                <option value="12" >Verbale</option>
                <option value="13" >Graduatoria</option>
                <option value="14" >Atto di liquidazione</option>
                <option value="15" >Atto di rendicontazione</option>
                <option value="16" >Rendiconto economico-finanziario</option>
                <option value="17" >Provvedimento di autorizzazione</option>
                <option value="18" >Provvedimento di consegna</option>
                <option value="19" >Nota di corrispondenza</option>
                <option value="20" >Altro</option>
            </select>

            <b>Evento di avvio</b>

            <select id="CTipoEventoAvvio" class="form-control">
                <option value="1" selected >Obbligo normativo</option>
                <option value="2" >Istanza di parte</option>
                <option value="3" >Istanza d'ufficio</option>
                <option value="4" >Richiesta da un altro ufficio</option>
                <option value="5" >Altro</option>
            </select>

            <strong>Periodicità</strong>
            <?php echo $this->Form->select('periodicitaselect', [
                'SI' => 'SI',
                'NO' => 'NO',
                'ND' => 'NON DEFINIBILE'
                ], array('default' => $fase->Periodicita, 'id'=>"periodicitaselect",'class'=>'form-control')); ?>

            <b>Mesi dell'anno in cui si svolge la fase</b>
            <table class="table table-striped" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td>
                        <input type="checkbox" name="GennaioCB" value='x' id="GennaioCB"><label for="ennaio">Gennaio</label>
                    </td>
                    <td>
                        <input type="checkbox" name="FebbraioCB" value='x' id="FebbraioCB"><label for="Febbraio"> Febbraio </label>
                    </td>
                    <td>
                        <input type="checkbox" name="MarzoCB" value='x' id="MarzoCB"><label for="Marzo"> Marzo </label>
                    </td>
                    <td>
                        <input type="checkbox" name="AprileCB" value='x' id="AprileCB"><label for="Aprile"> Aprile </label>
                    </td>

                </tr>
                <tr>
                    <td>
                        <input type="checkbox" name="MaggioCB" value='x' id="MaggioCB"><label for="Maggio"> Maggio </label>

                    </td>
                    <td>
                        <input type="checkbox" name="GiugnoCB" value='x' id="GiugnoCB"><label for="Giugno"> Giugno </label>
                    </td>
                    <td>
                        <input type="checkbox" name="LuglioCB" value='x' id="LuglioCB"><label for="Luglio"> Luglio </label>
                    </td>
                    <td>
                        <input type="checkbox" name="AgostoCB" value='x' id="AgostoCB"><label for="Agosto"> Agosto </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" name="SettembreCB" value='x' id="SettembreCB"><label for="Settembre"> Settembre </label>
                    </td>
                    <td>
                        <input type="checkbox" name="OttobreCB" value='x' id="OttobreCB"><label for="Ottobre"> Ottobre </label>
                    </td>
                    <td>
                        <input type="checkbox" name="NovembreCB" value='x' id="NovembreCB"><label for="Novembre"> Novembre </label>
                    </td>
                    <td>
                        <input type="checkbox" name="DicembreCB" value='x' id="DicembreCB"><label for="Dicembre"> Dicembre </label>
                    </td>

                </tr>

                </tbody>

            </table>

        <table border="0" align="center">
            <tbody>
            <tr>
                <td width="350px" >
                    <b>Numero medio di giorni per completare la fase  </b>
                </td>
                <td align="center" valign="middle">
                  <?php    echo $this->Form->control('NumMedioGiorniComplet',
                                      array('class'=>'form-control' ,'label'=>'  ',
                                          'style'=>'width:80px' , 'min'=>'1', 'max'=>'365'));
                  ?>
                  </td>
            </tr>
            <tr valign="middle">
                <td valign="middle" >
                    <b>Numero di iterazioni annue  </b>
                </td>
                <td align="center" valign="middle">
                    <?php      echo $this->Form->control('NumIterazioniAnnue',
                            array('class'=>'form-control' ,'label'=>'  ',
                            'style'=>'width:80px' , 'min'=>'1', 'max'=>'365'));
                    ?>
                </td>
            </tr>
            <tr>
                <td >
                    <b>Totale giornate lavoro annue </b>
                </td>
                <td align="center" valign="middle">
                    <?php
                       echo $this->Form->control('TotaleGiornateLavoroAnnue',
                           array('class'=>'form-control' ,'label'=>'  ',
                           'style'=>'width:80px' , 'readonly'=> 'true', 'min'=>'1', 'max'=>'365'));
                    ?>
                </td>
            </tr>
            </tbody>
        </table>



     <div hidden="true" >
                <?php
                $IdRilevamento = $this->request->session()->read('IdRilevamento');
                echo $this->Form->control('AltreUnitaOrganizzativeCoinvolte',array('class'=>'form-control', 'label' => 'Altre unità organizzative coinvolte'));
                echo $this->Form->control('Periodicita', array('label'=>'','hidden'=>true));
                echo $this->Form->control('TipoEventoAvvio',array('class'=>'form-control', 'hidden'=>true));
                echo $this->Form->control('TipoOutput',array('class'=>'form-control','hidden'=>true));
                echo $this->Form->input('IdRilevamento',array('type'=>'text','value'=>$IdRilevamento,'label'=>'','hidden'=>true));
                echo $this->Form->input('Processo_id',array('type'=>'text','value'=>$idprocesso,'label'=>'','hidden'=>'true'));
                echo $this->Form->input('CheckIdProcesso',array('type'=>'text','value'=>$CheckIdProcesso,'label'=>'','hidden'=>'true'));
                echo $this->Form->control('CheckIdFase',array('label'=>'','hiddden'=>true));
                echo $this->Form->control('CheckIdStruttura',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Gennaio',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Febbraio',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Marzo',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Aprile',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Maggio',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Giugno',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Luglio',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Agosto',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Settembre',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Ottobre',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Novembre',array('label'=>'','hidden'=>true));
                echo $this->Form->control('Dicembre',array('label'=>'','hidden'=>true));
      //   echo $this->Form->control('dipendente._ids', ['options' => $dipendente]);
        ?>
            </div>
    </fieldset>
</br>
        <div align="center">
            <?= $this->Form->button(__('Salva'), array('class' => 'btn btn-primary"'));
             echo   ' ' . $this->Form->button(_('Annulla'), array('type'=>'reset','class' => 'btn btn-primary', 'onclick' => 'goBack()'));
             echo $this->Form->end() ?>
            <script>
                function goBack() {
                    window.history.back();
                }
            </script>
        </div>
    </p>

</div>
