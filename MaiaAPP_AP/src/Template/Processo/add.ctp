<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div>
    <script>
        $(document).ready(function(){

            <?php
           if ((strcasecmp($this->request->session()->read('User.UserRole'), 'Analista') === 0)) {
            ?>
                $('#tipoprocesso-id') . change(function (e){
                    var
                    textValue;
                    textValue = $( "#tipoprocesso-id option:selected" ) . text();
                    $('input[name=CheckIdTipoProcesso]') . val(textValue);
                });
            // And now fire change event when the DOM is ready
            $('#tipoprocesso-id').trigger('change');
            <?php } ?>

            $('#struttura-id').change(function(e){

                var textValue;
                textValue = $( "#struttura-id option:selected" ).text();
                $('input[name=CheckIdStruttura]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#struttura-id').trigger('change');
        });
    </script>
</div>

    <div class="col-lg-8">
        <p class="bs-component">
    <fieldset>
        <h1> Inserisci un nuovo processo</h1>
        <div class="well">
            <?= $this->Form->create(($processo),array('class'=>'form-horizontal'));?>
                    <?php
                    if ((strcasecmp($this->request->session()->read('User.UserRole'), 'Analista') === 0)) {
                        echo
                        $this->Form->input('TipoProcesso_Id', array(
                            'type' => 'select',
                            'label' => 'Tipo Processo',
                            'options' => $CTipoProcesso,
                            'class' => 'form-control'
                            // 'value'=> $IdTipoprocessotoSelect  // indica il valore da selezionare
                        ));
                    }else{
                        echo $this->Form->control('TipoProcesso_Id',array('hidden'=>true, 'label'=>'', 'value'=>'21'));
                    }


                    ;?>

                     <?php  echo $this->Form->control('NomeProcesso',
                                array('class'=>'form-control',
                                       'required'=>true));
                     ?>
                     <br/>
                     <?php  echo $this->Form->control('DescrizioneProcesso',array('class'=>'form-control'));?>
                       <br/>
                      <?php  echo  $this->Form->input('Struttura_Id', array(
                            'type'=>'select',
                            'label'=>'Struttura di appartenenza',
                            'options'=>$CStrutturaFiltered,
                            'class' =>'form-control',
                            'required'=>true

                            //   'value'=> $IdStrutturaSelect //$this->Form->control('IdStruttura')// 20 // indica il valore con cui è selezionata la drop
                        ));?>
                       <br/>
                       <?php  echo $this->Form->control('NomeFunzione',array('class'=>'form-control', 'label'=>'Funzione'));?>
                       <br/>
                       <?php  echo $this->Form->control('Note',array('type'=>'textarea' ,'class'=>'form-control', 'label'=>'Note sul processo'));?>
                       <br/>

                       <div hidden="true">
                       <?php echo $this->Form->control('CheckIdStruttura',array('hidden'=>true, 'label'=>''));
                            echo $this->Form->control('CheckIdTipoProcesso',array('hidden'=>true, 'label'=>'','value'=>'ND'));
                           $IdRilevamento = $this->request->session()->read('IdRilevamento');
                            echo $this->Form->control('IdRilevamento', array('value'=>$IdRilevamento,'hidden'=>true, 'label'=>''));?>
                       </div>
                   </div>
        </fieldset>
        <div align="center">
            <?= $this->Form->button(__('Salva'), array('class' => 'btn btn-primary "')); ?>
            <?=$this->Form->button(_('Annulla '), array('type'=>'reset','class' => 'btn btn-primary', 'onclick' => 'goBack()'))?>

            <?= $this->Form->end() ?>
        </div>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
    </p>
    </div>



