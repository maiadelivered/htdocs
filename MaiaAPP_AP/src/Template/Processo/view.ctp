    <?php
    /**
     * @var \App\View\AppView $this
     * @var \App\Model\Entity\Processo $processo
     */
    ?>
<br/>

    <div class="processo view large-9 medium-8 columns content">
               <nav class="navbar navbar-default" >
                   <div class="container-fluid">
                       <div class="navbar-header">
                           <a class="navbar-brand" href="#">Dettaglio del processo: <?= h($processo->NomeProcesso) ?></a>
                       </div>
                       <?php
                       $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
                       if ($StatoRilevamento == 0) {
                           ?>

                           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                               <ul class="nav navbar-nav navbar-right">
                                   <li class="nav navbar-nav"><?= $this->Html->link(__('Modifica processo'), ['controller' => 'processo', 'action' => 'edit', $processo->IdProcesso]) ?>
                                   <li><?= $this->Form->postLink(__('Elimina Processo'), ['action' => 'delete', $processo->IdProcesso],
                                           ['confirm' => __(
                                               'Si è sicuri di voler eliminare il processo {0}? 
 L\'eliminazione del processo comporterà  l\'eliminazione delle relative fasi e i carichi di lavoro. ', $processo->NomeProcesso)]) ?>
                                   </li>

                               </ul>
                           </div>
                           <?php
                       }
                       ?>
                   </div>
               </nav>
        <table class="table table-striped ">
            <tr>
                <td>
                    <h4><?= __('Descrizione Processo') ?></h4>
                </td>

                <td>

                    <?php if(((string)($processo->DescrizioneProcesso)) == "0"): ?>

                        <h5>Non è stata fornita la descrizione per il processo selezionato </h5>

                    <?php else:  ?>
                        <?= $this->Text->autoParagraph(h($processo->DescrizioneProcesso)); ?>
                    <?php endif; ?>
                </td>
            </tr>

            <tr>
                <td>
                    <h4><?= __('Tipo Processo') ?></h4>
                </td>
                <td>
                    <?= $this->Text->autoParagraph(h($processo->CheckIdTipoProcesso)); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <h4><?= __('Struttura di appartenenza') ?></h4>
                </td>
                <td>
                    <?= $this->Text->autoParagraph(h($processo->CheckIdStruttura)); ?>
                </td>
            </tr>
            </tr>
        </table>


    </div>

    <div>
        <nav class="navbar navbar-default" >
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Fasi del processo</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                            <li  class="nav navbar-nav"><?= $this->Html->link(__('Inserisci fase'), ['controller' => 'Fase', 'action' => 'Add',
                                    '?' => array('idprocesso' => $processo->IdProcesso)]) ?></li>
                        </ul>

                </div>
                </div>
        </nav>
        <?php if (($fase->count())==0) {
         ?>
            <h3 align="center" > Non sono presenti fasi sul processo selezionato. Per iserire una nuova fase utilizzare il link "Nuova Fase". </h3>
        <?php } else {
        ?>
        <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th scope="col" class="info">Nome Fase</th>
                <th scope="col" class="info">Evento di avvio</th>
                <th scope="col" class="info">Tipologia di Output</th>
                <th scope="col" class="info">Altre UO Coinvolte</th>
                <th scope="col" class="info" width="120"><?= __(' ') ?></th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($fase as $fase): ?>
                <tr>
                    <td><?= h($fase->NomeFase) ?></td>
                    <td><?= h($fase->TipoOutput) ?></td>
                    <td><?= h($fase->TipoEventoAvvio) ?></td>
                    <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
                    <td class="actions">

                        <div class="btn-group">
                            <a href="#" class="btn btn-default">Gestisci</a>
                            <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span
                                        class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><?= $this->Html->link(__('Modifica Fase'),
                                        array('controller' => 'Fase', 'action' => 'edit',
                                            '?' => array('idprocesso' => $fase->Processo_id, 'idfase' => $fase->IdFase))); ?></li>
                                <li><?= $this->Form->postLink(__('Elimina fase'), ['controller' => 'Fase', 'action' => 'delete', $fase->IdFase],
                                        ['confirm' => __(
                                            'Si è sicuri di voler eliminare la fase {0}? 
     L\'eliminazione della fase comporterà  l\'eliminazione dei relativi carichi di lavoro. ', $fase->NomeFase)]) ?>
                                </li>
                                <li class="divider"></li>
                                <li><?= $this->Html->link(__(' Aggiungi carichi'),
                                        array('controller' => 'DipendenteFase', 'action' => 'Index',
                                            '?'=> array('idprocesso'=>$fase->Processo_id ,'idfase'=>$fase->IdFase)));
                                    ?></li>
                            </ul>
                        </div>

                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>


        <?php
        if (($vdipeprorownpAllproFiltered->count()) == 0) {
        ?>
        <div class="processo view large-9 medium-8 columns content">
            <h3 align="center">Non sono state allocate risorse sul processo <?= h($processo->NomeProcesso) ?></h3>
        </div>
    </div>
    <?php } else {
        ?>

        <div class="processo view large-9 medium-8 columns content">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Risorse allocate sul processo</a>
                    </div>
                </div>
            </nav>

            <h4> Numero dipendenti totali allocati sul
                processo: <?php echo($this->Number->format($vdipeprorownpAllproFiltered->first()->NumDipTot)) ?></h4>
            <table class="table table-striped table-hover ">
                <th scope="col" class="info">Qualifica</th>
                <th scope="col" class="info">Numero</th>
                <th scope="col" class="info">Percentuale occupazione</th>
                <tr>
                    <th scope="row"><?= __('Dirigenti') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirigente) ?></td>
                    <td><?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirigente) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Categoria D') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatD) ?></td>
                    <td> <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatD) ?> </td>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Posizioni Organizzative') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumPO) ?></td>
                    <td>  <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercPO) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Alte Professionalità') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumAP) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercAP) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Altre Posizioni Organizzative') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumAltrePOAP) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercAltrePOAP) ?></td>
                </tr>

                <tr>
                    <th scope="row"><?= __('Categorie C') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatC) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatC) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Categorie B') ?></th>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatB) ?></td>
                    <td>
                        <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatB) ?></td>
                </tr>

                <th scope="row"><?= __('Categorie A') ?></th>
                <td>
                    <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatA) ?></td>
                <td>
                    <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatA) ?></td>
                </tr>
                <?php if ($vdipeprorownpAllproFiltered->first()->NumCatDirDip > 0): ?>
                    <tr>
                        <th scope="row"><?= __('Dirigenti tempo determinato') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirDip) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirDip) ?></td>

                    </tr>
                <?php endif; ?>
                <?php if ($vdipeprorownpAllproFiltered->first()->NumCatCapoRedattore > 0): ?>
                    <tr>
                        <th scope="row"><?= __('Capo Redattore') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoRedattore) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoRedattore) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($vdipeprorownpAllproFiltered->first()->NumCatSegrGiuntaReg > 0) : ?>
                    <tr>
                        <th scope="row"><?= __('Segretario di Giunta Regionale') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatSegrGiuntaReg) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatSegrGiuntaReg) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($vdipeprorownpAllproFiltered->first()->NumCatResCoordPolInte > 0) : ?>
                    <tr>
                        <th scope="row"><?= __('Responsabile Politiche interne') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatResCoordPolInte) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatResCoordPolInte) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirConTD) > 0): ?>
                    <tr>
                        <th scope="row"><?= __('Dirigenti Tempo Determinato') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatDirConTD) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatDirConTD) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoSerSenior) > 0) : ?>
                    <tr>
                        <th scope="row"><?= __('NumCatCapoSerSenior') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoSerSenior) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoSerSenior) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatRed30Mesi) > 0) : ?>
                    <tr>
                        <th scope="row"><?= __('Categoria Red30Mesi') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatRed30Mesi) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatRed30Mesi) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoGab) > 0): ?>
                    <tr>
                        <th scope="row"><?= __('Capo Gabinetto') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatCapoGab) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatCapoGab) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatSegrGenPres) > 0) : ?>
                    <tr>
                        <th scope="row"><?= __('Segeretari Generali Presidenza') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatSegrGenPres) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatSegrGenPres) ?></td>
                    </tr>
                <?php endif; ?>
                <?php if ($this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatAvvCoord) > 0) : ?>
                    <tr>
                        <th scope="row"><?= __('Avvocato Coordinatore') ?></th>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->NumCatAvvCoord) ?></td>
                        <td>
                            <?= $this->Number->format($vdipeprorownpAllproFiltered->first()->PercCatAvvCoord) ?></td>
                    </tr>
                <?php endif; ?>

            </table>

        </div>
        <?php
    }
    }
        ?>
       </div>
