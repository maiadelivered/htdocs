<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Processo[]|\Cake\Collection\CollectionInterface $processo
 */

use Cake\I18n\Time;
use Cake\Routing\Router;
?>
</div>
<nav class="navbar navbar-default" >
    <div class="container-fluid">
        <div class="navbar-header" >
            <a class="navbar-brand" href="#"><?php
            if (isset($azionetodo)) {
              if (strcasecmp( $azionetodo, 'ricerca' ) == 0 ) {
                   echo 'Elenco dei processi trovati';
                    }else{
                        echo 'Elenco dei processi';
                    };
            }else{
                echo 'Elenco dei processi';
            };?>
            </a>
        </div>

       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <ul class="nav navbar-nav navbar-right">
                 <li  class="nav navbar-nav">
                    <a href="#" id= "ImgSearch"  >
                        <?= $this->Html->image('search.png',['height'=>'40', 'width'=>'40']) ?>
                    </a>
                 </li>
                <script>
                    $( "#ImgSearch" ).click(function() {
                        if($("#Divricerca").is(':hidden')){
                            $( "#Divricerca").show( "slow" );
                        }else {
                            $( "#Divricerca").hide("slow");
                        }
                    });
                </script>
            </ul>
            <?php
            $StatoRilevamento = $this->request->session()->read('StatoRilevamento');
            if ($StatoRilevamento == 0) {
            ?>
            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav"><?php
                    echo $this->Html->image("NewProcess.png", array(
                        "alt" => "Nuovo Processo",
                        'url' => ['controller' => 'Processo', 'action' => 'add'],
                        'height'=>'40', 'width'=>'40'));
                    ?>
                </li>
            </ul>
            <?php } ?>
        </div>
    </div>
</nav>

<div class="form-group" id="Divricerca" hidden="true" >

    <?= $this->Form->create('Post') ?>

    <fieldset>
        <legend><?= __('Ricerca Processo') ?></legend>

            <table cellpadding="10" cellspacing="10">
                <tbody>
                <tr  valign="middle">
                    <td valign="middle" width="700">
                        <?php
                        echo $this->Form->control('descrizione',
                            array('type'=>'text','class'=>'form-control', 'width'=>'30',
                                'label'=> 'Inserire il nome del processo da cercare (o parte di esso):', 'placeholder'=>'Nome processo'));
                        ?>
                    </td>
                    <td valign="bottom">
                        <?php echo $this->Form->button(
                            'Cerca',
                            array('class'=>'btn btn-primary ',
                                'formaction' => Router::url(
                                    array('controller' => 'processo','action' => 'SearchProcesso')
                                )
                            )
                        );
                        ?>
                    </td>
                </tr>
               </tbody>
            </table>

        </div>
    </fieldset>


    <?= $this->Form->end() ?>
</div>
</div>
</div>

<?php if (($processo->count())==0) {
    if ($StatoRilevamento == 0) {
        echo '<h3 align="center" > Non sono presenti processi. Per inserire un processo usare il link "Nuovo processo".  </h3>';
 } else {
        echo '<h3 align="center" > Il rilevamento è chiuso non è possibile inserire processi.  </h3>';
    }
}else{
    ?>



<div class="table table-striped table-hover " cellpadding="1" cellspacing="1">
    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col" class="info"><?= $this->Paginator->sort('NomeProcesso','Nome del processo') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('CheckIdStruttura', 'Struttura di appartenenza') ?></th>
                <?php
                    if ((strcasecmp($this->request->session()->read('User.UserRole'), 'Analista') === 0)) { ?>
                <th scope="col" class="info"><?= $this->Paginator->sort('CheckIdTipoProcesso','Tipo processo') ?></th>
                    <?php  }?>
                     <th scope="col" class="info" width="150" ><?= __('') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($processo as $processo): ?>
            <tr>
                <td><?= h($processo->NomeProcesso) ?></td>

                <td><?= h($processo->CheckIdStruttura) ?></td>
        <?php
        if ((strcasecmp($this->request->session()->read('User.UserRole'), 'Analista') === 0)) { ?>

            <td><?= h($processo->CheckIdTipoProcesso) ?></td>
            <?php
        } ?>

                <td class="actions">
                    <div class="btn-group">
                        <a href="#" class="btn btn-default ">Gestisci</a>

                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu">

                                <li><?= $this->Html->link(__('Mostra dettagli'),
                                        array('action' => 'view', '?'=> array('id'=>$processo->IdProcesso))); ?></li>
                                <li> <?= $this->Html->link(__('Modifica processo'), ['action' => 'edit', $processo->IdProcesso]) ?></li>
                                <li><?= $this->Form->postLink(__('Elimina processo'), ['action' => 'delete', $processo->IdProcesso],
                                        ['confirm' => __(
                                            'Si è sicuri di voler eliminare il processo {0}? 
     L\'eliminazione del processo comporterà  l\'eliminazione delle relative fasi ed i carichi di lavoro ad esse associati. ', $processo->NomeProcesso)]) ?>
                                </li>
                                <li class="divider"></li>
                                <li><?= $this->Html->link(__('Elenco fasi'),
                                        array('controller' => 'Fase', 'action' => 'index', '?'=> array('idprocesso'=>$processo->IdProcesso)));
                                    ?></li>
                                <li><?= $this->Html->link(__('Aggiungi fase'),
                                        array('controller' => 'Fase', 'action' => 'add', '?'=> array('idprocesso'=>$processo->IdProcesso)));
                                    ?></li>
                            <li><?= $this->Html->link(__('Aggiungi carico'),
                                    array('controller' => 'DipendenteFase', 'action' => 'indexp', '?'=> array('idprocesso'=>$processo->IdProcesso, 'NomeProcesso'=>$processo->NomeProcesso)));
                                ?></li>

                        </ul>
                    </div>

                </td>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


    <div  align="center" style="width:100%">
        <ul class="pagination pagination"  >
            <?= $this->Paginator->first('<< ' . __('Primo')) ?>
            <?= $this->Paginator->prev('< ' . __('Precedente')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Successivo') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
        </ul>
        <p align="center"><?= $this->Paginator->counter(['format' => __('Pagina {{page}} di {{pages}}. Totale processi presenti nella sezione {{count}}')]) ?></p>
    </div>



</div>



<?php }
    ?>