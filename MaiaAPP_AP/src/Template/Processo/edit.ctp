    <?php
    /**
     * @var \App\View\AppView $this
     */
    ?>

    <script>
        $(document).ready(function(){
            $('#tipoprocesso-id').change(function(e){
                  var textValue;

                textValue = $( "#tipoprocesso-id option:selected" ).text();
                $('input[name=CheckIdTipoProcesso]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#tipoprocesso-id').trigger('change');

            $('#struttura-id').change(function(e){
                var textValue;
                textValue = $( "#struttura-id option:selected" ).text();
                $('input[name=CheckIdStruttura]').val(textValue);
            });

            // And now fire change event when the DOM is ready
            $('#struttura-id').trigger('change');


        });
    </script>

    <div class="col-lg-8">

        <p class="bs-component">

        <?= $this->Form->create($processo) ?>
        <fieldset>
            <h1>Modifica il processo</h1>
            <div class="well">
            <?php
            echo $this->Form->control('NomeProcesso', array( 'label'=>'Nome processo','class' =>'form-control'));
            echo '</br>';
            echo $this->Form->control('DescrizioneProcesso', array('label'=>'Descrizione Processo','class' =>'form-control'));
            echo '</br>';

           // echo $this->Form->control('TipoProcesso_Id',array('hidden'=>true, 'label'=>''));

            $IdTipoprocessotoSelect = $processo->TipoProcesso_Id;
            if ((strcasecmp($this->request->session()->read('User.UserRole'), 'Analista') === 0)) {
                echo
                $this->Form->input('TipoProcesso_Id', array(
                    'type' => 'select',
                    'label' => 'Tipo Processo',
                    'options' => $CTipoProcesso,
                    'value' => $IdTipoprocessotoSelect,  // indica il valore da selezionare
                    'class' => 'form-control'
                ));
            }

            $IdStrutturaSelect = $processo->Struttura_Id;
            echo $this->Form->input('Struttura_Id', array(
                    'type'=>'select',
                    'label'=>'Struttura di appartenenza',
                    'options'=>$CStrutturaFiltered,
                    'value'=> $IdStrutturaSelect,
                    'class' =>'form-control'//$this->Form->control('IdStruttura')// 20 // indica il valore con cui è selezionata la drop
            ));
            echo '</br>';
            echo $this->Form->control('NomeFunzione', array( 'label'=>'Funzione','class' =>'form-control'));
            echo '</br>';
            echo $this->Form->control('Note',array('type'=>'textarea' ,'class'=>'form-control', 'label'=>'Note sul processo'));
                ?>
            <div hidden = true >
           <?php
                $IdRilevamento = $this->request->session()->read('IdRilevamento');
                echo $this->Form->control('IdRilevamento', array('value'=>$IdRilevamento,'hidden'=>true, 'label'=>''));
                echo $this->Form->control('CheckIdStruttura',array('hiddden'=>true, 'label'=>''));
                echo $this->Form->control('CheckIdTipoProcesso',array('hiddden'=>true, 'label'=>''));
           ?>
            </div>
        </fieldset>
        <div align="center">
        <?= $this->Form->button(__('Salva'), array('class' => 'btn btn-primary"')) ?>
        <?=$this->Form->button(_('Elimina modifiche'), array('type'=>'reset','class' => 'btn btn-primary"'));?>
        <?=$this->Form->button(_('Annulla '), array('type'=>'reset','class' => 'btn btn-primary', 'onclick' => 'goBack()'))?>

        <?= $this->Form->end() ?>

            <script>
                function goBack() {
                    window.history.back();
                }
            </script>
        </div>
    </div>
