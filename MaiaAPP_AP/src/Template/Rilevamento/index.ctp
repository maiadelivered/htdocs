<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento[]|\Cake\Collection\CollectionInterface $rilevamento
 */
use Cake\I18n\Date;
use Cake\Routing\Router;
?>
<br/>
<div class="related">
<nav class="navbar navbar-default" height=100px  >
    <div class="container-fluid">
        <div class="navbar-header">
        <a class="navbar-brand" href="#">Elenco dei rilevamenti disponibili</a>
        <?php
        $UserRole = $this->request->session()->read('User.UserRole');
        if  (strcasecmp($UserRole, 'DirettoreGenerale') === 0){
        ?>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav">
                    <?php
                    echo ($this->Html->link(__('Aggiungi rilevamento  '),
                        array('controller' => 'Rilevamento', 'action' => 'Add'
                        )))
                    ?>
                </li>
            </ul>
        </div>
        <?php } ?>
    </div>
</nav>
</div>
<table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
    <thead>
        <tr>
                <th scope="col" class="info"><?= $this->Paginator->sort('Nome Rilevamento') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Inizio', 'Data inizio rilevamento') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Fine','Data fine rilevamento') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('Descrizione') ?></th>
                <th scope="col" class="info" ><?= $this->Paginator->sort('Stato', 'Stato (*)') ?></th>
                <th scope="col" class="info"><?= __(' ') ?></th>
            <?php
            $UserRole = $this->request->session()->read('User.UserRole');
            if  (strcasecmp($UserRole, 'DirettoreGenerale') === 0){
            ?>
                <th scope="col" class="info"><?= __(' ') ?></th>
                <th scope="col" class="info"><?= __(' ') ?></th>
            <?php } ?>

            </tr>
        </thead>
        <tbody>
            <?php

            foreach ($rilevamento as $rilevamento): ?>
            <tr>
                <td><?= h($rilevamento->Rilevamento) ?></td>
                <td><?= h($rilevamento->StartDate)// $this->Time->format($rilevamento->StartDate); //,'dd-MMMM-YYYY') ?></td>
                <td><?= h($rilevamento->EndDate)  // $this->Time->format($rilevamento->EndDate); //,'dd-MMMM-YYYY') ?></td>
                <td><?= h($rilevamento->Descrizione) ?></td>
                <td><?= h($rilevamento->CheckIdStato) ?></td>

                <td class="actions">  <?= $this->Form->postLink(__('Seleziona'),
                        ['action' => 'SetRilevamento', '?'=>
                            array('id'=> $rilevamento->IdRilevamento)],
                        array('class'=>'btn btn-primary btn-sm ')
                    ); ?>
                </td>
                <?php
                $UserRole = $this->request->session()->read('User.UserRole');
                if  (strcasecmp($UserRole, 'DirettoreGenerale') === 0){
                    ?>
                    <td class="actions">  <?= $this->Html->link(__('Modifica'),
                            ['action' => 'Edit', '?'=>
                                array('idtoedit'=> $rilevamento->IdRilevamento)],
                            array('class'=>'btn btn-warning btn-sm ')); ?>
                    </td>

                    <td class="actions">  <?= $this->Form->postLink(__('Clona'),
                            ['action' => 'ClonaRilevamento', '?'=>
                                array('id'=> $rilevamento->IdRilevamento), ],
                            array('class'=>'btn btn-warning btn-sm ','confirm' => __(
                            'Il rilevamento clonato può essere eliminato solo contattando l\'amministratore del sistema.  
    Si vuole procedere alla clonazione?'))); ?>


                    </td>

                <?php } ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<br  align="center" style="width:100%">
  <b> (*) DESCRIZIONE STATO: </b>
</br> ATTIVO: Rilevamento in corso
</br> CHIUSO: Rilevamento chiuso. Non è possibile modificare i valori inseriti.
</br> CONSOLIDATO: Rilevamento chiuso. I valori inseriti sono stati utilizzati per la relativa reportistica.
</div>

</div>
