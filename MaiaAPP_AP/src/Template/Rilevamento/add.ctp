<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rilevamento $rilevamento
 */


?>

<script>


</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<?php echo  $this->Html->script('jquery-ui') ?>


<script>

        $(function () {
            $("#StartDateP").datepicker({
                dateFormat: 'dd/mm/yy',//check change
                changeMonth: false,
                changeYear: false
            });
        });
        $(function () {
            $("#EndDateP").datepicker({
                dateFormat: 'dd/mm/yy',//check change
                changeMonth: false,
                changeYear: false
            });
        });

        $(document).ready(function() {

            $('#CIdStato').change(function (e) {
                var textValue;
                textValue = $("#CIdStato option:selected").text();
                $('input[name=CheckIdStato]').val(textValue);
                var valtext;
                valtext = $("#CIdStato option:selected").val();
                $('input[name=IdStato]').val(valtext);

            });
            $('#CIdStato').trigger('change');

            $('#StartDateP').change(function (e) {
                var textValue;
                textValue = $("#StartDateP").val();

                $('input[name=StartDate]').val(textValue);

            });
            $('#StartDateP').trigger('change');

            $('#EndDateP').change(function (e) {
                var textValue;
                textValue = $("#EndDateP").val();

                $('input[name=EndDate]').val(textValue);

            });
            $('#EndDateP').trigger('change');

        });

</script>

<div class="col-lg-8">
    <p class="bs-component">
    <h1> Inserisci un nuovo rilevamento</h1>
    <div class="well">
    <fieldset>

    <?= $this->Form->create($rilevamento) ?>

        <?php
            echo $this->Form->control('Rilevamento',
                array('class'=>'form-control',
                    'label'=>'Nome Rilevamento',
                    'required'=>true ));

            echo $this->Form->control('Descrizione',  array('class'=>'form-control'));

        ?>
    <br/>
        <p><strong> Data Inizio: <input type="text" id="StartDateP">
                Data Fine:  <input type="text" id="EndDateP"></p>
    </strong>
            <b>Stato:</b>
            <select id="CIdStato" name="CIdStato" class="form-control">
                <option value="0" selected>Attivo</option>
                <option value="2" >Chiuso</option>
                <option value="3" >In Programmazione</option>
                <option value="4" >Consolidato</option>
            </select>

    <?php
     echo $this->Form->control('Note',array('class'=>'form-control'));
    ?>

    <div  hidden = true>

        <input type="text" name="StartDate"  id="StartDate">
        <input type="text" name="EndDate"  id="EndDate">
        <input type="text" name="Creatore"  id="Creatore" value="<?php  echo $this->request->session()->read('Auth.User.name')
            ?>">
        <?php
        echo $this->Form->control('IdStato');
        echo $this->Form->control('CheckIdStato');
        ?>
    </div>

    </fieldset>

<br/>
    <div align="center">
        <?= $this->Form->button(__('Inserisci'), array('class' => 'btn btn-primary"'));?>
        <?=$this->Form->button(_('Annulla'), array('type'=>'reset','class' => 'btn btn-primary', 'onclick' => 'goBack()'))?>

        <?=$this->Form->end() ?>
    </div>

        <script>
            function goBack() {
                window.history.back();
            }
        </script>

</div>


