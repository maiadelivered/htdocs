<?php
/**
 * @var \App\View\AppView $this
 */
?>

<script>
    $(document).ready(function(){

        // And now fire change event when the DOM is ready
        $('#userrole2').change(function(e){

            var textValue;
            textValue = $( "#userrole2").val();
            $('input[id=userrole]').val(textValue);

        });
        // And now fire change event when the DOM is ready
        $('#userrole2').trigger('change');
    });
</script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul  class="btn-group">
        <li class="btn btn-info"><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $vlistuser->id_user],
                ['confirm' => __('Si desidera eliminare l\' utente  {0}?', $vlistuser->username)]
            )
        ?></li>
        <li class="btn btn-info"><?= $this->Html->link(__('Elenco degli utenti'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="vlistusers form large-9 medium-8 columns content">
    <?= $this->Form->create($vlistuser);
     $UserRole = $vlistuser->UserRole;

     ?>
    <fieldset>
        <legend><?= __('Modifica dati utente') ?></legend>
        <?php
        $sezioneCorrente = $vlistuser->idSezione;
        debug($sezioneCorrente);
            echo $this->Form->control('name', array
            ('class'=>'form-control',
            'label'=>'Nome Utente',
            'required'=>true ));
            echo $this->Form->control('email', array
            ('class'=>'form-control',
                'label'=>'Emal uente',
                'required'=>true));
        echo $this->Form->control('username', array
        ('class'=>'form-control',
            'label'=>'Username Utente',
            'required'=>true,
            'readonly'=>true ));
                    echo $this->Form->control('password', array
            ('class'=>'form-control',
                'label'=>'password Utente',
                'required'=>true ));

        ?>
        <b>Sezione di operatività</b>
        <?php
        echo
        $this->Form->input('idSezione', array(
            'type'=>'select',
            'label'=>' ',
            'options'=>$CStrutturaFiltered
            ,'value'=> $sezioneCorrente // StrutturaFiltered  // indica il valore da selezionare
        ));
        ?>
        <b>Ruolo Utente</b>
        <select id="userrole2" class="form-control">
            <option value="DirettoreGenerale" <?php if (strcasecmp( $UserRole, 'DirettoreGenerale' ) == 0 ) {
                echo ' selected ';
            } ?> >Direttore Generale</option>
            <option value="DirettoreDipartimento"<?php if (strcasecmp( $UserRole, 'DirettoreDipartimento' ) == 0 ) {
                echo ' selected ';
            } ?>  >Direttore Dipartimento</option>
            <option value="RespSezione"<?php if (strcasecmp( $UserRole, 'RespSezione' ) == 0 ) {
                echo ' selected ';
            } ?>  >Responsabile di Sezione</option>

        </select>

        <div hidden="true">
            <?php
            echo $this->Form->control('UserRole');
            echo $this->Form->control('address');
            echo $this->Form->control('idDipartimento');
            echo $this->Form->control('CheckIdDipartimento');
        //    echo $this->Form->control('idSezione');
            echo $this->Form->control('checkIdSezione');
            echo $this->Form->control('idrilevamento');
            echo $this->Form->control('birthdate', ['empty' => true]);
            echo $this->Form->control('community');
            echo $this->Form->control('gender');
            echo $this->Form->control('language');
            echo $this->Form->control('location');
            echo $this->Form->control('short_bio');
            echo $this->Form->control('surname');
            ?>



        </div>

    </fieldset>
    <div align="center">
        <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary"')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
