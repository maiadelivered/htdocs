<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vlistuser $vlistuser
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul  class="btn-group">
        <li class="btn btn-info"><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $vlistuser->id_user],
                ['confirm' => __('Si desidera eliminare l\' utente  {0}?', $vlistuser->username)]
            )
            ?></li>
        <li class="btn btn-info"><?= $this->Html->link(__('Elenco degli utenti'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="vlistusers view large-9 medium-8 columns content">
    <h3><?= h($vlistuser->name) ?></h3>
    <table  class="table table-striped" class="vertical-table" cellpadding="0" cellspacing="0">
    <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($vlistuser->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($vlistuser->email) ?></td>
        </tr>

        <tr>
            <th scope="row"><?= __('CheckIdDipartimento') ?></th>
            <td><?= h($vlistuser->CheckIdDipartimento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CheckIdSezione') ?></th>
            <td><?= h($vlistuser->checkIdSezione) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($vlistuser->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('UserRole') ?></th>
            <td><?= h($vlistuser->UserRole) ?></td>
        </tr>

    </table>
</div>
