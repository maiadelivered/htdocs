<?php
/**
 * @var \App\View\AppView $this
 */
?>
<script>
    $(document).ready(function(){

        // And now fire change event when the DOM is ready
        $('#userrole2').change(function(e){

            var textValue;
            textValue = $( "#userrole2").val();
            $('input[id=userrole]').val(textValue);

        });
        // And now fire change event when the DOM is ready
        $('#userrole2').trigger('change');
    });
</script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">

    </ul>
</nav>
<div class="vlistusers form large-9 medium-8 columns content">
    <?= $this->Form->create($vlistuser) ?>
    <fieldset>
        <legend><?= __('Inserisci un nuovo utente') ?></legend>
        <?php
            echo $this->Form->control('name',  array
            ('class'=>'form-control',
                'label'=>'Nome Utente',
                'required'=>true ));
            echo $this->Form->control('email', array
        ('class'=>'form-control',
            'label'=>'Email Utente',
            'required'=>true ));
                  echo $this->Form->control('username', array
                  ('class'=>'form-control',
                      'label'=>'Username Utente',
                      'required'=>true ));

            echo $this->Form->control('password', array
            ('class'=>'form-control',
                'label'=>'Password',
                'required'=>true ));
?>
        <b>Sezione di operatività</b>
        <?php
        echo
        $this->Form->input('idSezione', array(
            'type'=>'select',
            'label'=>' ',
            'options'=>$CStrutturaFiltered,
            'value'=>'1'
           // 'value'=> $sezioneCorrente // StrutturaFiltered  // indica il valore da selezionare
        ));
        ?>

        <b>Ruolo Utente</b>
        <select id="userrole2" class="form-control">
            <option value="DirettoreGenerale"  >Direttore Generale</option>
            <option value="DirettoreDipartimento" >Direttore Dipartimento</option>
            <option value="RespSezione" selected  >Responsabile di Sezione</option>

        </select>

        <div hidden="true">
            <?php
            echo $this->Form->control('UserRole');
            echo $this->Form->control('address',[value=>'']);
            echo $this->Form->control('idDipartimento',[value=>'1']);
            echo $this->Form->control('CheckIdDipartimento',[value=>'']);
            //    echo $this->Form->control('idSezione');
            echo $this->Form->control('checkIdSezione',[value=>'']);
            echo $this->Form->control('idrilevamento',[value=>'']);
            echo $this->Form->control('birthdate', ['empty' => true]);
            echo $this->Form->control('community',[value=>'']);
            echo $this->Form->control('gender',[value=>'']);
            echo $this->Form->control('language',[value=>'']);
            echo $this->Form->control('location',[value=>'']);
            echo $this->Form->control('short_bio',[value=>'']);
            echo $this->Form->control('surname',[value=>'']);
            ?>

        </div>

    </fieldset>
    <div align="center">
        <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary"')) ?>
    <?= $this->Form->end() ?>
    </div>

</div>
