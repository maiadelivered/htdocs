<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vlistuser[]|\Cake\Collection\CollectionInterface $vlistusers
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">

</nav>
<div class="vlistusers index large-9 medium-8 columns content">
    <h3><?= __('Utenti') ?></h3>
    <table class="table table-striped table-hover " cellpadding="0" cellspacing="0">

    <thead>
        <tr>
            <th class="info" scope="col"><?= $this->Paginator->sort('name', 'Utente') ?></th>
            <th class="info"  scope="col"><?= $this->Paginator->sort('email') ?></th>
            <th class="info" scope="col"><?= $this->Paginator->sort('CheckIdDipartimento', 'Dipartimento') ?></th>
            <th class="info" scope="col"><?= $this->Paginator->sort('checkIdSezione', 'Sezione') ?></th>
            <th class="info" scope="col"><?= $this->Paginator->sort('username', 'Ueername') ?></th>
            <th  class="info" scope="col"><?= $this->Paginator->sort('UserRole', 'Ruolo') ?></th>
            <th  class="info" scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>

        </thead>
        <tbody>
            <?php foreach ($vlistusers as $vlistuser): ?>
            <tr>
                <td><?= h($vlistuser->name) ?></td>
                <td><?= h($vlistuser->email) ?></td>
                <td><?= h($vlistuser->CheckIdDipartimento) ?></td>
                <td><?= h($vlistuser->checkIdSezione) ?></td>
                <td><?= h($vlistuser->username) ?></td>
                <td><?= h($vlistuser->UserRole) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $vlistuser->id_user]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $vlistuser->id_user]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $vlistuser->id_user], ['confirm' => __('Si è sicuri di voler eliminare l\'utente  {0}?', $vlistuser->username)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
