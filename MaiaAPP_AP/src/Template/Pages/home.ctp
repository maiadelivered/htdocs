<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

// $this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('bootstrap.min') ?>
    <?php  echo  $this->Html->script('jquery-3.1.0') ?>
    <?php echo  $this->Html->script('bootstrap.min') ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body class="home">

<header class="row">

    <div class="col-lg-7">
        <p class="bs-component">
    <h1> <hr style="width: 100%; height: 2px;">
        <table style="text-align: left; width: 650px; height: 234px;" border="0"
               cellpadding="0" cellspacing="2">
            <tbody>
            <tr align="center" valign="center">
                <td style="vertical-align: center;" width="300px">
                    <br/>
                    <?= $this->Html->image('processo.jpg',['height'=>'120', 'width'=>'230', 'url' => ['controller' => 'Processo']]) ?>
                    <p class="lead">PROCESSI</p>
                    <br>
                </td>
                <td style="vertical-align: center;" width="300px">
                    <?= $this->Html->image('carichi.png',['height'=>'100', 'width'=>'100','url' => ['controller' => 'dipendente', 'action'=>'index']]) ?>
                    <p class="lead">CARICHI DI LAVORO</p>

                </td>
            </tr>
            <tr align="center" valign="center" width="300px">
                <td style="vertical-align: center;">  <?= $this->Html->image('report.png',['height'=>'100', 'width'=>'100','url'=>'http://localhost:8080/SpagoBI']) ?>
                    <p class="lead">SISTEMA DI REPORTISTICA</p>
                </td>
                <td style="vertical-align: center;" width="300px">  <?= $this->Html->image('button-160595_960_720.png',['height'=>'100', 'width'=>'100','url' => ['controller' => 'Users','action'=>'logout']]) ?><br>
                </td>
            </tr>
            </tbody>
        </table></h1>
    </p>
    </div>


    <div class="col-lg-4">

        <p class="bs-component">
            <?php if($loggedIn): ?>
        <h1></h1>
            <?php else : ?>

            <?= $this->requestAction('/Users/login') ?>
            <?php endif; ?>

    </div>
        </p>
    </div>

</header>
</body>


