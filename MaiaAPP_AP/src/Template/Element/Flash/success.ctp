<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-dismissible alert-success" >
    <h1 align="center">
        <div class="lead" onclick="this.classList.add('hidden')"><?= $message ?></div>
    </h1>
</div>

