<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dipendente $dipendente
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dipendente'), ['action' => 'edit', $dipendente->IdDipendente]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dipendente'), ['action' => 'delete', $dipendente->IdDipendente], ['confirm' => __('Are you sure you want to delete # {0}?', $dipendente->IdDipendente)]) ?> </li>
        <li><?= $this->Html->link(__('List Dipendente'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dipendente'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fase'), ['controller' => 'Fase', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fase'), ['controller' => 'Fase', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fase Backupdati'), ['controller' => 'FaseBackupdati', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fase Backupdati'), ['controller' => 'FaseBackupdati', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Struttura'), ['controller' => 'Struttura', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Struttura'), ['controller' => 'Struttura', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dipendente view large-9 medium-8 columns content">
    <h3><?= h($dipendente->IdDipendente) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dipendente') ?></th>
            <td><?= h($dipendente->Dipendente) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('COGNOME') ?></th>
            <td><?= h($dipendente->COGNOME) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NOME') ?></th>
            <td><?= h($dipendente->NOME) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('DESC QUALIFICA') ?></th>
            <td><?= h($dipendente->DESC_QUALIFICA) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PO AP') ?></th>
            <td><?= h($dipendente->PO_AP) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PO') ?></th>
            <td><?= h($dipendente->PO) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('AP') ?></th>
            <td><?= h($dipendente->AP) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('AltrePOAP') ?></th>
            <td><?= h($dipendente->AltrePOAP) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('DATA NAS') ?></th>
            <td><?= h($dipendente->DATA_NAS) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('DESC TITOLO STUDIO') ?></th>
            <td><?= h($dipendente->DESC_TITOLO_STUDIO) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('DIP') ?></th>
            <td><?= h($dipendente->DIP) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('SEZIONE') ?></th>
            <td><?= h($dipendente->SEZIONE) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('SERVIZIO') ?></th>
            <td><?= h($dipendente->SERVIZIO) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('IdDipendente') ?></th>
            <td><?= $this->Number->format($dipendente->IdDipendente) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categoria Id') ?></th>
            <td><?= $this->Number->format($dipendente->Categoria_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dipartimento Id') ?></th>
            <td><?= $this->Number->format($dipendente->Dipartimento_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sezione Id') ?></th>
            <td><?= $this->Number->format($dipendente->Sezione_id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Fase') ?></h4>
        <?php if (!empty($dipendente->fase)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('IdFase') ?></th>
                <th scope="col"><?= __('CheckIdFase') ?></th>
                <th scope="col"><?= __('Struttura Id') ?></th>
                <th scope="col"><?= __('CheckIdStruttura') ?></th>
                <th scope="col"><?= __('NomeFase') ?></th>
                <th scope="col"><?= __('RiferimentiNormativi') ?></th>
                <th scope="col"><?= __('TipoEventoAvvio') ?></th>
                <th scope="col"><?= __('TipoOutput') ?></th>
                <th scope="col"><?= __('AltreUnitaOrganizzativeCoinvolte') ?></th>
                <th scope="col"><?= __('Periodicita') ?></th>
                <th scope="col"><?= __('Gennaio') ?></th>
                <th scope="col"><?= __('Febbraio') ?></th>
                <th scope="col"><?= __('Marzo') ?></th>
                <th scope="col"><?= __('Aprile') ?></th>
                <th scope="col"><?= __('Maggio') ?></th>
                <th scope="col"><?= __('Giugno') ?></th>
                <th scope="col"><?= __('Luglio') ?></th>
                <th scope="col"><?= __('Agosto') ?></th>
                <th scope="col"><?= __('Settembre') ?></th>
                <th scope="col"><?= __('Ottobre') ?></th>
                <th scope="col"><?= __('Novembre') ?></th>
                <th scope="col"><?= __('Dicembre') ?></th>
                <th scope="col"><?= __('NumMedioGiorniComplet') ?></th>
                <th scope="col"><?= __('NumIterazioniAnnue') ?></th>
                <th scope="col"><?= __('TotaleGiornateLavoroAnnue') ?></th>
                <th scope="col"><?= __('Processo Id') ?></th>
                <th scope="col"><?= __('CheckIdProcesso') ?></th>
                <th scope="col"><?= __('IdRilevamento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dipendente->fase as $fase): ?>
            <tr>
                <td><?= h($fase->IdFase) ?></td>
                <td><?= h($fase->CheckIdFase) ?></td>
                <td><?= h($fase->Struttura_id) ?></td>
                <td><?= h($fase->CheckIdStruttura) ?></td>
                <td><?= h($fase->NomeFase) ?></td>
                <td><?= h($fase->RiferimentiNormativi) ?></td>
                <td><?= h($fase->TipoEventoAvvio) ?></td>
                <td><?= h($fase->TipoOutput) ?></td>
                <td><?= h($fase->AltreUnitaOrganizzativeCoinvolte) ?></td>
                <td><?= h($fase->Periodicita) ?></td>
                <td><?= h($fase->Gennaio) ?></td>
                <td><?= h($fase->Febbraio) ?></td>
                <td><?= h($fase->Marzo) ?></td>
                <td><?= h($fase->Aprile) ?></td>
                <td><?= h($fase->Maggio) ?></td>
                <td><?= h($fase->Giugno) ?></td>
                <td><?= h($fase->Luglio) ?></td>
                <td><?= h($fase->Agosto) ?></td>
                <td><?= h($fase->Settembre) ?></td>
                <td><?= h($fase->Ottobre) ?></td>
                <td><?= h($fase->Novembre) ?></td>
                <td><?= h($fase->Dicembre) ?></td>
                <td><?= h($fase->NumMedioGiorniComplet) ?></td>
                <td><?= h($fase->NumIterazioniAnnue) ?></td>
                <td><?= h($fase->TotaleGiornateLavoroAnnue) ?></td>
                <td><?= h($fase->Processo_id) ?></td>
                <td><?= h($fase->CheckIdProcesso) ?></td>
                <td><?= h($fase->IdRilevamento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Fase', 'action' => 'view', $fase->IdFase]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Fase', 'action' => 'edit', $fase->IdFase]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Fase', 'action' => 'delete', $fase->IdFase], ['confirm' => __('Are you sure you want to delete # {0}?', $fase->IdFase)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Fase Backupdati') ?></h4>
        <?php if (!empty($dipendente->fase_backupdati)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('IdFase') ?></th>
                <th scope="col"><?= __('CheckIdFase') ?></th>
                <th scope="col"><?= __('Struttura Id') ?></th>
                <th scope="col"><?= __('CheckIdStruttura') ?></th>
                <th scope="col"><?= __('NomeFase') ?></th>
                <th scope="col"><?= __('RiferimentiNormativi') ?></th>
                <th scope="col"><?= __('TipoEventoAvvio') ?></th>
                <th scope="col"><?= __('TipoOutput') ?></th>
                <th scope="col"><?= __('AltreUnitaOrganizzativeCoinvolte') ?></th>
                <th scope="col"><?= __('Periodicita') ?></th>
                <th scope="col"><?= __('Gennaio') ?></th>
                <th scope="col"><?= __('Febbraio') ?></th>
                <th scope="col"><?= __('Marzo') ?></th>
                <th scope="col"><?= __('Aprile') ?></th>
                <th scope="col"><?= __('Maggio') ?></th>
                <th scope="col"><?= __('Giugno') ?></th>
                <th scope="col"><?= __('Luglio') ?></th>
                <th scope="col"><?= __('Agosto') ?></th>
                <th scope="col"><?= __('Settembre') ?></th>
                <th scope="col"><?= __('Ottobre') ?></th>
                <th scope="col"><?= __('Novembre') ?></th>
                <th scope="col"><?= __('Dicembre') ?></th>
                <th scope="col"><?= __('NumMedioGiorniComplet') ?></th>
                <th scope="col"><?= __('NumIterazioniAnnue') ?></th>
                <th scope="col"><?= __('TotaleGiornateLavoroAnnue') ?></th>
                <th scope="col"><?= __('Processo Id') ?></th>
                <th scope="col"><?= __('CheckIdProcesso') ?></th>
                <th scope="col"><?= __('IdRilevamento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dipendente->fase_backupdati as $faseBackupdati): ?>
            <tr>
                <td><?= h($faseBackupdati->IdFase) ?></td>
                <td><?= h($faseBackupdati->CheckIdFase) ?></td>
                <td><?= h($faseBackupdati->Struttura_id) ?></td>
                <td><?= h($faseBackupdati->CheckIdStruttura) ?></td>
                <td><?= h($faseBackupdati->NomeFase) ?></td>
                <td><?= h($faseBackupdati->RiferimentiNormativi) ?></td>
                <td><?= h($faseBackupdati->TipoEventoAvvio) ?></td>
                <td><?= h($faseBackupdati->TipoOutput) ?></td>
                <td><?= h($faseBackupdati->AltreUnitaOrganizzativeCoinvolte) ?></td>
                <td><?= h($faseBackupdati->Periodicita) ?></td>
                <td><?= h($faseBackupdati->Gennaio) ?></td>
                <td><?= h($faseBackupdati->Febbraio) ?></td>
                <td><?= h($faseBackupdati->Marzo) ?></td>
                <td><?= h($faseBackupdati->Aprile) ?></td>
                <td><?= h($faseBackupdati->Maggio) ?></td>
                <td><?= h($faseBackupdati->Giugno) ?></td>
                <td><?= h($faseBackupdati->Luglio) ?></td>
                <td><?= h($faseBackupdati->Agosto) ?></td>
                <td><?= h($faseBackupdati->Settembre) ?></td>
                <td><?= h($faseBackupdati->Ottobre) ?></td>
                <td><?= h($faseBackupdati->Novembre) ?></td>
                <td><?= h($faseBackupdati->Dicembre) ?></td>
                <td><?= h($faseBackupdati->NumMedioGiorniComplet) ?></td>
                <td><?= h($faseBackupdati->NumIterazioniAnnue) ?></td>
                <td><?= h($faseBackupdati->TotaleGiornateLavoroAnnue) ?></td>
                <td><?= h($faseBackupdati->Processo_id) ?></td>
                <td><?= h($faseBackupdati->CheckIdProcesso) ?></td>
                <td><?= h($faseBackupdati->IdRilevamento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'FaseBackupdati', 'action' => 'view', $faseBackupdati->IdFase]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'FaseBackupdati', 'action' => 'edit', $faseBackupdati->IdFase]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'FaseBackupdati', 'action' => 'delete', $faseBackupdati->IdFase], ['confirm' => __('Are you sure you want to delete # {0}?', $faseBackupdati->IdFase)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Struttura') ?></h4>
        <?php if (!empty($dipendente->struttura)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('IdStruttura') ?></th>
                <th scope="col"><?= __('Dipartimento Id') ?></th>
                <th scope="col"><?= __('Sezione Id') ?></th>
                <th scope="col"><?= __('CheckIdSezione') ?></th>
                <th scope="col"><?= __('CheckIdDipartimento') ?></th>
                <th scope="col"><?= __('NomeStruttura') ?></th>
                <th scope="col"><?= __('NomeDirigente') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('IdDirigente') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dipendente->struttura as $struttura): ?>
            <tr>
                <td><?= h($struttura->IdStruttura) ?></td>
                <td><?= h($struttura->Dipartimento_Id) ?></td>
                <td><?= h($struttura->Sezione_Id) ?></td>
                <td><?= h($struttura->CheckIdSezione) ?></td>
                <td><?= h($struttura->CheckIdDipartimento) ?></td>
                <td><?= h($struttura->NomeStruttura) ?></td>
                <td><?= h($struttura->NomeDirigente) ?></td>
                <td><?= h($struttura->Email) ?></td>
                <td><?= h($struttura->IdDirigente) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Struttura', 'action' => 'view', $struttura->IdStruttura]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Struttura', 'action' => 'edit', $struttura->IdStruttura]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Struttura', 'action' => 'delete', $struttura->IdStruttura], ['confirm' => __('Are you sure you want to delete # {0}?', $struttura->IdStruttura)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
