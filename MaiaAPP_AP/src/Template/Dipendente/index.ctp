<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dipendente[]|\Cake\Collection\CollectionInterface $dipendente
 */
use Cake\I18n\Time;
use Cake\Routing\Router;
?>

</div>
<nav class="navbar navbar-default" >
    <div class="container-fluid">
        <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <?php
                      echo 'Dipendenti della sezione: '.
                        $this->request->session()->read('Sezione.nomeSezione') . ' '
                    ?>
                </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li  class="nav navbar-nav">
                    <a href="#" id= "ImgSearch"  >
                        <?= $this->Html->image('search.png',['height'=>'40', 'width'=>'40']) ?>
                    </a>
                </li>
                <script>
                    $( "#ImgSearch" ).click(function() {
                        if($("#Divricerca").is(':hidden')){
                            $( "#Divricerca").show( "slow" );
                        }else {
                            $( "#Divricerca").hide("slow");
                        }
                    });
                </script>
            </ul>
        </div>
    </div>
</nav>
<div class="form-group" id="Divricerca" hidden="true" >

    <?= $this->Form->create('Post') ?>

    <fieldset>
        <legend><?= __('Ricerca Dipendete') ?></legend>

        <table cellpadding="10" cellspacing="10">
            <tbody>
            <tr  valign="middle">
                <td valign="middle" width="700">
                    <?php
                    echo $this->Form->control('DipendenteToSearch',
                        array('type'=>'text','class'=>'form-control', 'width'=>'30',
                            'label'=> 'Inserire il cognome del dipendente da ricercare (o parte di esso):', 'placeholder'=>'Cognome dipendente'));
                    ?>
                </td>
                <td valign="bottom">
                    <?php echo $this->Form->button(
                        'Cerca',
                        array('class'=>'btn btn-primary ',
                            'formaction' => Router::url(
                                array('controller' => 'dipendente','action' => 'SearchDipendente')
                            )
                        )
                    );
                    ?>
                </td>
            </tr>
            </tbody>
        </table>

</div>
</fieldset>


<?= $this->Form->end() ?>
</div>
</div>
</div>

<?php
 if ((($dipendente->count())==0) AND (strcasecmp( $azionetodo, 'ricerca' ) == 0 )){

   echo '<h3 align="center" > Non sono stati trovati dipendenti con i criteri specificati.  </h3>';
    }

?>


<table class="table table-striped table-hover " cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col" class="info"><?= $this->Paginator->sort('Dipendente','Nominativo') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('DESC_QUALIFICA', 'Qualifica professionale') ?></th>
                <th scope="col" class="info"><?= $this->Paginator->sort('PO_AP', 'Posizione Organizzativa') ?></th>
                <th scope="col" class="info" ><?= $this->Paginator->sort('SERVIZIO','Struttura di appartenenza') ?></th>
                <th scope="col" class="info"><?= __(' ') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dipendente as $dipendente): ?>
            <tr>
                <td><?= h($dipendente->Dipendente) ?></td>
                <td><?= h($dipendente->DESC_QUALIFICA) ?></td>
                <td><?= h($dipendente->PO_AP) ?></td>
                <td><?= h($dipendente->SERVIZIO) ?></td>


                <td class="actions">  <?= $this->Form->postLink(__('Visualizza carico'),
                        ['controller'=>'dipendentefase',
                                'action' => 'indexd', '?'=>
                            array('IdDipendente'=>$dipendente->IdDipendente
                            )],
                        array('class'=>'btn btn-default')
                    ); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<div  align="center" style="width:100%">
    <ul class="pagination pagination"  >
        <?= $this->Paginator->first('<< ' . __('Primo')) ?>
        <?= $this->Paginator->prev('< ' . __('Precedente')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('Successivo') . ' >') ?>
        <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
    </ul>
    <p align="center"><?= $this->Paginator->counter(['format' => __('Pagina {{page}} di {{pages}}. Dipendenti totali della sezione {{count}}')]) ?></p>
</div>
</div>
