<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dipendente->IdDipendente],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dipendente->IdDipendente)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dipendente'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Fase'), ['controller' => 'Fase', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fase'), ['controller' => 'Fase', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fase Backupdati'), ['controller' => 'FaseBackupdati', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fase Backupdati'), ['controller' => 'FaseBackupdati', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Struttura'), ['controller' => 'Struttura', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Struttura'), ['controller' => 'Struttura', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dipendente form large-9 medium-8 columns content">
    <?= $this->Form->create($dipendente) ?>
    <fieldset>
        <legend><?= __('Edit Dipendente') ?></legend>
        <?php
            echo $this->Form->control('Categoria_id');
            echo $this->Form->control('Dipendente');
            echo $this->Form->control('COGNOME');
            echo $this->Form->control('NOME');
            echo $this->Form->control('DESC_QUALIFICA');
            echo $this->Form->control('PO_AP');
            echo $this->Form->control('PO');
            echo $this->Form->control('AP');
            echo $this->Form->control('AltrePOAP');
            echo $this->Form->control('DATA_NAS');
            echo $this->Form->control('DESC_TITOLO_STUDIO');
            echo $this->Form->control('DIP');
            echo $this->Form->control('SEZIONE');
            echo $this->Form->control('SERVIZIO');
            echo $this->Form->control('Dipartimento_id');
            echo $this->Form->control('Sezione_id');
            echo $this->Form->control('fase._ids', ['options' => $fase]);
            echo $this->Form->control('fase_backupdati._ids', ['options' => $faseBackupdati]);
            echo $this->Form->control('struttura._ids', ['options' => $struttura]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
