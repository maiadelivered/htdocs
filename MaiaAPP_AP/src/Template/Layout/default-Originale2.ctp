<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Routing\Router;
use Cake\View\Helper;

$cakeDescription = 'MAIA - Modello Ambidestro per l’Innovazione della macchina Amministrativa regionale';
?>

<!DOCTYPE html>

<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php // echo  $this->Html->css('base.css') ?>
    <?php // echo $this->Html->css('cake.css') ?>


    <?php echo  $this->Html->css('bootstrap.min') ?>
    <?php //   echo  $this->Html->css('bootstrap') ?>

    <?php  echo  $this->Html->script('jquery-3.1.0') ?>
    <?php echo  $this->Html->script('bootstrap.min') ?>

    <?php //echo  $this->Html->script('jquery-1.12.4') ?>



    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>
<body>

<nav class="navbar navbar-inverse" >

                <table class="table table-striped " cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>

                    <th >
                            <?= $this->Html->image('Regione.jpg',['height'=>'90', 'width'=>'105']) ?>
                        </th>
                        <th style="vertical-align: top;">
                      <h3 style="text-align: center;">
                                    <font color="white">MAIA - Modello Ambidestro per </br>
                                        l’Innovazione della macchina Amministrativa regionale
                                </font>
                                </h3>
                        </th>
                        <th >
                            <?= $this->Html->image('ipres.jpg',['height'=>'90', 'width'=>'105']) ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

    <?php
    if($loggedIn) { ?>
        <div align="right">
            <?php  if  (!(strcasecmp($this->request->session()->read('Rilevamento'), '') === 0))
               { ?>
                   <span class="badge badge-success" title="
                   <?php  echo($this->request->session()->read('Dipartimento.nomeDipartimento')). "(" .
                       $this->request->session()->read('User.idDipartimento') . ")  &#013;".

                       ($this->request->session()->read('Sezione.nomeSezione')). "(" .
                       ($this->request->session()->read('User.idSezione')) . ")"

                   ?>
                   ">
                       <?php  echo "User: " . ($this->request->session()->read('Auth.User.name')) ?>

                    </span>
               <span class="badge badge-danger" >
                       <?php
                            echo  "Rilevamento corrente: " . ($this->request->session()->read('Rilevamento'));
                             echo ($this->Html->link(' Cambia', ['controller' => 'rilevamento', 'action'=>'index']));?>
               </span>

        </div>


        <spam class="nav navbar-nav" mr-auto >
                            <?php echo($this->Html->link('Home',
                                ['controller' => 'Pages', 'action' => 'display'], ['class'=>'btn btn-info dropdown-toggle ']));?>



                        <div class="btn-group" >
                        <a href="#" class="btn btn-info dropdown-toggle ">Processi / Fasi</a>
                        <a href="#" class="btn btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu btn-primary">
                            <li>
                                <?php echo($this->Html->link(
                                    'Gestione Processi',
                                    ['controller' => 'Processo', 'action' => 'index', '_full' => true])); ?>
                            </li>
                            <li >
                                <?php echo ($this->Html->link('Gestione Fasi', ['controller' => 'fase', 'action'=>'index']));?>
                            </li>
                        </ul>
                        </div>

                        <div class="btn-group" >
                        <a href="#" class="btn btn-info dropdown-toggle ">Carichi di lavoro</a>
                        <a href="#" class="btn btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu btn-primary">
                            <li>
                                <?php echo ($this->Html->link('Gestione carichi di lavoro', ['controller' => 'dipendente', 'action'=>'index']));?>
                            </li>
                            <li  >
                                <?php echo ($this->Html->link('Riepilogo carichi di lavoro', ['controller' => 'dipendentefase', 'action'=>'riepilogoimpegni']));?>
                            </li>
                        </ul>

                        </div>

                        <div class="btn-group" >
                            <a href="#" class="btn btn-info dropdown-toggle ">Profilo</a>
                            <a href="#" class="btn btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                            <ul class="dropdown-menu btn-primary">
                                <?php
                                if  ((strcasecmp($this->request->session()->read('User.UserRole'), 'DirettoreDipartimento') === 0) or
                                    (strcasecmp($this->request->session()->read('User.UserRole'), 'DirettoreGenerale') === 0) or
                                    (strcasecmp($this->request->session()->read('User.UserRole'), 'Analista') === 0))
                                {
                                    ?>
                                    <li>
                                        <?php echo ($this->Html->link('Cambia Dipartimento', ['controller' => 'Users', 'action'=>'Changerole']));?>
                                    </li>
                                <?php } ?>
                                <li>
                                    <?php echo ($this->Html->link('Logout', ['controller' => 'Users', 'action'=>'logout']));?>
                                </li>


                            </ul>
                        </div>
            </spam>


<?php }
} ?>
</nav>

<div class="col-lg-8 col-md-7 col-sm-6">
    <div >

        <ul class="center-block">
            <?php if(!($loggedIn)): ?>

                <h1>Per iniziare effettuare il login</h1>

                       <?php endif; ?>
        </ul>
    </div>


</div>

 <?php echo $this->Flash->render();?>


<div class="container clearfix">

    <?=
    $this->fetch('content') ?>

</div>
    <footer>
    </footer>
</body>
</html>
