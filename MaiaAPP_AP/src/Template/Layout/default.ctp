<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Routing\Router;
use Cake\View\Helper;

$cakeDescription = 'MAIA - Modello Ambidestro per l’Innovazione della macchina Amministrativa regionale';
?>

<!DOCTYPE html>

<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php // echo  $this->Html->css('base.css') ?>
    <?php // echo $this->Html->css('cake.css') ?>


    <?php echo  $this->Html->css('bootstrap.min') ?>
    <?php //   echo  $this->Html->css('bootstrap') ?>

    <?php  echo  $this->Html->script('jquery-3.1.0') ?>
    <?php echo  $this->Html->script('bootstrap.min') ?>

    <?php //echo  $this->Html->script('jquery-1.12.4') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>
<body>

<nav class="navbar navbar-inverse" >

                <table class="table table-striped " cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>

                    <th >
                            <?= $this->Html->image('Regione.jpg',['height'=>'90', 'width'=>'105']) ?>
                        </th>
                        <th style="vertical-align: top;">
                      <h3 style="text-align: center;color: ivory">
                          MAIA - Modello Ambidestro per </br>
                                        l’Innovazione della macchina Amministrativa regionale

                                </h3>
                        </th>
                        <th>
                            <?= $this->Html->image('ipres.jpg',['height'=>'90', 'width'=>'105']) ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

    <?php
    if($loggedIn) { ?>
        <div align="right" style="height: 40px" >
            <?php  if  (!(strcasecmp($this->request->session()->read('Rilevamento'), '') === 0))
               { ?>
                   <span class="badge badge-danger" style="text-align:right;  background-color:#dddddd " >

                       <?php  echo "User: " . ($this->request->session()->read('Auth.User.name')) ?>
                       <?php

                       echo ($this->Html->link('Logout', ['controller' => 'Users', 'action'=>'logout'],
                               array('class'=>'badge badge-pill badge-danger', 'style'=>'height:15px; width: 70px; valign:top; background-color:#ce8483'))).  '<br/>';
                       echo  "Rilevamento corrente: " . ($this->request->session()->read('Rilevamento'));
                       echo ($this->Html->link(' Cambia', ['controller' => 'rilevamento', 'action'=>'index'],
                               array('class'=>'badge badge-pill badge-danger', 'style'=>'height:15px; width: 70px; valign:top; background-color:#b2dba1'))) . '<br/>' ;

                       ?>
                </span>
        </div>


        <span class="nav navbar-nav" mr-auto >
                            <?php echo($this->Html->link('Home',
                                ['controller' => 'Pages', 'action' => 'display'], ['class'=>'btn btn-info dropdown-toggle ']));?>
               <div class="btn-group" >
                    <?php echo($this->Html->link(
                        'Processi / Fasi',
                        ['controller' => 'Processo', 'action' => 'index', '_full' => true],
                        array('class'=>'btn btn-info dropdown-toggle'))); ?>
                        <a href="#" class="btn btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu btn-primary">
                            <li>
                                <?php echo($this->Html->link(
                                    'Gestione Processi',
                                    ['controller' => 'Processo', 'action' => 'index', '_full' => true])); ?>
                            </li>
                            <li >
                                <?php echo ($this->Html->link('Gestione Fasi', ['controller' => 'fase', 'action'=>'index']));?>
                            </li>
                        </ul>
                        </div>

                        <div class="btn-group" >
                        <?php echo ($this->Html->link('Carichi di lavoro',
                                    ['controller' => 'dipendentefase', 'action'=>'riepilogoimpegni'],
                            array('class'=>'btn btn-info dropdown-toggle')));?>

                            <a href="#" class="btn btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu btn-primary">
                            <li>
                                <?php echo ($this->Html->link('Carichi di lavoro dei dipendenti', ['controller' => 'dipendente', 'action'=>'index']));?>
                            </li>
                            <li  >
                                <?php echo ($this->Html->link('Riepilogo carichi di lavoro', ['controller' => 'dipendentefase', 'action'=>'riepilogoimpegni']));?>
                            </li>
                        </ul>

                        </div>

        <?php
        if ((strcasecmp($this->request->session()->read('User.UserRole'), 'administrator') === 0)){ ?>
                 <div class="btn-group" >
                        <?php echo ($this->Html->link('Gestione Utenti',
                            ['controller' => 'vlistusers', 'action'=>'index'],
                            array('class'=>'btn btn-info dropdown-toggle')));?>

                <a href="#" class="btn btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                        <ul class="dropdown-menu btn-primary">
                            <li>
                                <?php echo ($this->Html->link('Lista Utenti', ['controller' => 'vlistusers', 'action'=>'index']));?>
                            </li>
                            <li>
                                <?php echo ($this->Html->link('Inserisci Utente', ['controller' => 'vlistusers', 'action'=>'add']));?>
                            </li>
                        </ul>

                        </div>

            <?php } ?>

            </span>
                   <div align="right">
                       <span class="badge badge-danger" style="text-align:right"  >
                       <?php
                       echo($this->request->session()->read('Dipartimento.nomeDipartimento')). '<br/>' .
                           ($this->request->session()->read('Sezione.nomeSezione'));
                       if  ((strcasecmp($this->request->session()->read('User.UserRole'), 'DirettoreDipartimento') === 0) or
                           (strcasecmp($this->request->session()->read('User.UserRole'), 'DirettoreGenerale') === 0) or
                           (strcasecmp($this->request->session()->read('User.UserRole'), 'Analista') === 0))
                       {
                           echo  '<br/>'.($this->Html->link('Cambia sezione', ['controller' => 'Users', 'action'=>'Changerole'],
                                   array('class'=>'badge badge-pill badge-danger', 'style'=>'height:15px; width: 100px; valign:top; background-color:#ce8483'))) ;
                       } ?>


               </span></div>


<?php }
} ?>
</nav>

<div class="col-lg-8 col-md-7 col-sm-6">
    <div >

        <ul class="center-block">
            <?php if(!($loggedIn)): ?>

                <h1>Per iniziare effettuare il login</h1>

                       <?php endif; ?>
        </ul>
    </div>


</div>

 <?php echo $this->Flash->render();?>


<div class="container clearfix">

    <?=
    $this->fetch('content') ?>

</div>
    <footer>
    </footer>
</body>
</html>
