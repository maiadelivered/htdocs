<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vlistdipendentefase Model
 *
 * @property \App\Model\Table\SezioneTable|\Cake\ORM\Association\BelongsTo $Sezione
 *
 * @method \App\Model\Entity\Vlistdipendentefase get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vlistdipendentefase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vlistdipendentefase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vlistdipendentefase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vlistdipendentefase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistdipendentefase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistdipendentefase findOrCreate($search, callable $callback = null, $options = [])
 */
class VlistdipendentefaseTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vlistdipendentefase');

        $this->belongsTo('Sezione', [
            'foreignKey' => 'Sezione_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('Dipendente')
            ->allowEmpty('Dipendente');

        $validator
            ->scalar('PO_AP')
            ->allowEmpty('PO_AP');

        $validator
            ->scalar('Categoria')
            ->allowEmpty('Categoria');

        $validator
            ->scalar('NomeFase')
            ->allowEmpty('NomeFase');

        $validator
            ->integer('PercImpegnoSuFase')
            ->allowEmpty('PercImpegnoSuFase');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        $validator
            ->integer('IdProcesso')
            ->requirePresence('IdProcesso', 'create')
            ->notEmpty('IdProcesso');

        $validator
            ->scalar('NomeProcesso')
            ->allowEmpty('NomeProcesso');

        $validator
            ->integer('IdFase')
            ->requirePresence('IdFase', 'create')
            ->notEmpty('IdFase');

        $validator
            ->scalar('NomeStruttura')
            ->allowEmpty('NomeStruttura');

        $validator
            ->integer('IdStruttura')
            ->requirePresence('IdStruttura', 'create')
            ->notEmpty('IdStruttura');

        $validator
            ->integer('IdEsegue')
            ->requirePresence('IdEsegue', 'create')
            ->notEmpty('IdEsegue');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['Sezione_id'], 'Sezione'));

        return $rules;
    }
}
