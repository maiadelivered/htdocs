<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vdipeprorownp Model
 *
 * @method \App\Model\Entity\Vdipeprorownp get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vdipeprorownp newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vdipeprorownp[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vdipeprorownp|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vdipeprorownp patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vdipeprorownp[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vdipeprorownp findOrCreate($search, callable $callback = null, $options = [])
 */
class VdipeprorownpTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vdipeprorownp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('NomeDipartimento')
            ->allowEmpty('NomeDipartimento');

        $validator
            ->scalar('NomeSezione')
            ->allowEmpty('NomeSezione');

        $validator
            ->scalar('NomeStruttura')
            ->allowEmpty('NomeStruttura');

        $validator
            ->scalar('TipoProcesso')
            ->allowEmpty('TipoProcesso');

        $validator
            ->scalar('NomeProcesso')
            ->allowEmpty('NomeProcesso');

        $validator
            ->integer('IdDipartimento')
            ->requirePresence('IdDipartimento', 'create')
            ->notEmpty('IdDipartimento');

        $validator
            ->integer('Sezione_Id')
            ->requirePresence('Sezione_Id', 'create')
            ->notEmpty('Sezione_Id');

        $validator
            ->integer('Struttura_Id')
            ->requirePresence('Struttura_Id', 'create')
            ->notEmpty('Struttura_Id');

        $validator
            ->integer('IdProcesso')
            ->requirePresence('IdProcesso', 'create')
            ->notEmpty('IdProcesso');

        $validator
            ->integer('IdTipoProcesso')
            ->requirePresence('IdTipoProcesso', 'create')
            ->notEmpty('IdTipoProcesso');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        $validator
            ->requirePresence('NumDipTot', 'create')
            ->notEmpty('NumDipTot');

        $validator
            ->decimal('NumCatD')
            ->allowEmpty('NumCatD');

        $validator
            ->decimal('NumPO')
            ->allowEmpty('NumPO');

        $validator
            ->decimal('NumAP')
            ->allowEmpty('NumAP');

        $validator
            ->decimal('NumAltrePOAP')
            ->allowEmpty('NumAltrePOAP');

        $validator
            ->decimal('NumCatDirigente')
            ->allowEmpty('NumCatDirigente');

        $validator
            ->decimal('NumCatC')
            ->allowEmpty('NumCatC');

        $validator
            ->decimal('NumCatB')
            ->allowEmpty('NumCatB');

        $validator
            ->decimal('NumCatSegConsReg')
            ->allowEmpty('NumCatSegConsReg');

        $validator
            ->decimal('NumCatA')
            ->allowEmpty('NumCatA');

        $validator
            ->decimal('NumCatDirDip')
            ->allowEmpty('NumCatDirDip');

        $validator
            ->decimal('NumCatCapoRedattore')
            ->allowEmpty('NumCatCapoRedattore');

        $validator
            ->decimal('NumCatSegrGiuntaReg')
            ->allowEmpty('NumCatSegrGiuntaReg');

        $validator
            ->decimal('NumCatResCoordPolInte')
            ->allowEmpty('NumCatResCoordPolInte');

        $validator
            ->decimal('NumCatDirConTD')
            ->allowEmpty('NumCatDirConTD');

        $validator
            ->decimal('NumCatCapoSerSenior')
            ->allowEmpty('NumCatCapoSerSenior');

        $validator
            ->decimal('NumCatRed30Mesi')
            ->allowEmpty('NumCatRed30Mesi');

        $validator
            ->decimal('NumCatCapoGab')
            ->allowEmpty('NumCatCapoGab');

        $validator
            ->decimal('NumCatSegrGenPres')
            ->allowEmpty('NumCatSegrGenPres');

        $validator
            ->decimal('NumCatAvvCoord')
            ->allowEmpty('NumCatAvvCoord');

        $validator
            ->decimal('PercCatD')
            ->allowEmpty('PercCatD');

        $validator
            ->decimal('PercPO')
            ->allowEmpty('PercPO');

        $validator
            ->decimal('PercAP')
            ->allowEmpty('PercAP');

        $validator
            ->decimal('PercAltrePOAP')
            ->allowEmpty('PercAltrePOAP');

        $validator
            ->decimal('PercCatDirigente')
            ->allowEmpty('PercCatDirigente');

        $validator
            ->decimal('PercCatC')
            ->allowEmpty('PercCatC');

        $validator
            ->decimal('PercCatB')
            ->allowEmpty('PercCatB');

        $validator
            ->decimal('PercCatSegConsReg')
            ->allowEmpty('PercCatSegConsReg');

        $validator
            ->decimal('PercCatA')
            ->allowEmpty('PercCatA');

        $validator
            ->decimal('PercCatDirDip')
            ->allowEmpty('PercCatDirDip');

        $validator
            ->decimal('PercCatCapoRedattore')
            ->allowEmpty('PercCatCapoRedattore');

        $validator
            ->decimal('PercCatSegrGiuntaReg')
            ->allowEmpty('PercCatSegrGiuntaReg');

        $validator
            ->decimal('PercCatResCoordPolInte')
            ->allowEmpty('PercCatResCoordPolInte');

        $validator
            ->decimal('PercCatDirConTD')
            ->allowEmpty('PercCatDirConTD');

        $validator
            ->decimal('PercCatCapoSerSenior')
            ->allowEmpty('PercCatCapoSerSenior');

        $validator
            ->decimal('PercCatRed30Mesi')
            ->allowEmpty('PercCatRed30Mesi');

        $validator
            ->decimal('PercCatCapoGab')
            ->allowEmpty('PercCatCapoGab');

        $validator
            ->decimal('PercCatSegrGenPres')
            ->allowEmpty('PercCatSegrGenPres');

        $validator
            ->decimal('PercCatAvvCoord')
            ->allowEmpty('PercCatAvvCoord');

        return $validator;
    }
}
