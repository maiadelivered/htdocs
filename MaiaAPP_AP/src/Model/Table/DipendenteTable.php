<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dipendente Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Categoria
 * @property |\Cake\ORM\Association\BelongsTo $Dipartimento
 * @property |\Cake\ORM\Association\BelongsTo $Sezione
 * @property \App\Model\Table\FaseTable|\Cake\ORM\Association\BelongsToMany $Fase
 * @property \App\Model\Table\FaseBackupdatiTable|\Cake\ORM\Association\BelongsToMany $FaseBackupdati
 * @property \App\Model\Table\StrutturaTable|\Cake\ORM\Association\BelongsToMany $Struttura
 *
 * @method \App\Model\Entity\Dipendente get($primaryKey, $options = [])
 * @method \App\Model\Entity\Dipendente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Dipendente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dipendente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dipendente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Dipendente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dipendente findOrCreate($search, callable $callback = null, $options = [])
 */
class DipendenteTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dipendente');
        $this->setDisplayField('IdDipendente');
        $this->setPrimaryKey('IdDipendente');

        $this->belongsTo('Categoria', [
            'foreignKey' => 'Categoria_id'
        ]);
        $this->belongsTo('Dipartimento', [
            'foreignKey' => 'Dipartimento_id'
        ]);
        $this->belongsTo('Sezione', [
            'foreignKey' => 'Sezione_id'
        ]);
        $this->belongsToMany('Fase', [
            'foreignKey' => 'dipendente_id',
            'targetForeignKey' => 'fase_id',
            'joinTable' => 'dipendente_fase'
        ]);
        $this->belongsToMany('FaseBackupdati', [
            'foreignKey' => 'dipendente_id',
            'targetForeignKey' => 'fase_backupdati_id',
            'joinTable' => 'dipendente_fase_backupdati'
        ]);
        $this->belongsToMany('Struttura', [
            'foreignKey' => 'dipendente_id',
            'targetForeignKey' => 'struttura_id',
            'joinTable' => 'dipendente_struttura'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdDipendente')
            ->allowEmpty('IdDipendente', 'create');

        $validator
            ->scalar('Dipendente')
            ->allowEmpty('Dipendente');

        $validator
            ->scalar('COGNOME')
            ->allowEmpty('COGNOME');

        $validator
            ->scalar('NOME')
            ->allowEmpty('NOME');

        $validator
            ->scalar('DESC_QUALIFICA')
            ->allowEmpty('DESC_QUALIFICA');

        $validator
            ->scalar('PO_AP')
            ->allowEmpty('PO_AP');

        $validator
            ->scalar('PO')
            ->allowEmpty('PO');

        $validator
            ->scalar('AP')
            ->allowEmpty('AP');

        $validator
            ->scalar('AltrePOAP')
            ->allowEmpty('AltrePOAP');

        $validator
            ->scalar('DATA_NAS')
            ->allowEmpty('DATA_NAS');

        $validator
            ->scalar('DESC_TITOLO_STUDIO')
            ->allowEmpty('DESC_TITOLO_STUDIO');

        $validator
            ->scalar('DIP')
            ->allowEmpty('DIP');

        $validator
            ->scalar('SEZIONE')
            ->allowEmpty('SEZIONE');

        $validator
            ->scalar('SERVIZIO')
            ->requirePresence('SERVIZIO', 'create')
            ->notEmpty('SERVIZIO');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['Categoria_id'], 'Categoria'));
        $rules->add($rules->existsIn(['Dipartimento_id'], 'Dipartimento'));
        $rules->add($rules->existsIn(['Sezione_id'], 'Sezione'));

        return $rules;
    }
}
