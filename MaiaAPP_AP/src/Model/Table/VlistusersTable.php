<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vlistusers Model
 *
 * @method \App\Model\Entity\Vlistuser get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vlistuser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vlistuser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vlistuser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vlistuser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistuser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistuser findOrCreate($search, callable $callback = null, $options = [])
 */
class VlistusersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vlistusers');
        $this->setPrimaryKey('id_user');
        $this->setDisplayField('name');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_user')
            ->requirePresence('id_user', 'create')
            ->notEmpty('id_user');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('password')
            ->allowEmpty('password');

        $validator
            ->scalar('address')
            ->allowEmpty('address');

        $validator
            ->integer('idDipartimento')
            ->allowEmpty('idDipartimento');

        $validator
            ->scalar('CheckIdDipartimento')
            ->allowEmpty('CheckIdDipartimento');

        $validator
            ->integer('idSezione')
            ->allowEmpty('idSezione');

        $validator
            ->scalar('checkIdSezione')
            ->allowEmpty('checkIdSezione');

        $validator
            ->integer('idrilevamento')
            ->allowEmpty('idrilevamento');

        $validator
            ->dateTime('birthdate')
            ->allowEmpty('birthdate');

        $validator
            ->scalar('community')
            ->allowEmpty('community');

        $validator
            ->scalar('gender')
            ->allowEmpty('gender');

        $validator
            ->scalar('language')
            ->allowEmpty('language');

        $validator
            ->scalar('location')
            ->allowEmpty('location');

        $validator
            ->scalar('short_bio')
            ->allowEmpty('short_bio');

        $validator
            ->scalar('surname')
            ->allowEmpty('surname');

        $validator
            ->scalar('username')
            ->allowEmpty('username');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }
}
