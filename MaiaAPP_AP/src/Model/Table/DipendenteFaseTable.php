<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DipendenteFase Model
 *
 * @property \App\Model\Table\DipendenteTable|\Cake\ORM\Association\BelongsTo $Dipendente
 * @property \App\Model\Table\FaseTable|\Cake\ORM\Association\BelongsTo $Fase
 *
 * @method \App\Model\Entity\DipendenteFase get($primaryKey, $options = [])
 * @method \App\Model\Entity\DipendenteFase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DipendenteFase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DipendenteFase|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DipendenteFase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DipendenteFase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DipendenteFase findOrCreate($search, callable $callback = null, $options = [])
 */
class DipendenteFaseTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dipendente_fase');
        $this->setDisplayField('IdEsegue');
        $this->setPrimaryKey('IdEsegue');

        $this->belongsTo('Dipendente', [
            'foreignKey' => 'Dipendente_id'
        ]);
        $this->belongsTo('Fase', [
            'foreignKey' => 'Fase_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdEsegue')
            ->allowEmpty('IdEsegue', 'create');

        $validator
            ->scalar('CheckIdFase')
            ->allowEmpty('CheckIdFase');

        $validator
            ->scalar('STRUTTURA')
            ->allowEmpty('STRUTTURA');

        $validator
            ->scalar('checkDipendente')
            ->allowEmpty('checkDipendente');

        $validator
            ->integer('PercImpegnoSuFase')
            ->allowEmpty('PercImpegnoSuFase');

        $validator
            ->integer('IdRilevamento')
            ->allowEmpty('IdRilevamento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['Dipendente_id'], 'Dipendente'));
        $rules->add($rules->existsIn(['Fase_id'], 'Fase'));

        return $rules;
    }
}
