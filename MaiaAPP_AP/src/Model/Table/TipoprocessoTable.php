<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipoprocesso Model
 *
 * @method \App\Model\Entity\Tipoprocesso get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipoprocesso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipoprocesso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipoprocesso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipoprocesso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipoprocesso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipoprocesso findOrCreate($search, callable $callback = null, $options = [])
 */
class TipoprocessoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tipoprocesso');
        $this->setDisplayField('IdTipoProcesso');
        $this->setPrimaryKey('IdTipoProcesso');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('IdTipoProcesso')
            ->allowEmpty('IdTipoProcesso', 'create');

        $validator
            ->scalar('TipoProcesso')
            ->allowEmpty('TipoProcesso');

        return $validator;
    }
}
