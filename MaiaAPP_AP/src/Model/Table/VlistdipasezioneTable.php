<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vlistdipasezione Model
 *
 * @method \App\Model\Entity\Vlistdipasezione get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vlistdipasezione newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vlistdipasezione[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vlistdipasezione|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vlistdipasezione patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistdipasezione[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vlistdipasezione findOrCreate($search, callable $callback = null, $options = [])
 */
class VlistdipasezioneTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vlistdipasezione');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('NomeDipartimento')
            ->allowEmpty('NomeDipartimento');

        $validator
            ->scalar('NomeSezione')
            ->allowEmpty('NomeSezione');

        $validator
            ->integer('IdDipartimento')
            ->requirePresence('IdDipartimento', 'create')
            ->notEmpty('IdDipartimento');

        $validator
            ->integer('IdSezione')
            ->requirePresence('IdSezione', 'create')
            ->notEmpty('IdSezione');

        return $validator;
    }
}
