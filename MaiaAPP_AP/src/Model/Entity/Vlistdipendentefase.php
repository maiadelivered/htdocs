<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vlistdipendentefase Entity
 *
 * @property string $Dipendente
 * @property string $PO_AP
 * @property string $Categoria
 * @property int $Sezione_id
 * @property string $NomeFase
 * @property int $PercImpegnoSuFase
 * @property int $IdRilevamento
 * @property int $IdProcesso
 * @property string $NomeProcesso
 * @property int $IdFase
 * @property string $NomeStruttura
 * @property int $IdStruttura
 * @property int $IdEsegue
 *
 * @property \App\Model\Entity\Sezione $sezione
 */
class Vlistdipendentefase extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Dipendente' => true,
        'PO_AP' => true,
        'Categoria' => true,
        'Sezione_id' => true,
        'NomeFase' => true,
        'PercImpegnoSuFase' => true,
        'IdRilevamento' => true,
        'IdProcesso' => true,
        'NomeProcesso' => true,
        'IdFase' => true,
        'NomeStruttura' => true,
        'IdStruttura' => true,
        'IdEsegue' => true,
        'sezione' => true
    ];
}
