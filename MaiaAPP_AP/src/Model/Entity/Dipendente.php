<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dipendente Entity
 *
 * @property int $IdDipendente
 * @property int $Categoria_id
 * @property string $Dipendente
 * @property string $COGNOME
 * @property string $NOME
 * @property string $DESC_QUALIFICA
 * @property string $PO_AP
 * @property string $PO
 * @property string $AP
 * @property string $AltrePOAP
 * @property string $DATA_NAS
 * @property string $DESC_TITOLO_STUDIO
 * @property string $DIP
 * @property string $SEZIONE
 * @property string $SERVIZIO
 * @property int $Dipartimento_id
 * @property int $Sezione_id
 *
 * @property \App\Model\Entity\Categoria $categoria
 * @property \App\Model\Entity\Dipartimento $dipartimento
 * @property \App\Model\Entity\Sezione $sezione
 * @property \App\Model\Entity\Fase[] $fase
 * @property \App\Model\Entity\FaseBackupdati[] $fase_backupdati
 * @property \App\Model\Entity\Struttura[] $struttura
 */
class Dipendente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Categoria_id' => true,
        'Dipendente' => true,
        'COGNOME' => true,
        'NOME' => true,
        'DESC_QUALIFICA' => true,
        'PO_AP' => true,
        'PO' => true,
        'AP' => true,
        'AltrePOAP' => true,
        'DATA_NAS' => true,
        'DESC_TITOLO_STUDIO' => true,
        'DIP' => true,
        'SEZIONE' => true,
        'SERVIZIO' => true,
        'Dipartimento_id' => true,
        'Sezione_id' => true,
        'categoria' => true,
        'dipartimento' => true,
        'sezione' => true,
        'fase' => true,
        'fase_backupdati' => true,
        'struttura' => true
    ];
}
