<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Struttura Entity
 *
 * @property int $IdStruttura
 * @property int $Dipartimento_Id
 * @property int $Sezione_Id
 * @property string $CheckIdSezione
 * @property string $CheckIdDipartimento
 * @property string $NomeStruttura
 * @property string $NomeDirigente
 * @property string $Email
 * @property int $IdDirigente
 *
 * @property \App\Model\Entity\Dipendente[] $dipendente
 */
class Struttura extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Dipartimento_Id' => true,
        'Sezione_Id' => true,
        'CheckIdSezione' => true,
        'CheckIdDipartimento' => true,
        'NomeStruttura' => true,
        'NomeDirigente' => true,
        'Email' => true,
        'IdDirigente' => true,
        'dipendente' => true
    ];
}
