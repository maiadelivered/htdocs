<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DipendenteFase Entity
 *
 * @property int $IdEsegue
 * @property int $Dipendente_id
 * @property int $Fase_id
 * @property string $CheckIdFase
 * @property string $STRUTTURA
 * @property string $checkDipendente
 * @property int $PercImpegnoSuFase
 * @property int $IdRilevamento
 *
 * @property \App\Model\Entity\Dipendente $dipendente
 * @property \App\Model\Entity\Fase $fase
 */
class DipendenteFase extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Dipendente_id' => true,
        'Fase_id' => true,
        'CheckIdFase' => true,
        'STRUTTURA' => true,
        'checkDipendente' => true,
        'PercImpegnoSuFase' => true,
        'IdRilevamento' => true,
        'dipendente' => true,
        'fase' => true
    ];
}
