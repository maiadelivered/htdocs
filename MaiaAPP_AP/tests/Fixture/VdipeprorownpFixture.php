<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * VdipeprorownpFixture
 *
 */
class VdipeprorownpFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'vdipeprorownp';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'NomeDipartimento' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'NomeSezione' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'NomeStruttura' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'TipoProcesso' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'NomeProcesso' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'IdDipartimento' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Sezione_Id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Struttura_Id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'IdProcesso' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'IdTipoProcesso' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'IdRilevamento' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'NumDipTot' => ['type' => 'biginteger', 'length' => 21, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'NumCatD' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumPO' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumAP' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumAltrePOAP' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatDirigente' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatC' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatB' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatSegConsReg' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatA' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatDirDip' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatCapoRedattore' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatSegrGiuntaReg' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatResCoordPolInte' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatDirConTD' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatCapoSerSenior' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatRed30Mesi' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatCapoGab' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatSegrGenPres' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'NumCatAvvCoord' => ['type' => 'decimal', 'length' => 23, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatD' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercPO' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercAP' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercAltrePOAP' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatDirigente' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatC' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatB' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatSegConsReg' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatA' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatDirDip' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatCapoRedattore' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatSegrGiuntaReg' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatResCoordPolInte' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatDirConTD' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatCapoSerSenior' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatRed30Mesi' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatCapoGab' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatSegrGenPres' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'PercCatAvvCoord' => ['type' => 'decimal', 'length' => 32, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        '_options' => [
            'engine' => null,
            'collation' => null
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'NomeDipartimento' => 'Lorem ipsum dolor sit amet',
            'NomeSezione' => 'Lorem ipsum dolor sit amet',
            'NomeStruttura' => 'Lorem ipsum dolor sit amet',
            'TipoProcesso' => 'Lorem ipsum dolor sit amet',
            'NomeProcesso' => 'Lorem ipsum dolor sit amet',
            'IdDipartimento' => 1,
            'Sezione_Id' => 1,
            'Struttura_Id' => 1,
            'IdProcesso' => 1,
            'IdTipoProcesso' => 1,
            'IdRilevamento' => 1,
            'NumDipTot' => 1,
            'NumCatD' => 1.5,
            'NumPO' => 1.5,
            'NumAP' => 1.5,
            'NumAltrePOAP' => 1.5,
            'NumCatDirigente' => 1.5,
            'NumCatC' => 1.5,
            'NumCatB' => 1.5,
            'NumCatSegConsReg' => 1.5,
            'NumCatA' => 1.5,
            'NumCatDirDip' => 1.5,
            'NumCatCapoRedattore' => 1.5,
            'NumCatSegrGiuntaReg' => 1.5,
            'NumCatResCoordPolInte' => 1.5,
            'NumCatDirConTD' => 1.5,
            'NumCatCapoSerSenior' => 1.5,
            'NumCatRed30Mesi' => 1.5,
            'NumCatCapoGab' => 1.5,
            'NumCatSegrGenPres' => 1.5,
            'NumCatAvvCoord' => 1.5,
            'PercCatD' => 1.5,
            'PercPO' => 1.5,
            'PercAP' => 1.5,
            'PercAltrePOAP' => 1.5,
            'PercCatDirigente' => 1.5,
            'PercCatC' => 1.5,
            'PercCatB' => 1.5,
            'PercCatSegConsReg' => 1.5,
            'PercCatA' => 1.5,
            'PercCatDirDip' => 1.5,
            'PercCatCapoRedattore' => 1.5,
            'PercCatSegrGiuntaReg' => 1.5,
            'PercCatResCoordPolInte' => 1.5,
            'PercCatDirConTD' => 1.5,
            'PercCatCapoSerSenior' => 1.5,
            'PercCatRed30Mesi' => 1.5,
            'PercCatCapoGab' => 1.5,
            'PercCatSegrGenPres' => 1.5,
            'PercCatAvvCoord' => 1.5
        ],
    ];
}
