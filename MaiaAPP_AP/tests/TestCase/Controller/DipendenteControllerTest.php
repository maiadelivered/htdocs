<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DipendenteController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DipendenteController Test Case
 */
class DipendenteControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dipendente',
        'app.categorias',
        'app.dipartimentos',
        'app.seziones',
        'app.fase',
        'app.struttura',
        'app.dipendente_struttura',
        'app.processo',
        'app.dipendente_fase',
        'app.fase_backupdati',
        'app.dipendente_fase_backupdati'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
