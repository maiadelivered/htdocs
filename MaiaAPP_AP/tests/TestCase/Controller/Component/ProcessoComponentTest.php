<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\ProcessoComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\ProcessoComponent Test Case
 */
class ProcessoComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\ProcessoComponent
     */
    public $Processo;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Processo = new ProcessoComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Processo);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
