<?php
namespace App\Test\TestCase\Form;

use App\Form\ProcessoForm;
use Cake\TestSuite\TestCase;

/**
 * App\Form\ProcessoForm Test Case
 */
class ProcessoFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Form\ProcessoForm
     */
    public $Processo;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Processo = new ProcessoForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Processo);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
