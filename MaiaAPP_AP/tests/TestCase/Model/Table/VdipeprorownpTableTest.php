<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VdipeprorownpTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VdipeprorownpTable Test Case
 */
class VdipeprorownpTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VdipeprorownpTable
     */
    public $Vdipeprorownp;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vdipeprorownp'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Vdipeprorownp') ? [] : ['className' => VdipeprorownpTable::class];
        $this->Vdipeprorownp = TableRegistry::get('Vdipeprorownp', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Vdipeprorownp);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
