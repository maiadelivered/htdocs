<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VlistdipasezioneTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VlistdipasezioneTable Test Case
 */
class VlistdipasezioneTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VlistdipasezioneTable
     */
    public $Vlistdipasezione;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vlistdipasezione'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Vlistdipasezione') ? [] : ['className' => VlistdipasezioneTable::class];
        $this->Vlistdipasezione = TableRegistry::get('Vlistdipasezione', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Vlistdipasezione);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
