<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DipendenteTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DipendenteTable Test Case
 */
class DipendenteTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DipendenteTable
     */
    public $Dipendente;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dipendente',
        'app.categorias',
        'app.dipartimentos',
        'app.seziones',
        'app.fase',
        'app.struttura',
        'app.dipendente_struttura',
        'app.processo',
        'app.dipendente_fase',
        'app.fase_backupdati',
        'app.dipendente_fase_backupdati'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Dipendente') ? [] : ['className' => DipendenteTable::class];
        $this->Dipendente = TableRegistry::get('Dipendente', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dipendente);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
