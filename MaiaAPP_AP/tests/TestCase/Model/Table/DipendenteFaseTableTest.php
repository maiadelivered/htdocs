<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DipendenteFaseTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DipendenteFaseTable Test Case
 */
class DipendenteFaseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DipendenteFaseTable
     */
    public $DipendenteFase;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dipendente_fase',
        'app.dipendente',
        'app.fase',
        'app.struttura',
        'app.dipendente_struttura',
        'app.processo'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DipendenteFase') ? [] : ['className' => DipendenteFaseTable::class];
        $this->DipendenteFase = TableRegistry::get('DipendenteFase', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DipendenteFase);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
