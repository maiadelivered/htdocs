<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VlistdipendentefaseTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VlistdipendentefaseTable Test Case
 */
class VlistdipendentefaseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VlistdipendentefaseTable
     */
    public $Vlistdipendentefase;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vlistdipendentefase',
        'app.sezione'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Vlistdipendentefase') ? [] : ['className' => VlistdipendentefaseTable::class];
        $this->Vlistdipendentefase = TableRegistry::get('Vlistdipendentefase', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Vlistdipendentefase);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
