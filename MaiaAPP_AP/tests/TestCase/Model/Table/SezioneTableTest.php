<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SezioneTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SezioneTable Test Case
 */
class SezioneTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SezioneTable
     */
    public $Sezione;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sezione',
        'app.dipartimento'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sezione') ? [] : ['className' => SezioneTable::class];
        $this->Sezione = TableRegistry::get('Sezione', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sezione);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
