<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\RilevamentoBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\RilevamentoBehavior Test Case
 */
class RilevamentoBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Behavior\RilevamentoBehavior
     */
    public $Rilevamento;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Rilevamento = new RilevamentoBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rilevamento);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
