<?php 

/**
 * 
 */

error_reporting(1);


define('PROXY_HOST','config.istat.it');
define('PROXY_PORT','3128');

// esempio con il webMethod - GetCompactData
// http://ec.europa.eu/eurostat/sri/service/2.0/GetCompactData

$soapAction='http://ec.europa.eu/eurostat/sri/service/2.0/'.$_POST['webmethod'];



if ((!empty($_POST['requestboby']))&& (!empty($_POST['url'])) 
     && (!empty($_POST['requestboby']))){ 
	$responseBody=soapClient($soapAction,$_POST['requestboby'],$_POST['url'],$_POST['user'],$_POST['psw']);
	echo $responseBody;
} else echo "inserire i parametri richiesti! (url, autenticazione, query SDMX...)";





/*   esempio di funzione parametrizzata */

/**
 *
 * Richiesta al WebServices
 *
 * @param string $soapAction WebMethod
 * @param string $request la request body XML
 * @param string $url inizializzato con la define
 * @return string $response ritorna il response body XML
 *
 *
 * N.B. attivare o disattivare i parametri del proxy in base alle esigenze
 *
 */

function soapClient($soapAction,$request,$url,$login,$psw){
	 
	$wsdl=$url.'?WSDL';

	try
	{   //WSDL
		$soap = new SoapClient( $wsdl,
				array("trace" => 1,
						"exceptions" => 0,
						"login"=>$login,
						"password"=>$psw,
						"proxy_host" => PROXY_HOST, // host proxy istat
						"proxy_port" => PROXY_PORT, // porta proxy istat
						"cache_wsdl" => WSDL_CACHE_NONE,
						"features" => SOAP_SINGLE_ELEMENT_ARRAYS));
		$response = $soap->__doRequest($request, $url , $soapAction, null, $one_way = 0) ;

		// report
		//echo "<h4>URL:</h4>".  $wsdl ."<br/>";

		//echo "<h4>REQUEST HEADERS:</h4>" . highlight_string( $soap->__getLastRequestHeaders(), 1). "<br/>";
		 
		//echo "<h4>Request body (XML)</h4>".highlight_string( $request, 1)."<br/><br/>";

		//echo "<h4>RESPONSE HEADERS:</h4>" . highlight_string( $soap->__getLastResponseHeaders(), 1) . "<br/>";
		 
		//echo "<h4>Response body (XML)</h4>".highlight_string( $response ,1)."<br>";
		// end report


	}
	catch (SoapFault $fault)
	{
		var_dump( htmlentities($fault) );
	}
	catch (Exception $e)
	{
		var_dump( htmlentities($e) );
	}

	return $response;
}

?>